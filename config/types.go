package config

import "time"

const (
	NativeSign = "native"

	GASFEERATIO      = "2"
	DEFAULTNATIVEGAS = 21000
	MINCHECKBLOCKGAP = 30
)

const (
	BSC  = "BSC"
	ETH  = "ETH"
	AVA  = "AVA"
	POL  = "POL"
	ATOM = "ATOM"

	JOLTIFY = "JOLTIFY"

	RETRYTIMEOUTBLOCK = 30
)

var (
	// ROUNDBLOCK we may need to increase it as we increase the time for keygen/keysign and join party
	ROUNDBLOCK = 100

	// EvmChains is the list of supported evm chains
	EvmChains = []string{BSC, ETH, AVA, POL}
	// Chains is the list of supported chains
	Chains = append(EvmChains, ATOM)
)

var ChainID = "joltifymock-1"

const (
	QueryTimeOut = time.Second * 6
)

// Direction is the direction of the oppy_bridge
type Direction int
