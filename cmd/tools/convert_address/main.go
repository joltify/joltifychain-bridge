package main

import (
	"fmt"
	"os"

	"gitlab.com/joltify/joltifychain-bridge/misc"
)

func main() {
	misc.SetupBech32Prefix()
	pubkey := os.Args[1]

	joltAddr, err := misc.PoolPubKeyToJoltifyAddress(pubkey)
	if err != nil {
		fmt.Printf("pubkey invalid error %v\n", err)
	}

	ethAddr, err := misc.PoolPubKeyToEthAddress(pubkey)
	if err != nil {
		fmt.Printf("pubkey invalid error %v\n", err)
	}
	fmt.Printf("joltify address %v\n", joltAddr.String())
	fmt.Printf("eth address %v\n", ethAddr)
}
