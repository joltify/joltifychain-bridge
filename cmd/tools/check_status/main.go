package main

import (
	"context"
	"flag"
	"time"

	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/rs/zerolog/log"
)

func main() {
	websocket := flag.String("ws", "ws://127.0.0.1:8456/", "chain websocket address and port")
	flag.Parse()

	logger := log.With().Logger()

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
	wsClient, err := ethclient.DialContext(ctx, *websocket)
	cancel()

	if err != nil {
		logger.Error().Err(err).Msg("fail to dial the websocket")
		return
	}

	for {
		process, err := wsClient.SyncProgress(context.Background())
		if err != nil {
			logger.Error().Err(err).Msg("fail to check the sync progress")
			return
		}

		head, err := wsClient.BlockNumber(context.Background())
		if err != nil {
			logger.Error().Err(err).Msg("fail to get the block number")
		} else {
			logger.Info().Msgf("head is %v", head)
		}

		if process != nil {
			c := float32(process.CurrentBlock)
			h := float32(process.HighestBlock)
			logger.Info().Msgf("current:%v--heightest %v and done %.5f%%", process.CurrentBlock, process.HighestBlock, (c/h)*100)
		}
		time.Sleep(time.Second * 3)
	}
}
