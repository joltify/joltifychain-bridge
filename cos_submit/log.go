package cossubmit

import (
	"fmt"

	"github.com/rs/zerolog"
	"github.com/tendermint/tendermint/libs/log"
)

type Logger struct {
	zerolog.Logger
}

func (l *Logger) Info(msg string, kvs ...interface{}) {
	lw := l.with(kvs...)
	// ignore the response log in WSClient
	// it's too large and too many to print
	if msg == "got response" {
		// lw.Debug().Msgf(msg)
		return
	}
	lw.Info().Msgf(msg)
}

func (l *Logger) Debug(msg string, kvs ...interface{}) {
	lw := l.with(kvs...)
	lw.Debug().Msgf(msg)
}

func (l *Logger) Error(msg string, kvs ...interface{}) {
	lw := l.with(kvs...)
	lw.Error().Msgf(msg)
}

func (l *Logger) with(kvs ...interface{}) zerolog.Logger {
	lw := l.Logger
	for i := 0; i < len(kvs); i += 2 {
		lw = lw.With().Str(fmt.Sprintf("%v", kvs[i]), fmt.Sprintf("%+v", kvs[i+1])).Logger()
	}
	return lw
}

func (l *Logger) With(kvs ...interface{}) log.Logger {
	l.Logger = l.with(kvs...)
	return l
}
