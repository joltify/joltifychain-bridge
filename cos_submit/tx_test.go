package cossubmit

import (
	"context"
	"sync"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/types/bech32/legacybech32"
	banktypes "github.com/cosmos/cosmos-sdk/x/bank/types"
	bcommon "gitlab.com/joltify/joltifychain-bridge/common"
	"gitlab.com/joltify/joltifychain-bridge/misc"
	"gitlab.com/joltify/joltifychain-bridge/tssclient"
)

func (h *cosSubmitTestSuite) TestComposeAndSend() {
	accs, err := generateRandomPrivKey(3)
	h.Require().NoError(err)
	mockTss := TssMock{
		accs[0].sk,
		h.network.Validators[0].ClientCtx.Keyring,
		true,
		true,
	}
	wg := &sync.WaitGroup{}
	oc, err := NewCosOperations(context.Background(), wg, h.network.Validators[0].AppConfig.GRPC.Address, h.network.Validators[0].RPCAddress, "tset", nil, &mockTss)
	h.Require().NoError(err)

	oc.Keyring = h.validatorkey

	info, _ := h.network.Validators[0].ClientCtx.Keyring.Key("node0")
	pk, _ := info.GetPubKey()
	pkstr := legacybech32.MustMarshalPubKey(legacybech32.AccPK, pk) // nolint
	valAddr, err := misc.PoolPubKeyToJoltifyAddress(pkstr)
	h.Require().NoError(err)

	acc, err := bcommon.QueryAccount(oc.GrpcClient, valAddr.String(), "")
	h.Require().NoError(err)

	send := banktypes.NewMsgSend(valAddr, valAddr, sdk.Coins{sdk.NewCoin("stake", sdk.NewInt(100))})
	_, resp, err := oc.ComposeAndSend(oc.GrpcClient, info, send, acc.GetSequence(), acc.GetAccountNumber(), nil, valAddr.String())
	h.Require().NoError(err)

	err = oc.QueryTxStatus(oc.GrpcClient, resp, 1)
	h.Require().ErrorContains(err, "tx status code is not 0")

	signMsg := tssclient.TssSignigMsg{
		Pk:          pkstr,
		Msgs:        []string{"test"},
		BlockHeight: 123,
		Version:     "whatever",
	}
	_, _, err = oc.ComposeAndSend(oc.GrpcClient, info, send, acc.GetSequence(), acc.GetAccountNumber(), &signMsg, valAddr.String())
	h.Require().NoError(err)

	err = oc.Terminate()
	h.Require().NoError(err)
	oc.TerminateTss()
}
