package cossubmit

import (
	"context"
	"strconv"
	"sync"
	"testing"
	"time"

	"github.com/cosmos/cosmos-sdk/client/grpc/tmservice"
	"github.com/cosmos/cosmos-sdk/crypto/keyring"
	"github.com/cosmos/cosmos-sdk/crypto/keys/ed25519"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/types/bech32/legacybech32" //nolint
	stakingtypes "github.com/cosmos/cosmos-sdk/x/staking/types"
	"github.com/joltify-finance/joltify_lending/testutil/network"
	vaulttypes "github.com/joltify-finance/joltify_lending/x/vault/types"
	"github.com/stretchr/testify/suite"
	coretypes "github.com/tendermint/tendermint/rpc/core/types"
	"github.com/tendermint/tendermint/types"

	"gitlab.com/joltify/joltifychain-bridge/config"
	"gitlab.com/joltify/joltifychain-bridge/misc"
)

type subscribeTestSuite struct {
	suite.Suite
	cfg         network.Config
	network     *network.Network
	validatorky keyring.Keyring
	queryClient tmservice.ServiceClient
}

func (f *subscribeTestSuite) SetupSuite() {
	misc.SetupBech32Prefix()
	cfg := network.DefaultConfig()
	cfg.BondDenom = "stake"
	cfg.BondedTokens = sdk.NewInt(10000000000000000)
	cfg.StakingTokens = sdk.NewInt(100000000000000000)
	f.cfg = cfg
	f.validatorky = keyring.NewInMemory(cfg.Codec)
	// now we put the mock pool list in the test
	state := vaulttypes.GenesisState{}
	stateStaking := stakingtypes.GenesisState{}

	f.Require().NoError(cfg.Codec.UnmarshalJSON(cfg.GenesisState[vaulttypes.ModuleName], &state))
	f.Require().NoError(cfg.Codec.UnmarshalJSON(cfg.GenesisState[stakingtypes.ModuleName], &stateStaking))

	validators, err := genNValidator(3, f.validatorky)
	f.Require().NoError(err)
	for i := 1; i < 5; i++ {
		randPoolSk := ed25519.GenPrivKey()
		poolPubKey, err := legacybech32.MarshalPubKey(legacybech32.AccPK, randPoolSk.PubKey()) // nolint
		f.Require().NoError(err)

		var nodes []sdk.AccAddress
		for _, el := range validators {
			operator, err := sdk.ValAddressFromBech32(el.OperatorAddress)
			if err != nil {
				panic(err)
			}
			nodes = append(nodes, operator.Bytes())
		}
		pro := vaulttypes.PoolProposal{
			PoolPubKey: poolPubKey,
			PoolAddr:   randPoolSk.PubKey().Address().Bytes(),
			Nodes:      nodes,
		}
		state.CreatePoolList = append(state.CreatePoolList, &vaulttypes.CreatePool{BlockHeight: strconv.Itoa(i), Validators: validators, Proposal: []*vaulttypes.PoolProposal{&pro}})
	}
	state.LatestTwoPool = state.CreatePoolList[:2]
	testToken := vaulttypes.IssueToken{
		Index: "testindex",
	}
	state.IssueTokenList = append(state.IssueTokenList, &testToken)

	buf, err := cfg.Codec.MarshalJSON(&state)
	f.Require().NoError(err)
	cfg.GenesisState[vaulttypes.ModuleName] = buf

	var stateVault stakingtypes.GenesisState
	f.Require().NoError(cfg.Codec.UnmarshalJSON(cfg.GenesisState[stakingtypes.ModuleName], &stateVault))
	stateVault.Params.MaxValidators = 3
	state.Params.BlockChurnInterval = 1
	buf, err = cfg.Codec.MarshalJSON(&stateVault)
	f.Require().NoError(err)
	cfg.GenesisState[stakingtypes.ModuleName] = buf

	f.network = network.New(f.T(), cfg)

	f.Require().NotNil(f.network)

	_, err = f.network.WaitForHeight(1)
	f.Require().Nil(err)
	f.queryClient = tmservice.NewServiceClient(f.network.Validators[0].ClientCtx)
}

func (s *subscribeTestSuite) TestSubscribe() {
	key := keyring.NewInMemory(s.cfg.Codec)
	wg := &sync.WaitGroup{}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// NOTE: should use RPCAddress as websocket address, else it will raise websocket: bad handshake
	handler, err := NewCosOperations(ctx, wg, s.network.Validators[0].APIAddress, s.network.Validators[0].RPCAddress, config.JOLTIFY, key, nil, s.network.Validators[0].ClientCtx)
	s.Require().NoError(err)

	err = handler.StartSubscription()
	s.Require().NoError(err)
	defer handler.Terminate()
	block := <-handler.GetChannelQueueNewBlockChan()
	currentBlockHeight1 := block.Height

	err = s.network.WaitForNextBlock()
	s.Require().NoError(err)

	block = <-handler.GetChannelQueueNewBlockChan()
	currentBlockHeight2 := block.Height

	s.Require().Equal(currentBlockHeight1+1, currentBlockHeight2)

	counter := currentBlockHeight2
	go func() {
		for {
			b := <-handler.GetChannelQueueNewBlockChan()
			s.Require().Equal(b.Height, counter+1)
			counter++
			// 11=4+2+5+3=14
			if counter == currentBlockHeight2+14 {
				return
			}
		}
	}()

	// we cache 4 blocks
	current := currentBlockHeight2 + 4
	_, err = s.network.WaitForHeightWithTimeout(current, 4*6*time.Second)
	s.Require().NoError(err)
	err = handler.RetryChain(true)
	s.Require().NoError(err)
	current += 2
	_, err = s.network.WaitForHeightWithTimeout(current, 2*6*time.Second)
	s.Require().NoError(err)

	// do the test again
	current += 5
	_, err = s.network.WaitForHeightWithTimeout(current, 5*6*time.Second)
	s.Require().NoError(err)
	err = handler.RetryChain(true)
	s.Require().NoError(err)

	current += 3
	_, err = s.network.WaitForHeightWithTimeout(current, 3*6*time.Second)
	s.Require().NoError(err)
}

func (s *subscribeTestSuite) createMockChan() <-chan coretypes.ResultEvent {
	e1 := coretypes.ResultEvent{
		Data: types.EventDataNewBlockHeader{
			Header: types.Header{Height: 1},
		},
	}
	e2 := coretypes.ResultEvent{
		Data: types.EventDataNewBlockHeader{
			Header: types.Header{Height: 2},
		},
	}
	mockChan := make(chan coretypes.ResultEvent, 2)
	mockChan <- e1
	mockChan <- e2
	return mockChan
}

func (f *subscribeTestSuite) TestProcess() {
	key := keyring.NewInMemory(f.cfg.Codec)
	wg := &sync.WaitGroup{}
	ctx, cancel := context.WithCancel(context.Background())
	handler, err := NewCosOperations(ctx, wg, f.network.Validators[0].RPCAddress, f.network.Validators[0].APIAddress, config.JOLTIFY, key, nil, f.network.Validators[0].ClientCtx)
	f.Require().NoError(err)

	mockChan := f.createMockChan()
	handler.currentNewValidator = mockChan
	wg.Add(2)
	go func() {
		handler.eventListener()
		wg.Done()
	}()
	go func() {
		defer func() {
			cancel()
			wg.Done()
		}()
		timeout := time.After(time.Second)
		tick := time.Tick(200 * time.Millisecond)
		for {
			select {
			case <-tick:
				if len(handler.currentNewValidator) == 0 {
					return
				}
			case <-timeout:
				return
			}
		}
	}()
	wg.Wait()

	f.Require().Equal(2, len(handler.ChannelQueueValidator))
}

func TestSubscribeAndRetry(t *testing.T) {
	suite.Run(t, new(subscribeTestSuite))
}
