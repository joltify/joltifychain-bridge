package cossubmit

import (
	"context"
	"sync"
	"sync/atomic"
	"time"

	"github.com/cosmos/cosmos-sdk/client/grpc/tmservice"
	"github.com/cosmos/cosmos-sdk/crypto/keyring"
	"github.com/cosmos/cosmos-sdk/simapp/params"
	grpc1 "github.com/gogo/protobuf/grpc"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	tmclienthttp "github.com/tendermint/tendermint/rpc/client/http"
	ctypes "github.com/tendermint/tendermint/rpc/core/types"
	ttypes "github.com/tendermint/tendermint/types"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	bcommon "gitlab.com/joltify/joltifychain-bridge/common"
	"gitlab.com/joltify/joltifychain-bridge/config"
	"gitlab.com/joltify/joltifychain-bridge/tssclient"
)

const (
	grpcTimeout   = time.Second * 30
	submitBackoff = time.Second * 2
	channelSize   = 2000
	capacity      = 10000
)

type CosHandler struct {
	ctx      context.Context
	wg       *sync.WaitGroup
	logger   zerolog.Logger
	encoding *params.EncodingConfig

	GrpcAddr   string
	GrpcClient grpc1.ClientConn
	GrpcLock   *sync.RWMutex

	httpAddr string
	wsClient *tmclienthttp.HTTP

	status        atomic.Int32
	locker        *sync.RWMutex
	wgSub         *sync.WaitGroup
	reSubChannel  chan struct{}
	currentHeight int64

	tssServer tssclient.TssInstance
	Keyring   keyring.Keyring

	ChainId   string
	ChainType string

	currentNewBlockChan   <-chan ctypes.ResultEvent
	ChannelQueueNewBlock  chan *ttypes.Header
	currentNewValidator   <-chan ctypes.ResultEvent
	ChannelQueueValidator chan *ttypes.Header
}

// the last param is used for test
func NewCosOperations(ctx context.Context, wg *sync.WaitGroup, grpcAddr, httpAddr, chainType string, keyring keyring.Keyring, tssServer tssclient.TssInstance, grpcClients ...grpc1.ClientConn) (*CosHandler, error) {
	encode := bcommon.MakeEncodingConfig()
	var grpcClient grpc1.ClientConn
	if len(grpcClients) > 0 {
		grpcClient = grpcClients[0]
	} else {
		var err error
		grpcClient, err = grpc.Dial(grpcAddr, grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			return nil, err
		}
	}

	ts := tmservice.NewServiceClient(grpcClient)
	ctxQeury, cancel := context.WithTimeout(context.Background(), grpcTimeout)
	defer cancel()

	nodeInfo, err := ts.GetNodeInfo(ctxQeury, &tmservice.GetNodeInfoRequest{})
	if err != nil {
		return nil, err
	}

	handler := &CosHandler{
		ctx:      ctx,
		wg:       wg,
		logger:   log.With().Str("module", chainType+"Handler").Logger(),
		encoding: &encode,

		GrpcAddr:   grpcAddr,
		GrpcClient: grpcClient,
		GrpcLock:   &sync.RWMutex{},

		httpAddr: httpAddr,

		locker:       &sync.RWMutex{},
		wgSub:        &sync.WaitGroup{},
		reSubChannel: make(chan struct{}, 1),

		Keyring:   keyring,
		tssServer: tssServer,

		ChainId:   nodeInfo.DefaultNodeInfo.Network,
		ChainType: chainType,

		ChannelQueueNewBlock: make(chan *ttypes.Header, channelSize),
	}
	handler.initJoltify()

	return handler, nil
}

// IsJoltify check if the handler is joltify
func (cs *CosHandler) IsJoltify() bool {
	return cs.ChainType == config.JOLTIFY
}

func (cs *CosHandler) IsRunning() bool {
	return cs.status.Load() == statusRunning
}

func (cs *CosHandler) initJoltify() {
	if !cs.IsJoltify() {
		return
	}
	cs.ChannelQueueValidator = make(chan *ttypes.Header, channelSize)
}

func (cs *CosHandler) TerminateTss() {
	// FIXME: will tssServer be stopped multiple times?
	if cs.tssServer != nil {
		cs.tssServer.Stop()
	}
}

func (cs *CosHandler) Block(ctx context.Context, height *int64) (*ttypes.Block, error) {
	block, err := cs.wsClient.Block(ctx, height)
	if err != nil {
		return nil, err
	}
	return block.Block, nil
}
