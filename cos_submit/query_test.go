package cossubmit

import (
	"context"
	"strconv"
	"sync"

	"github.com/cosmos/cosmos-sdk/crypto/keyring"
)

func (h *cosSubmitTestSuite) TestQuery() {
	accs, err := generateRandomPrivKey(3)
	h.Require().NoError(err)
	mockTss := TssMock{
		accs[0].sk,
		h.network.Validators[0].ClientCtx.Keyring,
		true,
		true,
	}
	wg := &sync.WaitGroup{}
	oc, err := NewCosOperations(context.Background(), wg, h.network.Validators[0].AppConfig.GRPC.Address, h.network.Validators[0].RPCAddress, "tset", nil, &mockTss)
	h.Require().NoError(err)
	ret := oc.GetTssNodeID()
	h.Require().Equal("mock", ret)
	_, err = oc.KeyGen([]string{"test"}, 32, "whaterver")
	h.Require().NoError(err)

	h.validatorkey = keyring.NewInMemory(h.cfg.Codec)
	vals, err := h.network.Validators[0].ClientCtx.Keyring.ExportPrivKeyArmor("node0", "1234567890")
	h.Require().NoError(err)
	oc.Keyring = h.validatorkey
	err = oc.SetKey("node0", []byte(vals), []byte("1234567890"))
	h.Require().NoError(err)
	_, err = oc.GetKey("node0")
	h.Require().NoError(err)

	_, err = oc.GetLastBlockHeightWithLock()
	h.Require().NoError(err)
	_, err = oc.GetValidators(strconv.FormatInt(2, 10))
	h.Require().ErrorContains(err, "not found")
	err = oc.Terminate()
	h.Require().NoError(err)
	oc.TerminateTss()
}
