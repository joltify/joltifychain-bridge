package cossubmit

import (
	"context"
	"fmt"
	"strconv"
	"sync"
	"testing"
	"time"

	"github.com/cosmos/cosmos-sdk/client/grpc/tmservice"
	"github.com/cosmos/cosmos-sdk/crypto/hd"
	"github.com/cosmos/cosmos-sdk/crypto/keyring"
	"github.com/cosmos/cosmos-sdk/crypto/keys/ed25519"
	"github.com/cosmos/cosmos-sdk/crypto/keys/secp256k1"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/types/bech32/legacybech32"
	banktypes "github.com/cosmos/cosmos-sdk/x/bank/types"
	stakingtypes "github.com/cosmos/cosmos-sdk/x/staking/types"
	"github.com/ethereum/go-ethereum/common"
	"github.com/joltify-finance/joltify_lending/testutil/network"
	pricefeedtypes "github.com/joltify-finance/joltify_lending/x/third_party/pricefeed/types"
	vaulttypes "github.com/joltify-finance/joltify_lending/x/vault/types"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"

	bcommon "gitlab.com/joltify/joltifychain-bridge/common"
	"gitlab.com/joltify/joltifychain-bridge/config"
	"gitlab.com/joltify/joltifychain-bridge/misc"
	"gitlab.com/joltify/joltifychain-bridge/tssclient"
)

type cosSubmitTestSuite struct {
	suite.Suite
	cfg          network.Config
	network      *network.Network
	validatorkey keyring.Keyring
	queryClient  tmservice.ServiceClient
}

type Account struct {
	sk       *secp256k1.PrivKey
	pk       string
	joltAddr sdk.AccAddress
	commAddr common.Address
}

func generateRandomPrivKey(n int) ([]Account, error) {
	randomAccounts := make([]Account, n)
	for i := 0; i < n; i++ {
		sk := secp256k1.GenPrivKey()
		pk := legacybech32.MustMarshalPubKey(legacybech32.AccPK, sk.PubKey()) // nolint

		ethAddr, err := misc.PoolPubKeyToEthAddress(pk)
		if err != nil {
			return nil, err
		}
		addrOppy, err := sdk.AccAddressFromHexUnsafe(sk.PubKey().Address().String())
		if err != nil {
			return nil, err
		}
		tAccount := Account{
			sk,
			pk,
			addrOppy,
			ethAddr,
		}
		randomAccounts[i] = tAccount
	}
	return randomAccounts, nil
}

func genNValidator(n int, validatorky keyring.Keyring) ([]stakingtypes.Validator, error) {
	var validators []stakingtypes.Validator
	var uid string
	for i := 0; i < n; i++ {
		if i == 0 {
			uid = "operator"
		} else {
			uid = "o" + strconv.Itoa(i)
		}
		info, _, err := validatorky.NewMnemonic(uid, keyring.English, sdk.FullFundraiserPath, keyring.DefaultBIP39Passphrase, hd.Secp256k1)
		if err != nil {
			return nil, err
		}

		pubkey, err := info.GetPubKey()
		if err != nil {
			return nil, err
		}
		operator, err := sdk.ValAddressFromHex(pubkey.Address().String())
		if err != nil {
			return nil, err
		}
		desc := stakingtypes.NewDescription("tester", "testId", "www.test.com", "aaa", "aaa")
		testValidator, err := stakingtypes.NewValidator(operator, pubkey, desc)
		if err != nil {
			return nil, err
		}
		validators = append(validators, testValidator)
	}
	return validators, nil
}

func (h *cosSubmitTestSuite) SetupSuite() {
	misc.SetupBech32Prefix()
	cfg := network.DefaultConfig()
	cfg.BondDenom = "stake"
	cfg.BondedTokens = sdk.NewInt(10000000000000000)
	cfg.StakingTokens = sdk.NewInt(100000000000000000)
	cfg.MinGasPrices = "0stake"
	h.cfg = cfg
	h.validatorkey = keyring.NewInMemory(cfg.Codec)
	// now we put the mock pool list in the test
	state := vaulttypes.GenesisState{}
	stateStaking := stakingtypes.GenesisState{}

	// we add the price for the tokens
	priceFeed := pricefeedtypes.GenesisState{}

	bnbPrice := pricefeedtypes.PostedPrice{
		MarketID:      "bnb:usd",
		OracleAddress: sdk.AccAddress("mock"),
		Price:         sdk.NewDecWithPrec(2571, 1),
		Expiry:        time.Now().Add(time.Hour),
	}

	joltPrice := pricefeedtypes.PostedPrice{
		MarketID:      "jolt:usd",
		OracleAddress: sdk.AccAddress("mock"),
		Price:         sdk.NewDecWithPrec(12, 1),
		Expiry:        time.Now().Add(time.Hour),
	}

	priceFeed.PostedPrices = pricefeedtypes.PostedPrices{bnbPrice, joltPrice}
	priceFeed.Params = pricefeedtypes.Params{Markets: pricefeedtypes.GenDefaultMarket()}

	bufPriceFeed, err := cfg.Codec.MarshalJSON(&priceFeed)
	h.Require().NoError(err)
	cfg.GenesisState[pricefeedtypes.ModuleName] = bufPriceFeed

	h.Require().NoError(cfg.Codec.UnmarshalJSON(cfg.GenesisState[vaulttypes.ModuleName], &state))
	h.Require().NoError(cfg.Codec.UnmarshalJSON(cfg.GenesisState[stakingtypes.ModuleName], &stateStaking))

	validators, err := genNValidator(3, h.validatorkey)
	h.Require().NoError(err)
	for i := 1; i < 5; i++ {
		randPoolSk := ed25519.GenPrivKey()
		poolPubKey, err := legacybech32.MarshalPubKey(legacybech32.AccPK, randPoolSk.PubKey()) // nolint
		h.Require().NoError(err)

		var nodes []sdk.AccAddress
		for _, el := range validators {
			operator, err := sdk.ValAddressFromBech32(el.OperatorAddress)
			if err != nil {
				panic(err)
			}
			nodes = append(nodes, operator.Bytes())
		}
		pro := vaulttypes.PoolProposal{
			PoolPubKey: poolPubKey,
			PoolAddr:   randPoolSk.PubKey().Address().Bytes(),
			Nodes:      nodes,
		}
		state.CreatePoolList = append(state.CreatePoolList, &vaulttypes.CreatePool{BlockHeight: strconv.Itoa(i), Validators: validators, Proposal: []*vaulttypes.PoolProposal{&pro}})
	}
	state.LatestTwoPool = state.CreatePoolList[:2]
	testToken := vaulttypes.IssueToken{
		Index: "testindex",
	}
	state.IssueTokenList = append(state.IssueTokenList, &testToken)

	buf, err := cfg.Codec.MarshalJSON(&state)
	h.Require().NoError(err)
	cfg.GenesisState[vaulttypes.ModuleName] = buf

	var stateVault stakingtypes.GenesisState
	h.Require().NoError(cfg.Codec.UnmarshalJSON(cfg.GenesisState[stakingtypes.ModuleName], &stateVault))
	stateVault.Params.MaxValidators = 3
	state.Params.BlockChurnInterval = 1
	buf, err = cfg.Codec.MarshalJSON(&stateVault)
	h.Require().NoError(err)
	cfg.GenesisState[stakingtypes.ModuleName] = buf

	stateBank := banktypes.GenesisState{}
	require.NoError(h.T(), cfg.Codec.UnmarshalJSON(cfg.GenesisState[banktypes.ModuleName], &stateBank))

	info, err := h.validatorkey.Key("o1")
	require.NoError(h.T(), err)
	pk, _ := info.GetPubKey()
	pkstr := legacybech32.MustMarshalPubKey(legacybech32.AccPK, pk) // nolint
	valAddr, err := misc.PoolPubKeyToJoltifyAddress(pkstr)
	fmt.Printf(">>>>operator address we put is %v\n", valAddr.String())

	stateBank.Balances = []banktypes.Balance{
		{Address: "jolt1txtsnx4gr4effr8542778fsxc20j5vzqxet7t0", Coins: sdk.Coins{sdk.NewCoin("stake", sdk.NewInt(100000))}},
		{Address: valAddr.String(), Coins: sdk.Coins{sdk.NewCoin("stake", sdk.NewInt(100000))}},
	}
	bankBuf, err := cfg.Codec.MarshalJSON(&stateBank)
	require.NoError(h.T(), err)
	cfg.GenesisState[banktypes.ModuleName] = bankBuf

	h.network = network.New(h.T(), cfg)

	h.Require().NotNil(h.network)

	_, err = h.network.WaitForHeight(1)
	h.Require().Nil(err)
	h.queryClient = tmservice.NewServiceClient(h.network.Validators[0].ClientCtx)
}

func (h *cosSubmitTestSuite) TestWaitAndSend() {
	wg := &sync.WaitGroup{}
	handler, err := NewCosOperations(context.Background(), wg, h.network.Validators[0].AppConfig.GRPC.Address, h.network.Validators[0].RPCAddress, config.ATOM, nil, nil)
	h.Require().NoError(err)
	handler.GrpcClient = h.network.Validators[0].ClientCtx
	info, _ := h.network.Validators[0].ClientCtx.Keyring.Key("node0")
	pk, _ := info.GetPubKey()
	pkstr := legacybech32.MustMarshalPubKey(legacybech32.AccPK, pk) // nolint
	valAddr, err := misc.PoolPubKeyToJoltifyAddress(pkstr)
	h.Require().NoError(err)
	acc, err := bcommon.QueryAccount(handler.GrpcClient, valAddr.String(), "")
	h.Require().NoError(err)
	acc.GetSequence()

	err = handler.waitAndSend(handler.GrpcClient, valAddr.String(), acc.GetSequence())
	h.Require().NoError(err)

	err = handler.waitAndSend(handler.GrpcClient, valAddr.String(), acc.GetSequence()-1)
	h.Require().Error(err, "already passed")

	err = handler.waitAndSend(handler.GrpcClient, "mock", acc.GetSequence()-1)
	h.Require().Error(err, "invalid Account query")
	err = handler.Terminate()
	h.Require().NoError(err)
}

func (h *cosSubmitTestSuite) TestBatchComposeAndSend() {
	accs, err := generateRandomPrivKey(3)
	h.Require().NoError(err)
	tss := TssMock{
		accs[0].sk,
		h.network.Validators[0].ClientCtx.Keyring,
		true,
		true,
	}

	wg := &sync.WaitGroup{}
	handler, err := NewCosOperations(context.Background(), wg, h.network.Validators[0].AppConfig.GRPC.Address, h.network.Validators[0].RPCAddress, config.ATOM, nil, &tss)
	h.Require().NoError(err)
	// handler.GrpcClient = h.network.Validators[0].RPCClient.(*grpc.ClientConn)

	info, _ := h.network.Validators[0].ClientCtx.Keyring.Key("node0")
	pk, _ := info.GetPubKey()
	pkstr := legacybech32.MustMarshalPubKey(legacybech32.AccPK, pk) // nolint
	valAddr, err := misc.PoolPubKeyToJoltifyAddress(pkstr)
	h.Require().NoError(err)

	operatorInfo, err := h.validatorkey.Key("operator")
	h.Require().NoError(err)

	signMsg := tssclient.TssSignigMsg{
		Pk:          pkstr,
		Signers:     nil,
		BlockHeight: 10,
		Version:     tssclient.TssVersion,
	}

	acc, err := bcommon.QueryAccount(handler.GrpcClient, valAddr.String(), "")
	h.Require().NoError(err)

	accAddr, err := operatorInfo.GetAddress()
	h.Require().NoError(err)
	handler.Keyring = h.network.Validators[0].ClientCtx.Keyring
	send := banktypes.NewMsgSend(valAddr, accAddr, sdk.Coins{sdk.NewCoin("stake", sdk.NewInt(100))})
	_, err = handler.BatchComposeAndSend(handler.GrpcClient, []sdk.Msg{send}, acc.GetSequence(), acc.GetAccountNumber(), &signMsg, valAddr.String(), []string{"mock"})
	h.Require().Error(err, "operator.info: key not found")

	handler.Keyring = h.validatorkey
	_, err = handler.BatchComposeAndSend(handler.GrpcClient, []sdk.Msg{send}, acc.GetSequence(), acc.GetAccountNumber(), &signMsg, valAddr.String(), []string{"mock"})
	h.Require().NoError(err)

	err = handler.Terminate()
	h.Require().NoError(err)
	handler.TerminateTss()
}

func (h *cosSubmitTestSuite) TestOppyChainBridge_CheckWhetherAlreadyExist() {
	wg := &sync.WaitGroup{}
	oc, err := NewCosOperations(context.Background(), wg, h.network.Validators[0].AppConfig.GRPC.Address, h.network.Validators[0].RPCAddress, config.ATOM, nil, nil)
	h.Require().NoError(err)
	oc.GrpcClient = h.network.Validators[0].ClientCtx
	ret := oc.CheckWhetherIssueTokenAlreadyExist(oc.GrpcClient, "testindex")
	h.Require().True(ret)

	ret = oc.CheckWhetherIssueTokenAlreadyExist(oc.GrpcClient, "testindexnoexist")
	h.Require().False(ret)

	err = oc.Terminate()
	h.Require().NoError(err)
}

func (h *cosSubmitTestSuite) TestCheckTxStatus() {
	wg := &sync.WaitGroup{}
	oc, err := NewCosOperations(context.Background(), wg, h.network.Validators[0].AppConfig.GRPC.Address, h.network.Validators[0].RPCAddress, config.ATOM, nil, nil)
	h.Require().NoError(err)

	// oc.GrpcClient = h.queryClient
	err = oc.CheckIssueTokenTxStatus(oc.GrpcClient, "testindex", 1)
	h.Require().NoError(err)
	err = oc.CheckIssueTokenTxStatus(oc.GrpcClient, "testindexnoexist", 1)
	h.Require().Error(err)

	err = oc.Terminate()
	h.Require().NoError(err)
}

func (h *cosSubmitTestSuite) TestGetKeysAndIDs() {
	accs, err := generateRandomPrivKey(3)
	h.Require().NoError(err)
	mockTss := TssMock{
		accs[0].sk,
		h.network.Validators[0].ClientCtx.Keyring,
		true,
		true,
	}
	wg := &sync.WaitGroup{}
	oc, err := NewCosOperations(context.Background(), wg, h.network.Validators[0].AppConfig.GRPC.Address, h.network.Validators[0].RPCAddress, config.ATOM, nil, &mockTss)
	h.Require().NoError(err)
	ret := oc.GetTssNodeID()
	h.Require().Equal("mock", ret)
	_, err = oc.KeyGen([]string{"test"}, 32, "whaterver")
	h.Require().NoError(err)

	h.validatorkey = keyring.NewInMemory(h.cfg.Codec)
	vals, err := h.network.Validators[0].ClientCtx.Keyring.ExportPrivKeyArmor("node0", "1234567890")
	h.Require().NoError(err)
	oc.Keyring = h.validatorkey
	err = oc.SetKey("node0", []byte(vals), []byte("1234567890"))
	h.Require().NoError(err)
	_, err = oc.GetKey("node0")
	h.Require().NoError(err)

	err = oc.Terminate()
	h.Require().NoError(err)
	oc.TerminateTss()
}

func TestHelper(t *testing.T) {
	suite.Run(t, new(cosSubmitTestSuite))
}
