package cossubmit

import (
	"context"
	"fmt"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/rs/zerolog/log"
	tmclienthttp "github.com/tendermint/tendermint/rpc/client/http"
	ctypes "github.com/tendermint/tendermint/rpc/core/types"
	ttypes "github.com/tendermint/tendermint/types"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

// TODO: merge pubchain and cossubmit status
const (
	statusInit int32 = iota
	statusPending
	statusRunning
	statusStopped
)

func (cs *CosHandler) subscribe() error {
	cs.locker.Lock()
	defer cs.locker.Unlock()

	cs.logger.Info().Msg("start to subscribe")
	cs.unsubscribe()

	var (
		client     *tmclienthttp.HTTP
		grpcClient *grpc.ClientConn

		headerChan          <-chan ctypes.ResultEvent
		headerValidatorChan <-chan ctypes.ResultEvent
	)
	bf := backoff.WithMaxRetries(backoff.NewConstantBackOff(time.Second*3), 3)
	op := func() error {
		var err error
		grpcClient, err = grpc.Dial(cs.GrpcAddr, grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			return err
		}
		client, err = tmclienthttp.New(cs.httpAddr, "/websocket")
		if err != nil {
			return err
		}

		client.SetLogger(&Logger{
			Logger: log.With().Str("module", cs.ChainType+"Client").Logger(),
		})
		err = client.Start()
		if err != nil {
			return err
		}

		query := "tm.event = 'NewBlockHeader'"
		headerChan, err = client.Subscribe(context.Background(), "joltBridgeNewBlockHeader", query, capacity)
		if err != nil {
			return err
		}

		if !cs.IsJoltify() {
			return nil
		}
		query = "complete_churn.churn = 'oppy_churn'"
		headerValidatorChan, err = client.Subscribe(context.Background(), "joltBridgeChurn", query, capacity)
		if err != nil {
			return err
		}
		return nil
	}
	if err := backoff.Retry(op, bf); err != nil {
		cs.logger.Error().Err(err).Msg("fail to subscribe the new head")
		return err
	}

	cs.wsClient = client
	cs.currentNewBlockChan, cs.currentNewValidator = headerChan, headerValidatorChan
	cs.GrpcLock.Lock()
	cs.GrpcClient = grpcClient
	cs.GrpcLock.Unlock()
	// drain reSubChannel
LOOP:
	for {
		select {
		case <-cs.reSubChannel:
		default:
			break LOOP
		}
	}
	if !cs.status.CompareAndSwap(statusPending, statusRunning) {
		cs.logger.Warn().Msgf("invalid status[%v] on subscribe", cs.status.Load())
		return fmt.Errorf("invalid status")
	}
	cs.logger.Info().Msg("subscribe success")
	return nil
}

func (cs *CosHandler) unsubscribe() {
	if cs.wsClient != nil {
		cs.wsClient.Stop()
	}
	if cs.GrpcClient != nil {
		a, ok := cs.GrpcClient.(*grpc.ClientConn)
		// for the test, it is not the grpc.clientconn type, so we skip close it
		if ok && a.GetState().String() != "SHUTDOWN" {
			if err := a.Close(); err != nil {
				cs.logger.Error().Err(err).Msg("fail to close the grpc client")
			}
		}
	}
}

// forceResubscribe is used to resubscribe the chain
// will block until the resubscribe is done
func (cs *CosHandler) forceResubscribe() {
	for {
		select {
		case <-cs.ctx.Done():
			cs.status.Store(statusStopped)
			return
		default:
		}

		if err := cs.subscribe(); err != nil {
			continue
		}
		break
	}
}

func (cs *CosHandler) RetryChain(force bool) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
	defer cancel()

	if !force {
		_, err := cs.wsClient.Status(ctx)
		if err == nil {
			cs.logger.Info().Msgf("all good, we do not need to reset")
			return nil
		}
	}

	output := false
	if cs.status.Load() == statusRunning {
		cs.wgSub.Add(1)
		cs.reSubChannel <- struct{}{}
		output = true
	}

	if force {
		// wait for the resubscribe to be done
		cs.wgSub.Wait()
	}
	if output {
		cs.logger.Warn().Msgf("we renewed the chain")
	}
	return nil
}

// StartSubscription start the subscription of the chain
func (cs *CosHandler) StartSubscription() error {
	if !cs.status.CompareAndSwap(statusInit, statusPending) {
		return fmt.Errorf("invalid status[%v]", cs.status.Load())
	}
	if err := cs.subscribe(); err != nil {
		return err
	}

	cs.wg.Add(1)
	go func() {
		cs.eventListener()
		cs.wg.Done()
	}()

	cs.wg.Add(1)
	go func() {
		defer cs.wg.Done()
		for {
			select {
			case <-cs.ctx.Done():
				cs.status.Store(statusStopped)
				cs.logger.Info().Msg("shutdown the subscription channel")
				return
			case <-cs.reSubChannel:
				if cs.status.CompareAndSwap(statusRunning, statusPending) {
					go func() {
						cs.forceResubscribe()
						cs.wgSub.Done()
					}()
				} else {
					cs.wgSub.Done()
				}
			}
		}
	}()
	return nil
}

func (cs *CosHandler) Terminate() error {
	cs.unsubscribe()
	// FIXME: close all the channels should be better?
	return nil
}

func (cs *CosHandler) catchupBlock(headerNow *ttypes.Header) {
	if headerNow.Height-cs.currentHeight < 2 || cs.currentHeight == 0 {
		return
	}

	fromHeight := cs.currentHeight + 1
	cs.logger.Info().Msgf("we are catching up the block from[%v] to(%v)", fromHeight, headerNow.Height)

	bf := backoff.WithMaxRetries(backoff.NewConstantBackOff(time.Second*3), 3)
	var block *ctypes.ResultBlock
	op := func() error {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
		var err error
		height := cs.currentHeight + 1
		block, err = cs.wsClient.Block(ctx, &height)
		cancel()
		return err
	}

	for headerNow.Height-cs.currentHeight > 1 {
		err := backoff.Retry(op, bf)
		if err != nil {
			cs.logger.Warn().Err(err).Msgf("fail to get the block[%v]", cs.currentHeight+1)
			continue
		}

		cs.GetChannelQueueNewBlockChan() <- &block.Block.Header
		cs.currentHeight = block.Block.Header.Height
	}
	cs.logger.Info().Msgf("we caught the block from[%v] to(%v)", fromHeight, headerNow.Height)
}

func (cs *CosHandler) eventListener() {
	duration := 40 * time.Second // let client try 5 times
	ticker := time.NewTicker(duration)

	for {
		select {
		case event := <-cs.currentNewBlockChan:
			block := event.Data.(ttypes.EventDataNewBlockHeader)
			// NOTE: joltify change role as a sync clock
			// the most important thing is the latest block
			// so we don't need to catchup the block
			if !cs.IsJoltify() {
				cs.catchupBlock(&block.Header)
			}
			cs.GetChannelQueueNewBlockChan() <- &block.Header
			cs.currentHeight = block.Header.Height
			ticker.Reset(duration)
		case event := <-cs.currentNewValidator:
			header, ok := event.Data.(ttypes.EventDataNewBlockHeader)
			if !ok {
				continue
			}
			// NOTE: we don't need to catchup validator event
			// just let it go, will handle at next round
			cs.GetChannelQueueNewValidator() <- &header.Header
			cs.logger.Info().Msgf("received a new validator event[%v]", header.Header.Height)
		case <-ticker.C:
			cs.logger.Error().Msgf("it has been %v since the last message received, will attempt to reconnect", duration)
			cs.RetryChain(true)
			ticker.Reset(duration)
		case <-cs.ctx.Done():
			return
		}
	}
}
