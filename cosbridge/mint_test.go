package cosbridge

import (
	"context"
	"strconv"
	"sync"
	"testing"
	"time"

	"github.com/cosmos/cosmos-sdk/client"
	grpc1 "github.com/gogo/protobuf/grpc"
	"gitlab.com/joltify/joltifychain-bridge/config"
	"gitlab.com/joltify/joltifychain-bridge/tokenlist"

	"github.com/cosmos/cosmos-sdk/client/grpc/tmservice"
	"github.com/cosmos/cosmos-sdk/crypto/keyring"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/types/bech32/legacybech32" // nolint
	"github.com/cosmos/cosmos-sdk/types/tx/signing"
	xauthsigning "github.com/cosmos/cosmos-sdk/x/auth/signing"
	banktypes "github.com/cosmos/cosmos-sdk/x/bank/types"
	stakingtypes "github.com/cosmos/cosmos-sdk/x/staking/types"
	"github.com/joltify-finance/joltify_lending/testutil/network"
	vaulttypes "github.com/joltify-finance/joltify_lending/x/vault/types"
	"github.com/stretchr/testify/suite"
	"gitlab.com/joltify/joltifychain-bridge/common"
	"gitlab.com/joltify/joltifychain-bridge/misc"
)

type MintTestSuite struct {
	suite.Suite
	cfg         network.Config
	network     *network.Network
	validatorky keyring.Keyring
	queryClient tmservice.ServiceClient
	grpc        grpc1.ClientConn
	privKeys    []Account
}

func (v *MintTestSuite) SetupSuite() {
	misc.SetupBech32Prefix()
	cfg := network.DefaultConfig()
	cfg.BondDenom = "stake"
	cfg.BondedTokens = sdk.NewInt(10000000000000000)
	cfg.StakingTokens = sdk.NewInt(100000000000000000)
	config.ChainID = cfg.ChainID
	cfg.MinGasPrices = "0stake"
	v.validatorky = keyring.NewInMemory(cfg.Codec)
	// now we put the mock pool list in the test
	state := vaulttypes.GenesisState{}
	stateStaking := stakingtypes.GenesisState{}

	v.Require().NoError(cfg.Codec.UnmarshalJSON(cfg.GenesisState[vaulttypes.ModuleName], &state))
	v.Require().NoError(cfg.Codec.UnmarshalJSON(cfg.GenesisState[stakingtypes.ModuleName], &stateStaking))

	validators, err := genNValidator(3, v.validatorky)
	v.Require().NoError(err)
	v.privKeys, err = generateRandomPrivKey(3)
	v.Require().NoError(err)
	for i := 0; i < 3; i++ {
		var nodes []sdk.AccAddress
		for _, el := range validators {
			operator, err := sdk.ValAddressFromBech32(el.OperatorAddress)
			if err != nil {
				panic(err)
			}
			nodes = append(nodes, operator.Bytes())
		}
		pro := vaulttypes.PoolProposal{
			PoolPubKey: v.privKeys[i].pk,
			PoolAddr:   v.privKeys[i].joltAddr,
			Nodes:      nodes,
		}
		state.CreatePoolList = append(state.CreatePoolList, &vaulttypes.CreatePool{BlockHeight: strconv.Itoa(i), Validators: validators, Proposal: []*vaulttypes.PoolProposal{&pro}})
	}
	state.LatestTwoPool = state.CreatePoolList[:2]
	testToken := vaulttypes.IssueToken{
		Index: "testindex",
	}
	state.IssueTokenList = append(state.IssueTokenList, &testToken)

	state.Params.BlockChurnInterval = 1
	state.Params.TargetQuota, err = sdk.ParseCoinsNormalized("10000stake,10000test")
	v.Require().NoError(err)
	buf, err := cfg.Codec.MarshalJSON(&state)
	v.Require().NoError(err)
	cfg.GenesisState[vaulttypes.ModuleName] = buf

	v.Require().NoError(cfg.Codec.UnmarshalJSON(cfg.GenesisState[stakingtypes.ModuleName], &stateStaking))
	stateStaking.Params.MaxValidators = 3
	buf, err = cfg.Codec.MarshalJSON(&stateStaking)
	v.Require().NoError(err)
	cfg.GenesisState[stakingtypes.ModuleName] = buf

	v.network = network.New(v.T(), cfg)
	v.cfg = cfg

	v.Require().NotNil(v.network)

	_, err = v.network.WaitForHeight(1)
	v.Require().Nil(err)
	v.grpc = v.network.Validators[0].ClientCtx
	v.queryClient = tmservice.NewServiceClient(v.network.Validators[0].ClientCtx)
}

func (m MintTestSuite) TestPrepareIssueTokenRequest() {
	accs, err := generateRandomPrivKey(3)
	m.Require().NoError(err)
	tx := common.NewAccountInboundReq(accs[0].joltAddr, sdk.NewCoin("test", sdk.NewInt(1)), []byte("test"), int64(2))
	_, err = prepareIssueTokenRequest(&tx, accs[2].commAddr.String(), "1")
	m.Require().EqualError(err, "decoding bech32 failed: string not all lowercase or all uppercase")

	_, err = prepareIssueTokenRequest(&tx, accs[2].joltAddr.String(), "1")

	m.Require().NoError(err)
}

func Gensigntx(oc *JoltChainInstance, sdkMsg []sdk.Msg, key *keyring.Record, accNum, accSeq uint64, signkeyring keyring.Keyring, memo string) (client.TxBuilder, error) {
	encCfg := *oc.encoding
	// Create a new TxBuilder.
	txBuilder := encCfg.TxConfig.NewTxBuilder()

	err := txBuilder.SetMsgs(sdkMsg...)
	if err != nil {
		return nil, err
	}

	// we use the default here
	txBuilder.SetGasLimit(500000)
	txBuilder.SetMemo(memo)

	var sigV2 signing.SignatureV2
	tpk, err := key.GetPubKey()
	if err != nil {
		return nil, err
	}
	sigV2 = signing.SignatureV2{
		PubKey: tpk,
		Data: &signing.SingleSignatureData{
			SignMode:  encCfg.TxConfig.SignModeHandler().DefaultMode(),
			Signature: nil,
		},
		Sequence: accSeq,
	}

	err = txBuilder.SetSignatures(sigV2)
	if err != nil {
		return nil, err
	}

	signerData := xauthsigning.SignerData{
		ChainID:       config.ChainID,
		AccountNumber: accNum,
		Sequence:      accSeq,
	}

	signMode := encCfg.TxConfig.SignModeHandler().DefaultMode()
	// Generate the bytes to be signed.
	signBytes, err := encCfg.TxConfig.SignModeHandler().GetSignBytes(signMode, signerData, txBuilder.GetTx())
	if err != nil {
		return nil, err
	}

	signature, pk, err := signkeyring.Sign("node0", signBytes)
	if err != nil {
		return nil, err
	}
	sigData := signing.SingleSignatureData{
		SignMode:  signMode,
		Signature: signature,
	}

	sigV2 = signing.SignatureV2{
		PubKey:   pk,
		Data:     &sigData,
		Sequence: signerData.Sequence,
	}
	err = txBuilder.SetSignatures(sigV2)
	if err != nil {
		oc.logger.Error().Err(err).Msgf("fail to set the signature")
		return nil, err
	}

	return txBuilder, nil
}

//nolint:funlen
func (m MintTestSuite) TestProcessInbound() {
	accs := m.privKeys[:2]
	tss := TssMock{
		accs[0].sk,
		// nil,
		m.network.Validators[0].ClientCtx.Keyring,
		// m.network.Validators[0].ClientCtx.Keyring,
		true,
		true,
	}
	tl, err := tokenlist.CreateMockTokenlist([]string{"test"}, []string{"stake"}, []string{config.BSC})
	m.Require().NoError(err)
	rp := common.NewRetryPools()
	wg := &sync.WaitGroup{}
	oc, err := NewJoltifyBridge(context.Background(), wg, m.network.Validators[0].APIAddress, m.network.Validators[0].RPCAddress, &tss, tl, rp, m.network.Validators[0].ClientCtx)
	m.Require().NoError(err)
	oc.CosHandler.Keyring = m.validatorky

	// we need to add this as it seems the rpcaddress is incorrect
	defer func() {
		err := oc.TerminateBridge()
		if err != nil {
			oc.logger.Error().Err(err).Msgf("fail to terminate the bridge")
		}
	}()

	_, err = m.network.WaitForHeightWithTimeout(10, time.Second*30)
	m.Require().NoError(err)

	// err = oc.InitValidators(m.network.Validators[0].APIAddress)
	// m.Require().NoError(err)

	info, _ := m.network.Validators[0].ClientCtx.Keyring.Key("node0")
	pk, err := info.GetPubKey()
	m.Require().NoError(err)
	pkstr := legacybech32.MustMarshalPubKey(legacybech32.AccPK, pk) // nolint
	valAddr, err := misc.PoolPubKeyToJoltifyAddress(pkstr)
	m.Require().NoError(err)

	acc, err := common.QueryAccount(m.grpc, valAddr.String(), m.network.Validators[0].RPCAddress)
	m.Require().NoError(err)
	tx := common.NewAccountInboundReq(valAddr, sdk.NewCoin("test", sdk.NewInt(1)), []byte("test"), int64(100))

	tx.SetAccountInfo(0, 0, accs[0].joltAddr, accs[0].pk)

	send := banktypes.NewMsgSend(valAddr, accs[0].joltAddr, sdk.Coins{sdk.NewCoin("stake", sdk.NewInt(1))})

	txBuilder, err := Gensigntx(oc, []sdk.Msg{send}, info, acc.GetAccountNumber(), acc.GetSequence(), m.network.Validators[0].ClientCtx.Keyring, "")
	m.Require().NoError(err)
	txBytes, err := oc.encoding.TxConfig.TxEncoder()(txBuilder.GetTx())
	m.Require().NoError(err)
	ret, _, err := oc.CosHandler.BroadcastTx(context.Background(), m.grpc, txBytes, false)
	m.Require().NoError(err)
	m.Require().True(ret)

	pool := common.PoolInfo{
		Pk:         accs[0].pk,
		CosAddress: accs[0].joltAddr,
		EthAddress: accs[0].commAddr,
	}

	oc.lastTwoPools[0] = &pool
	oc.lastTwoPools[1] = &pool

	_, _, err = oc.DoProcessInBound(m.grpc, []*common.InBoundReq{&tx})
	m.Require().NoError(err)
}

func TestMint(t *testing.T) {
	suite.Run(t, new(MintTestSuite))
}
