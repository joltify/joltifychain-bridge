package storage

import (
	"encoding/json"
	"errors"
	"os"
	"path/filepath"
	"sync"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	bcommon "gitlab.com/joltify/joltifychain-bridge/common"
)

// PendingMoveFundMgr save the local state to file
type PendingMoveFundMgr struct {
	folder           string
	writePendingLock *sync.RWMutex
	logger           zerolog.Logger
}

// NewMoveFundStateMgr creates a new instance of the FileStateMgr which implements LocalStateManager
func NewMoveFundStateMgr(folder string) *PendingMoveFundMgr {
	logger := log.With().Str("module", "movefundsave").Logger()
	return &PendingMoveFundMgr{
		folder:           folder,
		writePendingLock: &sync.RWMutex{},
		logger:           logger,
	}
}

func (fsm *PendingMoveFundMgr) SavePendingItems(pendingTxsPub []*bcommon.MoveFundItem) error {
	fsm.writePendingLock.Lock()
	defer fsm.writePendingLock.Unlock()

	pubfile := filepath.Join(fsm.folder, "movefundpending_pub.dat")
	os.Rename(pubfile, pubfile+".bak")
	bufPub, err := json.Marshal(pendingTxsPub)
	if err != nil {
		fsm.logger.Error().Err(err).Msgf("fail to marshal the inbound pending tx")
		return err
	}

	err = os.WriteFile(pubfile, bufPub, 0o600)
	if err != nil {
		fsm.logger.Error().Err(err).Msgf("fail to load the pub move fund")
		return err
	}

	return nil
}

func (fsm *PendingMoveFundMgr) LoadPendingItems() ([]*bcommon.MoveFundItem, error) {
	if len(fsm.folder) < 1 {
		return nil, errors.New("base file path is invalid")
	}
	pubFilePathName := filepath.Join(fsm.folder, "movefundpending_pub.dat")
	_, err := os.Stat(pubFilePathName)
	if err != nil {
		return nil, err
	}

	fsm.writePendingLock.RLock()
	inputPub, err := os.ReadFile(pubFilePathName)
	if err != nil || len(inputPub) == 0 {
		fsm.writePendingLock.RUnlock()
		return nil, err
	}

	fsm.writePendingLock.RUnlock()
	var moveFundPendingPub []*bcommon.MoveFundItem
	err = json.Unmarshal(inputPub, &moveFundPendingPub)
	if err != nil {
		fsm.logger.Error().Err(err).Msgf("fail to unmarshal the move fund pending tx")
	}

	return moveFundPendingPub, nil
}
