package common

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMakeEncodingConfig(t *testing.T) {
	ret := MakeEncodingConfig()
	assert.NotNil(t, ret.TxConfig)
	assert.NotNil(t, ret.Amino)
}
