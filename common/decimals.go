package common

import (
	"math/big"

	"cosmossdk.io/math"
	sdk "github.com/cosmos/cosmos-sdk/types"
)

func AdjustInt(input math.Int, deltaPrecision int64) math.Int {
	val := new(big.Int).Exp(big.NewInt(10), new(big.Int).Abs(big.NewInt(deltaPrecision)), nil)
	if deltaPrecision < 0 {
		return input.Quo(sdk.NewIntFromBigInt(val))
	}
	return input.Mul(sdk.NewIntFromBigInt(val))
}
