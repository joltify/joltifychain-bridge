package common

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPool(t *testing.T) {
	mockTxID := "mockTxID"
	m := InBoundReq{
		TxID: []byte(mockTxID),
	}
	ret := m.HexTxID()
	assert.Equal(t, ret, "0x0000000000000000000000000000000000000000000000006d6f636b54784944")

	retrypool := NewRetryPools()
	retrypool.RetryOutboundReq.Store("1", 1)
	ret2, _ := retrypool.RetryOutboundReq.Load("1")
	assert.Equal(t, ret2, 1)
}
