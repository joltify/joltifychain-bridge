package common

import (
	"encoding/hex"
	"strconv"

	"github.com/cosmos/cosmos-sdk/types"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
)

func (o *OutBoundReq) Hash() common.Hash {
	data, err := hex.DecodeString(o.TxID)
	if err != nil {
		panic(err)
	}
	needMintStr := "true"
	if !o.NeedMint {
		needMintStr = "false"
	}

	hash := crypto.Keccak256Hash(o.OutReceiverAddress, []byte(o.ChainType), []byte(needMintStr), data)
	return hash
}

func NewOutboundReq(txID string, address []byte, fromPoolAddr []byte, coin types.Coin, coinAddr string, txBlockHeight int64, feeToValidator types.Coins, chainType string, needMint bool) OutBoundReq {
	return OutBoundReq{
		txID,
		address,
		fromPoolAddr,
		coin,
		coinAddr,
		txBlockHeight,
		uint64(0),
		uint64(0),
		"",
		common.Hash{}.Hex(),
		feeToValidator,
		chainType,
		needMint,
	}
}

// Index generate the index of a given inbound req
func (o *OutBoundReq) Index() string {
	data, err := hex.DecodeString(o.TxID)
	if err != nil {
		panic(err)
	}
	hash := crypto.Keccak256Hash(o.OutReceiverAddress, data)
	lower := hash.Big().String()
	higher := strconv.FormatInt(o.BlockHeight, 10)
	indexStr := higher + lower
	return indexStr
}

// SetItemNonce sets the block height of the tx
func (o *OutBoundReq) SetItemNonce(fromPoolAddr []byte, nonce uint64, fromPk string, accNum uint64) {
	o.FromPoolAddr = fromPoolAddr
	o.FromPubkey = fromPk
	o.AccNum = accNum
	o.Nonce = nonce
}

func (i *InBoundReq) Hash() common.Hash {
	hash := crypto.Keccak256Hash(i.UserReceiverAddress.Bytes(), i.TxID)
	return hash
}

// Index generate the index of a given inbound req
func (i *InBoundReq) Index() string {
	hash := crypto.Keccak256Hash(i.UserReceiverAddress.Bytes(), i.TxID)
	lower := hash.Big().String()
	higher := strconv.FormatInt(i.BlockHeight, 10)
	indexStr := higher + lower
	return indexStr
}

func NewAccountInboundReq(address types.AccAddress, coin types.Coin, txid []byte, blockHeight int64) InBoundReq {
	return InBoundReq{
		address,
		txid,
		coin,
		blockHeight,
		0,
		0,
		nil,
		"",
	}
}

// GetInboundReqInfo returns the info of the inbound transaction
func (i *InBoundReq) GetInboundReqInfo() (types.AccAddress, types.Coin, int64) {
	return i.UserReceiverAddress, i.Coin, i.BlockHeight
}

// SetAccountInfo sets the block height of the tx
func (i *InBoundReq) SetAccountInfo(number, seq uint64, address types.AccAddress, pk string) {
	i.AccNum = number
	i.AccSeq = seq
	i.PoolCosAddress = address
	i.PoolPk = pk
}

// GetAccountInfo returns the account number and seq
func (i *InBoundReq) GetAccountInfo() (uint64, uint64, types.AccAddress, string) {
	return i.AccSeq, i.AccNum, i.PoolCosAddress, i.PoolPk
}
