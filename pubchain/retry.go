package pubchain

import (
	"context"
	"fmt"
	"math/big"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
)

// subscribeNewHead is used to subscribe the block event
func (c *Erc20ChainInfo) subscribeNewHead() error {
	bf := backoff.WithMaxRetries(backoff.NewConstantBackOff(time.Second*3), 3)
	var handler ethereum.Subscription
	op := func() error {
		var err error
		handler, err = c.Client.SubscribeNewHead(c.Ctx, c.SubChannelNow)
		return err
	}

	if err := backoff.Retry(op, bf); err != nil {
		return err
	}
	c.SubHandler = handler
	return nil
}

// StartSubscription start the subscription of the token
func (c *Erc20ChainInfo) StartSubscription() error {
	if !c.status.CompareAndSwap(statusInit, statusPending) {
		return fmt.Errorf("invalid status[%v]", c.status.Load())
	}
	if err := c.subscribe(); err != nil {
		return err
	}

	c.wg.Add(1)
	go func() {
		defer c.wg.Done()
		for {
			select {
			case <-c.Ctx.Done():
				c.status.Store(statusStopped)
				c.logger.Info().Msg("shutdown the subscription channel")
				return
			case <-c.reSubChannel:
				if c.status.CompareAndSwap(statusRunning, statusPending) {
					go c.forceResubscribe()
				} else {
					c.wgSub.Done()
				}
			}
		}
	}()
	return nil
}

func (c *Erc20ChainInfo) RetryPubChain(force bool) error {
	if !force {
		err := c.CheckChainHealthWithLock()
		if err == nil {
			c.logger.Info().Msgf("all good we do not need to reset")
			return nil
		}
	}

	output := false
	if c.status.Load() == statusRunning {
		c.wgSub.Add(1)
		c.reSubChannel <- struct{}{}
		output = true
	}

	if force {
		// wait for the resubscribe to be done
		c.wgSub.Wait()
	}
	if output {
		c.logger.Warn().Msgf("we renewed the chain")
	}
	return nil
}

// forceResubscribe is used to resubscribe the chain
// will block until the resubscribe is done
func (c *Erc20ChainInfo) forceResubscribe() {
	defer c.wgSub.Done()
	for {
		select {
		case <-c.Ctx.Done():
			c.status.Store(statusStopped)
			return
		default:
		}

		if err := c.subscribe(); err != nil {
			continue
		}
		break
	}
}

func (c *Erc20ChainInfo) subscribe() error {
	c.ChainLocker.Lock()
	defer c.ChainLocker.Unlock()

	c.logger.Info().Msg("start to subscribe")
	c.unsubscribe()

	bf := backoff.WithMaxRetries(backoff.NewConstantBackOff(time.Second*3), 3)
	var ethClient *ethclient.Client
	op := func() error {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
		var err error
		ethClient, err = ethclient.DialContext(ctx, c.WsAddr)
		cancel()
		return err
	}

	if err := backoff.Retry(op, bf); err != nil {
		c.logger.Error().Err(err).Msg("fail to dial the websocket")
		return err
	}

	c.Client = ethClient
	if err := c.subscribeNewHead(); err != nil {
		c.logger.Error().Err(err).Msg("fail to subscribe the new head")
		return err
	}

	if !c.status.CompareAndSwap(statusPending, statusRunning) {
		c.logger.Warn().Msgf("invalid status[%v] on subscribe", c.status.Load())
		return fmt.Errorf("invalid status")
	}
	c.logger.Info().Msg("subscribe success")
	return nil
}

func (c *Erc20ChainInfo) unsubscribe() {
	if c.SubHandler != nil {
		c.SubHandler.Unsubscribe()
	}
	if c.Client != nil {
		c.Client.Close()
	}
}

func (c *Erc20ChainInfo) catchupBlock(pi *Instance, headNow *types.Header) {
	if c.CurrentHeight == nil || big.NewInt(0).Sub(headNow.Number, c.CurrentHeight).Int64() < 2 {
		return
	}

	fromHeight := big.NewInt(0).Add(c.CurrentHeight, big.NewInt(1))
	c.logger.Info().Msgf("we are catching up the block from[%v] to(%v)", fromHeight, headNow.Number)

	bf := backoff.WithMaxRetries(backoff.NewConstantBackOff(time.Second*3), 3)
	var head *types.Header
	op := func() error {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
		var err error
		head, err = c.Client.HeaderByNumber(ctx, big.NewInt(0).Add(c.CurrentHeight, big.NewInt(1)))
		cancel()
		return err
	}

	for big.NewInt(0).Sub(headNow.Number, c.CurrentHeight).Int64() > 1 {
		c.ChainLocker.RLock()
		err := backoff.Retry(op, bf)
		c.ChainLocker.RUnlock()
		if err != nil {
			c.logger.Warn().Err(err).Msgf("fail to get the block[%v]", big.NewInt(0).Add(c.CurrentHeight, big.NewInt(1)))
			continue
		}

		block := &BlockHead{
			Head:      head,
			ChainType: c.ChainType,
		}
		pi.ChannelQueue <- block
		c.CurrentHeight = head.Number
	}
	c.logger.Info().Msgf("we caught the block from[%v] to(%v)", fromHeight, headNow.Number)
}

func (c *Erc20ChainInfo) EvmSubscribe(pi *Instance) {
	for {
		select {
		case head := <-c.SubChannelNow:
			if head == nil {
				c.logger.Info().Msgf("received nil head")
				continue
			}
			c.catchupBlock(pi, head)
			block := &BlockHead{
				Head:      head,
				ChainType: c.ChainType,
			}
			pi.ChannelQueue <- block
			c.CurrentHeight = head.Number
		case <-c.Ctx.Done():
			c.logger.Info().Msgf("we quit the subscribe")
			return
		case err := <-c.SubHandler.Err():
			c.logger.Error().Err(err).Msgf("we fail to subscribe, will retry")
			_ = c.RetryPubChain(true)
		}
	}
}
