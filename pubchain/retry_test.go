package pubchain

import (
	"context"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"

	"gitlab.com/joltify/joltifychain-bridge/config"
	"gitlab.com/joltify/joltifychain-bridge/misc"
)

type TestRetrySuite struct {
	cancel context.CancelFunc
	suite.Suite
	pubChain *Instance
}

func (tn *TestRetrySuite) SetupSuite() {
	misc.SetupBech32Prefix()
	ctx, cancel := context.WithCancel(context.Background())
	wg := sync.WaitGroup{}
	cfg := config.Config{}
	cfg.PubChainConfig.WsAddressBSC = misc.WebsocketTest
	cfg.PubChainConfig.WsAddressETH = misc.WebsocketTest

	bscChainClient, err := NewErc20ChainInfo(ctx, cfg.PubChainConfig.WsAddressBSC, config.BSC, &wg, nil)
	tn.Require().NoError(err)
	bscChainClient.ChannelQueue = make(chan *BlockHead, sbchannelsize)

	ethChainClient, err := NewErc20ChainInfo(ctx, cfg.PubChainConfig.WsAddressETH, config.ETH, &wg, nil)
	tn.Require().NoError(err)

	ethChainClient.ChannelQueue = make(chan *BlockHead, sbchannelsize)

	pubChain := &Instance{
		wg:         &wg,
		BSCChain:   bscChainClient,
		EthChain:   ethChainClient,
		poolLocker: &sync.RWMutex{},
	}

	tn.pubChain, tn.cancel = pubChain, cancel
}

func (tn *TestRetrySuite) TestRetry() {

	err := tn.pubChain.BSCChain.StartSubscription()
	tn.Require().NoError(err)
	b := <-tn.pubChain.BSCChain.SubChannelNow
	currentBlock := b.Number.Uint64()

	time.Sleep(time.Second * 10)
	err = tn.pubChain.BSCChain.RetryPubChain(false)
	tn.Require().NoError(err)
	blockInNew := currentBlock + uint64(len(tn.pubChain.ChannelQueue))
	lwg := sync.WaitGroup{}
	lwg.Add(1)
	var lastBlock uint64
	go func() {
		defer lwg.Done()
		for {
			select {
			case b := <-tn.pubChain.BSCChain.SubChannelNow:
				lastBlock = b.Number.Uint64()

			default:
				break
			}
		}
	}()

	tn.Require().True(lastBlock-blockInNew > 3)

	tn.cancel()
	tn.pubChain.wg.Wait()
}

func TestRetryEvent(t *testing.T) {
	suite.Run(t, new(TestRetrySuite))
}
