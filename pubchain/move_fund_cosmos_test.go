package pubchain

import (
	"context"
	"crypto/ecdsa"
	"encoding/base64"
	"errors"
	"fmt"
	"math/rand"
	"strconv"
	"sync"
	"testing"
	"time"

	"github.com/cosmos/cosmos-sdk/client/grpc/tmservice"
	"github.com/cosmos/cosmos-sdk/crypto/keyring"
	"github.com/cosmos/cosmos-sdk/crypto/keys/ed25519"
	"github.com/cosmos/cosmos-sdk/crypto/keys/secp256k1"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/types/bech32/legacybech32"
	"github.com/cosmos/cosmos-sdk/types/simulation"
	acctypes "github.com/cosmos/cosmos-sdk/x/auth/types"
	banktypes "github.com/cosmos/cosmos-sdk/x/bank/types"
	stakingtypes "github.com/cosmos/cosmos-sdk/x/staking/types"
	"github.com/ethereum/go-ethereum/crypto"
	grpc1 "github.com/gogo/protobuf/grpc"
	"github.com/joltify-finance/joltify_lending/testutil/network"
	vaulttypes "github.com/joltify-finance/joltify_lending/x/vault/types"
	"github.com/joltify-finance/tss/blame"
	tcommon "github.com/joltify-finance/tss/common"
	"github.com/joltify-finance/tss/keygen"
	"github.com/joltify-finance/tss/keysign"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"google.golang.org/grpc"

	"gitlab.com/joltify/joltifychain-bridge/common"
	"gitlab.com/joltify/joltifychain-bridge/config"
	"gitlab.com/joltify/joltifychain-bridge/misc"
)

type MoveFundTssMock struct {
	sk             *secp256k1.PrivKey
	keys           keyring.Keyring
	keygenSuccess  bool
	keysignSuccess bool
}

func (tm *MoveFundTssMock) KeySign(pk string, msgs []string, blockHeight int64, signers []string, version string) (keysign.Response, error) {
	if !tm.keysignSuccess {
		return keysign.NewResponse(nil, tcommon.Fail, blame.NewBlame("", nil)), errors.New("fail in keysign")
	}

	msg, err := base64.StdEncoding.DecodeString(msgs[0])
	if err != nil {
		return keysign.Response{}, err
	}
	var sk *ecdsa.PrivateKey
	if tm.sk != nil {
		sk, err = crypto.ToECDSA(tm.sk.Bytes())
		if err != nil {
			return keysign.Response{}, err
		}
	}
	var signature []byte
	if tm.keys != nil {
		s, _, err := tm.keys.Sign("node0", msg)
		signature = s
		if err != nil {
			return keysign.Response{}, err
		}
	} else {
		signature, err = crypto.Sign(msg, sk)
		if err != nil {
			return keysign.Response{}, err
		}
	}
	r := signature[:32]
	s := signature[32:64]
	var v []byte
	if len(signature) == 64 {
		v = signature[63:]
	} else {
		v = signature[64:65]
	}

	rEncoded := base64.StdEncoding.EncodeToString(r)
	sEncoded := base64.StdEncoding.EncodeToString(s)
	vEncoded := base64.StdEncoding.EncodeToString(v)

	sig := keysign.Signature{
		Msg:        msgs[0],
		R:          rEncoded,
		S:          sEncoded,
		RecoveryID: vEncoded,
	}

	return keysign.Response{Signatures: []keysign.Signature{sig}, Status: tcommon.Success}, nil
}

func (tm *MoveFundTssMock) KeyGen(_ []string, _ int64, _ string) (keygen.Response, error) {
	if tm.keygenSuccess {
		sk := secp256k1.GenPrivKey()
		pk := legacybech32.MustMarshalPubKey(legacybech32.AccPK, sk.PubKey()) // nolint
		address, err := sdk.AccAddressFromHexUnsafe(sk.PubKey().Address().String())
		if err != nil {
			panic(err)
		}
		return keygen.NewResponse(pk, address.String(), tcommon.Success, blame.Blame{}), nil
	}
	return keygen.NewResponse("", "", tcommon.Fail, blame.Blame{}), nil
}

func (tm *MoveFundTssMock) GetTssNodeID() string {
	return "mock"
}

func (tm *MoveFundTssMock) Stop() {
}

type FeedCosmosMoveFundTestSuite struct {
	suite.Suite
	cfg         network.Config
	network     *network.Network
	validatorky keyring.Keyring
	queryClient tmservice.ServiceClient
	grpc        grpc1.ClientConn
}

func (f *FeedCosmosMoveFundTestSuite) SetupSuite() {
	misc.SetupBech32Prefix()
	cfg := network.DefaultConfig()
	cfg.BondDenom = "stake"
	cfg.BondedTokens = sdk.NewInt(10000000000000000)
	cfg.StakingTokens = sdk.NewInt(100000000000000000)
	f.cfg = cfg
	f.validatorky = keyring.NewInMemory(cfg.Codec)
	// now we put the mock pool list in the test
	state := vaulttypes.GenesisState{}
	stateStaking := stakingtypes.GenesisState{}

	f.Require().NoError(cfg.Codec.UnmarshalJSON(cfg.GenesisState[vaulttypes.ModuleName], &state))
	f.Require().NoError(cfg.Codec.UnmarshalJSON(cfg.GenesisState[stakingtypes.ModuleName], &stateStaking))

	validators, err := genNValidator(3, f.validatorky)
	f.Require().NoError(err)
	for i := 1; i < 5; i++ {
		randPoolSk := ed25519.GenPrivKey()
		poolPubKey, err := legacybech32.MarshalPubKey(legacybech32.AccPK, randPoolSk.PubKey()) // nolint
		f.Require().NoError(err)

		var nodes []sdk.AccAddress
		for _, el := range validators {
			operator, err := sdk.ValAddressFromBech32(el.OperatorAddress)
			if err != nil {
				panic(err)
			}
			nodes = append(nodes, operator.Bytes())
		}
		pro := vaulttypes.PoolProposal{
			PoolPubKey: poolPubKey,
			PoolAddr:   randPoolSk.PubKey().Address().Bytes(),
			Nodes:      nodes,
		}
		state.CreatePoolList = append(state.CreatePoolList, &vaulttypes.CreatePool{BlockHeight: strconv.Itoa(i), Validators: validators, Proposal: []*vaulttypes.PoolProposal{&pro}})
	}
	state.LatestTwoPool = state.CreatePoolList[:2]
	testToken := vaulttypes.IssueToken{
		Index: "testindex",
	}
	state.IssueTokenList = append(state.IssueTokenList, &testToken)

	buf, err := cfg.Codec.MarshalJSON(&state)
	f.Require().NoError(err)
	cfg.GenesisState[vaulttypes.ModuleName] = buf

	var stateVault stakingtypes.GenesisState
	f.Require().NoError(cfg.Codec.UnmarshalJSON(cfg.GenesisState[stakingtypes.ModuleName], &stateVault))
	stateVault.Params.MaxValidators = 3
	state.Params.BlockChurnInterval = 1
	buf, err = cfg.Codec.MarshalJSON(&stateVault)
	f.Require().NoError(err)
	cfg.GenesisState[stakingtypes.ModuleName] = buf

	addr := state.LatestTwoPool[0].Proposal[0].PoolAddr
	addr1 := state.LatestTwoPool[1].Proposal[0].PoolAddr

	acc0 := acctypes.NewBaseAccountWithAddress(state.LatestTwoPool[0].Proposal[0].PoolAddr)
	acc1 := acctypes.NewBaseAccountWithAddress(state.LatestTwoPool[1].Proposal[0].PoolAddr)

	genAccounts := acctypes.GenesisAccounts{acc0, acc1}
	accounts, err := acctypes.PackAccounts(genAccounts)

	stateAcc := acctypes.DefaultGenesisState()
	// stateAcc.Accounts = append(stateAcc.Accounts, acc1)
	stateAcc.Accounts = append(stateAcc.Accounts, accounts...)

	accBuf, err := cfg.Codec.MarshalJSON(stateAcc)
	require.NoError(f.T(), err)
	cfg.GenesisState[acctypes.ModuleName] = accBuf

	stateBank := banktypes.DefaultGenesisState()

	stateBank.Balances = append(stateBank.Balances, []banktypes.Balance{
		{Address: "jolt1txtsnx4gr4effr8542778fsxc20j5vzqxet7t0", Coins: sdk.Coins{sdk.NewCoin("uatom", sdk.NewInt(100000))}},
		{Address: addr.String(), Coins: sdk.Coins{sdk.NewCoin("uatom", sdk.NewInt(100000))}},
		{Address: addr1.String(), Coins: sdk.Coins{sdk.NewCoin("stake", sdk.NewInt(1000)), sdk.NewCoin("uatom", sdk.NewInt(100000))}},
	}...)

	bankBuf, err := cfg.Codec.MarshalJSON(stateBank)
	require.NoError(f.T(), err)
	cfg.GenesisState[banktypes.ModuleName] = bankBuf

	f.network = network.New(f.T(), cfg)
	f.Require().NotNil(f.network)

	_, err = f.network.WaitForHeight(1)
	f.Require().Nil(err)
	f.grpc = f.network.Validators[0].ClientCtx
	f.queryClient = tmservice.NewServiceClient(f.network.Validators[0].ClientCtx)
}

func createdTestInBoundReqs(n int) []*common.InBoundReq {
	r := rand.New(rand.NewSource(time.Now().Unix()))
	accs := simulation.RandomAccounts(r, n)
	retReq := make([]*common.InBoundReq, n)
	for i := 0; i < n; i++ {
		txid := fmt.Sprintf("testTXID %v", i)
		testCoin := sdk.NewCoin("test", sdk.NewInt(32))
		item := common.NewAccountInboundReq(accs[i].Address, testCoin, []byte(txid), int64(i))
		retReq[i] = &item
	}
	return retReq
}

func (f *FeedCosmosMoveFundTestSuite) TestFeedTransactions() {
	wg := sync.WaitGroup{}
	retryOutboundReq := sync.Map{}
	n := f.network.Validators[0]
	var cfg config.Config
	cfg.AtomChain.GrpcAddress = n.AppConfig.GRPC.Address
	cfg.AtomChain.HTTPAddress = n.RPCAddress
	cfg.PubChainConfig.WsAddressBSC = misc.WebsocketTest
	cfg.PubChainConfig.WsAddressETH = misc.WebsocketTest
	cfg.PubChainConfig.WsAddressAVA = misc.WebsocketTest
	cfg.PubChainConfig.WsAddressPOL = misc.WebsocketTest

	accs, err := generateRandomPrivKey(3)
	f.Require().NoError(err)

	mockTss := MoveFundTssMock{
		accs[0].sk,
		f.network.Validators[0].ClientCtx.Keyring,
		true,
		true,
	}

	ins, err := NewChainInstance(context.Background(), cfg, &mockTss, nil, &wg, &retryOutboundReq)
	f.Assert().NoError(err)

	grpcClient, err := grpc.Dial(n.AppConfig.GRPC.Address, grpc.WithInsecure())
	f.Assert().NoError(err)

	//key, err := f.network.Validators[0].ClientCtx.Keyring.Key("node0")
	//f.Assert().Nil(err)
	//_ = key

	ts := vaulttypes.NewQueryClient(grpcClient)
	ctx, cancel := context.WithTimeout(context.Background(), grpcTimeout)
	defer cancel()

	req := vaulttypes.QueryLatestPoolRequest{}
	resp, err := ts.GetLastPool(ctx, &req)
	f.Assert().NoError(err)

	pInfo := common.PoolInfo{
		Pk:         resp.Pools[0].CreatePool.PoolPubKey,
		CosAddress: resp.Pools[0].CreatePool.PoolAddr,
		PoolInfo:   resp.Pools[0],
	}

	ins.lastTwoPools[0] = &common.PoolInfo{
		Pk:         resp.Pools[0].CreatePool.PoolPubKey,
		CosAddress: resp.Pools[0].CreatePool.PoolAddr,
		PoolInfo:   resp.Pools[0],
	}

	ins.lastTwoPools[1] = &common.PoolInfo{
		Pk:         resp.Pools[1].CreatePool.PoolPubKey,
		CosAddress: resp.Pools[1].CreatePool.PoolAddr,
		PoolInfo:   resp.Pools[1],
	}

	// from and to the same, so we do not move
	ok := ins.MoveFundCosmos(100, "jolt", grpcClient, &pInfo)
	f.Assert().True(ok)
}

func TestMoveFundCosmos(t *testing.T) {
	suite.Run(t, new(FeedCosmosMoveFundTestSuite))
}
