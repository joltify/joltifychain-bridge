package pubchain

import (
	"context"
	"math/big"
	"sync"
	"testing"
	"time"

	"github.com/ethereum/go-ethereum/common"
	"github.com/stretchr/testify/suite"

	"gitlab.com/joltify/joltifychain-bridge/config"
	"gitlab.com/joltify/joltifychain-bridge/misc"
)

type TestHelperSuite struct {
	suite.Suite
	pubChain *Instance
	cancel   context.CancelFunc
}

func (tn *TestHelperSuite) SetupSuite() {
	misc.SetupBech32Prefix()
	wg := sync.WaitGroup{}
	ctx, cancel := context.WithCancel(context.Background())

	cfg := config.Config{}
	cfg.PubChainConfig.WsAddressBSC = misc.WebsocketTest
	cfg.PubChainConfig.WsAddressETH = misc.WebsocketTest

	bscChainClient, err := NewErc20ChainInfo(ctx, cfg.PubChainConfig.WsAddressBSC, config.BSC, &wg, nil)
	tn.Require().NoError(err)

	bscChainClient.ChannelQueue = make(chan *BlockHead, sbchannelsize)

	ethChainClient, err := NewErc20ChainInfo(ctx, cfg.PubChainConfig.WsAddressETH, config.ETH, &wg, nil)
	tn.Require().NoError(err)

	ethChainClient.ChannelQueue = make(chan *BlockHead, sbchannelsize)

	pubChain := &Instance{
		wg:       &wg,
		BSCChain: bscChainClient,
		EthChain: ethChainClient,
	}
	err = bscChainClient.StartSubscription()
	tn.Require().NoError(err)
	err = ethChainClient.StartSubscription()
	tn.Require().NoError(err)

	tn.pubChain, tn.cancel = pubChain, cancel
}

func (tn *TestHelperSuite) TearDownSuite() {
	tn.cancel()
	tn.pubChain.wg.Wait()
}

func (tn *TestHelperSuite) TestAllWithLockOperations() {
	_, err := tn.pubChain.BSCChain.GetBlockByNumberWithLock(nil)
	tn.Require().NoError(err)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*20)
	defer cancel()
	ethAddr := common.HexToAddress("0xbDf7Fb0Ad9b0D722ea54D808b79751608E7AE991")
	balance, err := tn.pubChain.BSCChain.getBalanceWithLock(ctx, ethAddr)
	tn.Require().NoError(err)
	tn.Require().True(balance.Cmp(big.NewInt(0)) == 1)
	nonce, err := tn.pubChain.BSCChain.getPendingNonceWithLock(ctx, ethAddr)
	tn.Require().NoError(err)
	tn.Require().True(nonce > 0)

	_, err = tn.pubChain.BSCChain.GetGasPriceWithLock()
	tn.Require().NoError(err)

	// now we test the operations with closed client to force renew
	tn.pubChain.BSCChain.Client.Close()

	go func() {
		_, err = tn.pubChain.BSCChain.getBalanceWithLock(ctx, ethAddr)
		tn.Require().Error(err)
	}()
	time.Sleep(time.Second)
	tn.pubChain.BSCChain.wgSub.Wait()
	_, err = tn.pubChain.BSCChain.getBalanceWithLock(ctx, ethAddr)
	tn.Require().NoError(err)
	tn.pubChain.BSCChain.Client.Close()
	go func() {
		_, err = tn.pubChain.BSCChain.getPendingNonceWithLock(ctx, ethAddr)
		tn.Require().Error(err)
	}()
	time.Sleep(time.Second)
	tn.pubChain.BSCChain.wgSub.Wait()

	_, err = tn.pubChain.BSCChain.getPendingNonceWithLock(ctx, ethAddr)
	tn.Require().NoError(err)

	time.Sleep(time.Second)
	tn.pubChain.BSCChain.Client.Close()
	go func() {
		_, _, _, err = tn.pubChain.BSCChain.GetFeeLimitWithLock()
		tn.Require().Error(err)
	}()
	time.Sleep(time.Second)
	tn.pubChain.BSCChain.wgSub.Wait()
	_, _, _, err = tn.pubChain.BSCChain.GetFeeLimitWithLock()
	tn.Require().NoError(err)

	tn.pubChain.BSCChain.Client.Close()

	go func() {
		_, err = tn.pubChain.BSCChain.GetBlockByNumberWithLock(nil)
		tn.Require().Error(err)
	}()
	time.Sleep(time.Second)
	tn.pubChain.BSCChain.wgSub.Wait()
	_, err = tn.pubChain.BSCChain.GetBlockByNumberWithLock(nil)
	tn.Require().NoError(err)

	tn.pubChain.BSCChain.Client.Close()
	ctx, cancel = context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	go func() {
		_, err = tn.pubChain.BSCChain.getTransactionReceiptWithLock(ctx, common.HexToHash("0xac0E15a038eedfc68ba3C35c73feD5bE4A07afB5"))
		tn.Require().Error(err)
	}()
	time.Sleep(time.Second)
	tn.pubChain.BSCChain.wgSub.Wait()

	_, err = tn.pubChain.BSCChain.getTransactionReceiptWithLock(ctx, common.HexToHash("0x6e1a0257370db7334ffc10c87d22e78ac0e3edf7a957ce61eb11e59c79300217"))
	tn.Require().NoError(err)

	tn.pubChain.BSCChain.Client.Close()
	tn.pubChain.BSCChain.HealthCheckAndReset()
	tn.pubChain.BSCChain.wgSub.Wait()

	err = tn.pubChain.BSCChain.CheckTxStatus("0x6e1a0257370db7334ffc10c87d22e78ac0e3edf7a957ce61eb11e59c79300217")
	tn.Require().NoError(err)

	tn.pubChain.BSCChain.Client.Close()
	tn.pubChain.EthChain.Client.Close()
	tn.Require().NoError(err)
}

func (tn *TestHelperSuite) TestRecoverKeyFromTx() {
	// we need to recover the connection
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*20)
	defer cancel()
	go func() {
		ethAddr := common.HexToAddress("0xbDf7Fb0Ad9b0D722ea54D808b79751608E7AE991")
		_, err := tn.pubChain.BSCChain.getPendingNonceWithLock(ctx, ethAddr)
		tn.Require().Error(err)
	}()
	// we check that the client is close now and retry will set it up again
	time.Sleep(time.Second)
	tn.pubChain.BSCChain.ChainLocker.RLock()
	h := common.HexToHash("0xf6a04ff4be84c163fb0e400848cff62bad19b6e757339705634e136495dfed3d")
	tx, _, err := tn.pubChain.BSCChain.Client.TransactionByHash(ctx, h)
	tn.Require().NoError(err)
	tn.pubChain.BSCChain.ChainLocker.RUnlock()

	addr, err := tn.pubChain.retrieveAddrfromRawTx(tx)
	tn.Require().NoError(err)
	tn.Require().Equal("jolt196vj6jyqaqydjfqm5n58zegtrx2x02gcm6y9dt", addr.String())
}

func TestHelper(t *testing.T) {
	suite.Run(t, new(TestHelperSuite))
}
