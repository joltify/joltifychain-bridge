package pubchain

import (
	"context"
	"errors"
	"fmt"
	"html"
	"math"
	"math/big"

	sdk "github.com/cosmos/cosmos-sdk/types"
	banktypes "github.com/cosmos/cosmos-sdk/x/bank/types"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	grpc1 "github.com/gogo/protobuf/grpc"
	zlog "github.com/rs/zerolog/log"

	bcommon "gitlab.com/joltify/joltifychain-bridge/common"
	"gitlab.com/joltify/joltifychain-bridge/config"
	"gitlab.com/joltify/joltifychain-bridge/generated"
	"gitlab.com/joltify/joltifychain-bridge/tssclient"
)

func (pi *Instance) MoveFundCosmos(height int64, prefix string, grpcClient grpc1.ClientConn, previousPool *bcommon.PoolInfo) bool {
	currentPool := pi.GetPool()
	// if we restart the bridge, pubchain go routine may run before joltify go routine which acquire the pool info
	if currentPool[1] == nil {
		zlog.Warn().Msgf("[%v]the current pool has not been set, move fund can not start", pi.CosChain.ChainType)
		return false
	}

	fromAtomAddress := sdk.MustBech32ifyAddressBytes(prefix, previousPool.CosAddress)
	acc, err := bcommon.QueryAccount(pi.CosChain.CosHandler.GrpcClient, fromAtomAddress, "")
	if err != nil {
		pi.logger.Error().Err(err).Msgf("[%v]fail to query the Account", pi.CosChain.ChainType)
		return false
	}

	roundBlockHeight := height / ROUNDBLOCK

	signMsg := tssclient.TssSignigMsg{
		Pk:          previousPool.PoolInfo.CreatePool.PoolPubKey,
		Signers:     nil,
		BlockHeight: roundBlockHeight,
		Version:     tssclient.TssVersion,
	}

	from := fromAtomAddress
	to := sdk.MustBech32ifyAddressBytes("cosmos", currentPool[1].CosAddress)

	balance, err := bcommon.QueryBalance(nil, from, pi.CosChain.CosHandler.GrpcAddr, "uatom")
	if err != nil {
		pi.logger.Error().Err(err).Msgf("[%v]fail to query the Account Balance", pi.CosChain.ChainType)
		return false
	}

	if from == to || balance.Amount.IsZero() {
		tick := html.UnescapeString("&#" + "9193" + ";")
		pi.logger.Info().Msgf(" %v [%v]found has already moved", tick, pi.CosChain.ChainType)
		return true
	}

	tick := html.UnescapeString("&#" + "9193" + ";")
	pi.logger.Info().Msgf(" %v [%v]we move fund[%v] from[%v] to[%v]", tick, pi.CosChain.ChainType, balance, from, to)

	sendMsg := banktypes.MsgSend{
		FromAddress: from,
		ToAddress:   to,
		Amount:      sdk.NewCoins(sdk.NewCoin(balance.Denom, balance.Amount)),
	}

	previousPoolAddr := sdk.MustBech32ifyAddressBytes("cosmos", previousPool.CosAddress)
	ret, err := pi.CosChain.CosHandler.BatchComposeAndSend(grpcClient, []sdk.Msg{&sendMsg}, acc.GetSequence(), acc.GetAccountNumber(), &signMsg, previousPoolAddr, []string{"move fund"})
	if err != nil {
		pi.logger.Error().Err(err).Msgf("fail to move the %v", pi.CosChain.ChainType)
		pi.AddMoveFundItem(previousPool, height+movefundretrygap, pi.CosChain.ChainType)
		return false
	}

	txHash := ret[acc.GetSequence()]

	err = pi.CosChain.CosHandler.QueryTxStatus(pi.CosChain.CosHandler.GrpcClient, txHash, 10)
	if err != nil {
		pi.logger.Error().Err(err).Msgf("[%v]fail to find the successful submited tx %v", pi.CosChain.ChainType, txHash)
		pi.AddMoveFundItem(previousPool, height+movefundretrygap, pi.CosChain.ChainType)
		return false
	}
	zlog.Logger.Info().Msgf("[%v]the move fund request has been submitted by others", pi.CosChain.ChainType)

	return true
}

// MoveFound moves the fund for the public chain
// our strategy is we need to run move fund at least twice to ensure the account is empty, even if
// we move the fund success this round, we still need to run it again to 100% ensure the old pool is empty
func (pi *Instance) MoveFound(height int64, chainInfo *Erc20ChainInfo, previousPool *bcommon.PoolInfo, ethClient *ethclient.Client) bool {
	// we get the latest pool address and move funds to the latest pool
	currentPool := pi.GetPool()

	// if we restart the bridge, pubchain go routine may run before joltify go routine which acquire the pool info
	if currentPool[1] == nil {
		zlog.Warn().Msgf("[%v]the current pool has not been set, move fund can not start", chainInfo.ChainType)
		return false
	}

	isMoved, err := pi.moveOperator(chainInfo, previousPool, currentPool[1].EthAddress, ethClient)
	if err != nil {
		pi.AddMoveFundItem(previousPool, height+movefundretrygap, chainInfo.ChainType)
		pi.logger.Error().Err(err).Msgf("[%v]fail to move the operator", chainInfo.ChainType)
		return false
	}
	if !isMoved {
		pi.AddMoveFundItem(previousPool, height+movefundretrygap, chainInfo.ChainType)
		return false
	}

	isMoved, isEmpty, err := pi.doMoveBNBFunds(chainInfo, previousPool, currentPool[1].EthAddress)
	if isEmpty || isMoved {
		return true
	}

	pi.AddMoveFundItem(previousPool, height+movefundretrygap, chainInfo.ChainType)
	if err != nil {
		zlog.Log().Err(err).Msgf("[%v]fail to move the operator from %v to %v", chainInfo.ChainType, previousPool.EthAddress.String(), currentPool[1].EthAddress.String())
	}
	return false
}

func (pi *Instance) setOperator(outboundInstance *generated.Outbound, chainInfo *Erc20ChainInfo, signerPk string, sender, receiver common.Address, nonce *big.Int) (common.Hash, error) {
	txHash, err := pi.SetOperator(outboundInstance, chainInfo, signerPk, sender, receiver, nonce)
	if err != nil {
		if err.Error() == alreadyKnown {
			pi.logger.Warn().Msgf("[%v]the set operator tx[%v] has been submitted by others", chainInfo.ChainType, txHash)
			return txHash, nil
		}
		pi.logger.Error().Err(err).Msgf("[%v]fail to set operator", chainInfo.ChainType)
		return txHash, err
	}
	return txHash, nil
}

func (pi *Instance) moveOperator(chainInfo *Erc20ChainInfo, previousPool *bcommon.PoolInfo, receiver common.Address, ethClient *ethclient.Client) (bool, error) {
	outboundInstance, err := generated.NewOutbound(common.HexToAddress(chainInfo.Contract.OutboundAddr), ethClient)
	if err != nil {
		return false, err
	}

	operatorRole, err := outboundInstance.OPERATORROLE(&bind.CallOpts{})
	if err != nil {
		return false, err
	}

	hasRole, err := outboundInstance.HasRole(&bind.CallOpts{}, operatorRole, previousPool.EthAddress)
	if err != nil {
		return false, err
	}
	if !hasRole {
		return true, nil
	}

	tick := html.UnescapeString("&#" + "9193" + ";")
	pi.logger.Info().Msgf(" %v we move operator[%v] from %v to %v", tick, chainInfo.ChainType, previousPool.EthAddress.String(), receiver.String())

	ctx, cancel := context.WithTimeout(context.Background(), config.QueryTimeOut)
	defer cancel()
	nonce, err := chainInfo.getPendingNonceWithLock(ctx, previousPool.EthAddress)
	if err != nil {
		return false, err
	}

	txHash, err := pi.setOperator(outboundInstance, chainInfo, previousPool.Pk, previousPool.EthAddress, receiver, new(big.Int).SetUint64(nonce))
	if err != nil && err.Error() != "already passed the seq" {
		return false, errors.New("fail to move operator")
	}

	err = chainInfo.CheckTxStatus(txHash.Hex())
	if err != nil {
		return false, err
	}

	hasRole, err = outboundInstance.HasRole(&bind.CallOpts{}, operatorRole, previousPool.EthAddress)
	if err != nil {
		return false, err
	}
	if !hasRole {
		return true, nil
	}

	return false, errors.New("we failed to move operator")
}

func (pi *Instance) doMoveBNBFunds(chainInfo *Erc20ChainInfo, previousPool *bcommon.PoolInfo, receiver common.Address) (bool, bool, error) {
	ctx, cancel := context.WithTimeout(context.Background(), config.QueryTimeOut)
	defer cancel()
	balanceBnB, err := chainInfo.getBalanceWithLock(ctx, previousPool.EthAddress)
	if err != nil {
		return false, false, err
	}

	fee, _, _, err := chainInfo.GetFeeLimitWithLock()
	if err != nil {
		return false, false, err
	}

	// this indicates we have the leftover that smaller than the fee
	if balanceBnB.Cmp(fee) != 1 {
		return true, true, nil
	}

	tick := html.UnescapeString("&#" + "9193" + ";")
	pi.logger.Info().Msgf(" %v we move fund %v:%v from %v to %v", tick, chainInfo.ChainType, balanceBnB, previousPool.EthAddress.String(), receiver.String())

	// we move the bnb
	nonce, err := chainInfo.getPendingNonceWithLock(ctx, previousPool.EthAddress)
	if err != nil {
		return false, false, err
	}

	bnbTxHash, emptyAccount, err := pi.SendNativeTokenForMoveFund(chainInfo, previousPool.Pk, previousPool.EthAddress, receiver, balanceBnB, new(big.Int).SetUint64(nonce))
	// bnbTxHash, err = pi.moveBnb(previousPool.Pk, receiver, balanceBnB, nonce, blockHeight)
	if err != nil {
		if err.Error() == "already passed the seq" {
			ctx2, cancel2 := context.WithTimeout(context.Background(), config.QueryTimeOut)
			defer cancel2()
			nowBalanceBnB, err := chainInfo.getBalanceWithLock(ctx2, previousPool.EthAddress)
			if err != nil {
				return false, false, err
			}

			// this indicates we have the leftover that smaller than the fee
			if nowBalanceBnB.Cmp(fee) != 1 {
				return true, true, nil
			}
		}
		return false, false, err
	}
	if emptyAccount {
		zlog.Logger.Info().Msgf("[%v]this is the empty account to move fund", chainInfo.ChainType)
		return true, true, nil
	}

	errCheck := chainInfo.CheckTxStatus(bnbTxHash.Hex())
	if errCheck != nil {
		return false, false, errCheck
	}
	tick = html.UnescapeString("&#" + "127974" + ";")
	zlog.Logger.Info().Msgf(" %v we have moved the fund in the public chain (%v): %v with hash %v", tick, chainInfo.ChainType, balanceBnB.String(), bnbTxHash)

	ctx2, cancel2 := context.WithTimeout(context.Background(), config.QueryTimeOut)
	defer cancel2()
	nowBalanceBnB, err := chainInfo.getBalanceWithLock(ctx2, previousPool.EthAddress)
	if err != nil {
		return false, false, err
	}

	// this indicate we have the leftover that smaller than the fee
	if nowBalanceBnB.Cmp(fee) != 1 {
		return true, true, nil
	}
	return true, false, nil
}

func (pi *Instance) AddMoveFundItem(pool *bcommon.PoolInfo, height int64, chainType string) {
	item := bcommon.MoveFundItem{PoolInfo: pool, ChainType: chainType, Height: height}
	index := fmt.Sprintf("%v:%v", height, chainType)
	pi.moveFundReq.Store(index, &item)
}

func (pi *Instance) ExportMoveFundItems() []*bcommon.MoveFundItem {
	var data []*bcommon.MoveFundItem
	pi.moveFundReq.Range(func(key, value any) bool {
		exported := value.(*bcommon.MoveFundItem)
		data = append(data, exported)
		return true
	})
	return data
}

// PopMoveFundItemAfterBlock pop up the item after the given block duration
func (pi *Instance) PopMoveFundItemAfterBlock(currentBlockHeight int64, chainType string) (*bcommon.MoveFundItem, int64) {
	min := int64(math.MaxInt64)
	pi.moveFundReq.Range(func(key, value interface{}) bool {
		val := value.(*bcommon.MoveFundItem)
		h := val.Height
		if h <= min && val.ChainType == chainType {
			min = h
		}
		return true
	})

	if min < math.MaxInt64 && (currentBlockHeight-min > config.MINCHECKBLOCKGAP) {
		index := fmt.Sprintf("%v:%v", min, chainType)
		item, _ := pi.moveFundReq.LoadAndDelete(index)
		return item.(*bcommon.MoveFundItem), min
	}
	return nil, 0
}
