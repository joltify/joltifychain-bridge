package pubchain

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"sync"
	"time"

	"github.com/cosmos/cosmos-sdk/crypto/keyring"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/rs/zerolog/log"
	ttypes "github.com/tendermint/tendermint/types"

	"gitlab.com/joltify/joltifychain-bridge/common"
	"gitlab.com/joltify/joltifychain-bridge/config"
	cossubmit "gitlab.com/joltify/joltifychain-bridge/cos_submit"
	"gitlab.com/joltify/joltifychain-bridge/generated"
	"gitlab.com/joltify/joltifychain-bridge/tokenlist"
	"gitlab.com/joltify/joltifychain-bridge/tssclient"
)

func NewCosChainInfo(ctx context.Context, grpcAddr, httpAddr, chainType string, wg *sync.WaitGroup, tssServer tssclient.TssInstance) (*CosMosChainInfo, error) {
	enc := common.MakeEncodingConfig()
	key := keyring.NewInMemory(enc.Codec)
	handler, err := cossubmit.NewCosOperations(ctx, wg, grpcAddr, httpAddr, chainType, key, tssServer)
	if err != nil {
		return nil, err
	}

	c := CosMosChainInfo{
		logger:     log.With().Str("module", chainType).Logger(),
		ChainType:  chainType,
		wg:         wg,
		encoding:   &enc,
		CosHandler: handler,
	}
	return &c, nil
}

func NewErc20ChainInfo(ctx context.Context, wsAddress, chainType string, wg *sync.WaitGroup, queue chan *BlockHead) (*Erc20ChainInfo, error) {
	contract, ok := ContractMapping[chainType]
	if !ok {
		return nil, errors.New("contract not found")
	}

	ctxDial, cancelDial := context.WithTimeout(context.Background(), time.Second*2)
	client, err := ethclient.DialContext(ctxDial, wsAddress)
	cancelDial()
	if err != nil {
		return nil, errors.New("fail to dial the network")
	}

	ctxQeury, cancelQeury := context.WithTimeout(context.Background(), chainQueryTimeout)
	defer cancelQeury()
	chainID, err := client.ChainID(ctxQeury)
	if err != nil {
		return nil, err
	}

	c := Erc20ChainInfo{
		Ctx:      ctx,
		WsAddr:   wsAddress,
		Contract: contract,

		ChainType:   chainType,
		Client:      client,
		ChainID:     chainID,
		ChainLocker: &sync.RWMutex{},
		logger:      log.With().Str("module", chainType).Logger(),
		wg:          wg,

		wgSub:         &sync.WaitGroup{},
		SubChannelNow: make(chan *types.Header, sbchannelsize),
		ChannelQueue:  queue,
		reSubChannel:  make(chan struct{}, 1),
	}
	return &c, nil
}

func (c *Erc20ChainInfo) Terminate() {
	c.unsubscribe()
	// FIXME: close all the channels should be better?
}

// NewChainInstance initialize the oppy_bridge entity
func NewChainInstance(ctx context.Context, cfg config.Config, tssServer tssclient.TssInstance, tl tokenlist.BridgeTokenListI, wg *sync.WaitGroup, joltRetryOutBoundReq *sync.Map) (*Instance, error) {
	logger := log.With().Str("module", "pubchain").Logger()
	retryPools := common.NewRetryPools()
	channelQueue := make(chan *BlockHead, sbchannelsize)

	// add atom chain
	atomClient, err := NewCosChainInfo(ctx, cfg.AtomChain.GrpcAddress, cfg.AtomChain.HTTPAddress, config.ATOM, wg, tssServer)
	if err != nil {
		logger.Error().Err(err).Msgf("fail to create the atom client")
		return nil, err
	}
	atomClient.ChannelQueue = channelQueue

	tAbi, err := abi.JSON(strings.NewReader(generated.GeneratedMetaData.ABI))
	if err != nil {
		return nil, fmt.Errorf("fail to get the tokenABI with err %v", err)
	}
	oAbi, err := abi.JSON(strings.NewReader(generated.OutboundABI))
	if err != nil {
		return nil, fmt.Errorf("fail to get the outboundABI with err %v", err)
	}

	pi := &Instance{
		logger:                logger,
		CosChain:              atomClient,
		tokenAbi:              &tAbi,
		outboundAbi:           &oAbi,
		poolLocker:            &sync.RWMutex{},
		tssServer:             tssServer,
		lastTwoPools:          make([]*common.PoolInfo, 2),
		InboundReqChan:        make(chan []*common.InBoundReq, inboundprosSize),
		RetryInboundReq:       &sync.Map{},
		moveFundReq:           &sync.Map{},
		TokenList:             tl,
		ChannelQueue:          channelQueue,
		wg:                    wg,
		onHoldRetryQueue:      []*common.InBoundReq{},
		onHoldRetryQueueLock:  &sync.Mutex{},
		RetryOutboundReq:      retryPools.RetryOutboundReq,
		FeeModule:             common.InitFeeModule(),
		joltHandler:           NewJoltHandler(),
		joltRetryOutBoundReq:  joltRetryOutBoundReq,
		subscriptionCloseChan: nil,
	}

	for _, chainType := range config.EvmChains {
		chainClient, err := NewErc20ChainInfo(ctx, cfg.PubChainConfig.GetWsAddress(chainType), chainType, wg, channelQueue)
		if err != nil {
			logger.Error().Err(err).Msgf("[%v]fail to create chain client", chainType)
			return nil, fmt.Errorf("invalid %s client", chainType)
		}
		pi.SetERC20Chain(chainClient)
	}
	return pi, nil
}

func (pi *Instance) SetKey(uid string, data, pass []byte) error {
	return pi.CosChain.CosHandler.SetKey(uid, data, pass)
}

func (pi *Instance) GetCosChannelQueueNewBlockChan() chan *ttypes.Header {
	return pi.CosChain.CosHandler.GetChannelQueueNewBlockChan()
}
