package pubchain

import (
	"context"
	"math/big"
	"sync"
	"sync/atomic"
	"time"

	"github.com/cosmos/cosmos-sdk/simapp/params"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
	grpc1 "github.com/gogo/protobuf/grpc"
	"github.com/rs/zerolog"

	bcommon "gitlab.com/joltify/joltifychain-bridge/common"
	"gitlab.com/joltify/joltifychain-bridge/config"
	cossubmit "gitlab.com/joltify/joltifychain-bridge/cos_submit"
	"gitlab.com/joltify/joltifychain-bridge/tokenlist"
	"gitlab.com/joltify/joltifychain-bridge/tssclient"
)

const (
	statusInit int32 = iota
	statusPending
	statusRunning
	statusStopped
)

const (
	inboundprosSize   = 512
	sbchannelsize     = 20000
	chainQueryTimeout = time.Second * 15
	ROUNDBLOCK        = 50
	submitBackoff     = time.Second * 2
	GroupBlockGap     = 8
	GroupSign         = 4
	channelSize       = 2000
	movefundretrygap  = 3
)

var (
	OppyContractAddressBSC = "0x94277968dff216265313657425d9d7577ad32dd1"
	OppyContractAddressETH = "0x2BCD5745eBf28f367A0De2cF3C6fEBfE42B21338"
	OppyContractAddressAVA = "0x76586783ED36a9b5104fcf249e5677c2Dc981943"
	OppyContractAddressPOL = "0xB99d3ceD6C0fAF2939BC01E3e4fDCF0f1884072D"

	OutboundContractAddressBSC = "0xB75AAc921130A644d4DAa2b20D28318f63630FDd"
	OutboundContractAddressETH = "0xB75AAc921130A644d4DAa2b20D28318f63630FDd"
	OutboundContractAddressAVA = "0xB99d3ceD6C0fAF2939BC01E3e4fDCF0f1884072D"
	OutboundContractAddressPOL = "0x6A798D111dE848A2624ae8128A8d54febc91559C"

	ContractMapping = map[string]*Contract{
		config.BSC: {
			TransferAddr: OppyContractAddressBSC,
			OutboundAddr: OutboundContractAddressBSC,
		},
		config.ETH: {
			TransferAddr: OppyContractAddressETH,
			OutboundAddr: OutboundContractAddressETH,
		},
		config.AVA: {
			TransferAddr: OppyContractAddressAVA,
			OutboundAddr: OutboundContractAddressAVA,
		},
		config.POL: {
			TransferAddr: OppyContractAddressPOL,
			OutboundAddr: OutboundContractAddressPOL,
		},
	}
)

type Contract struct {
	TransferAddr string
	OutboundAddr string
}

type JoltHandler interface {
	queryTokenPrice(grpcClient grpc1.ClientConn, grpcAddr string, denom string) (sdk.Dec, error)
	QueryJoltBlockHeight(grpcAddr string) (int64, error)
}

type InboundTx struct {
	TxID            string         `json:"tx_id"` // this variable is used for locally saving and loading
	ReceiverAddress sdk.AccAddress `json:"receiver_address"`
	PubBlockHeight  uint64         `json:"pub_block_height"` // this variable is used to delete the expired tx
	Token           sdk.Coin       `json:"token"`
}

type Erc20TxInfo struct {
	receiverAddr      sdk.AccAddress // address that we should send token to
	receiverAddrERC20 string         // address that we should send token to
	toAddr            common.Address // address to the pool
	Amount            *big.Int       // the amount of the token that cross the bridge
	tokenAddress      common.Address // the erc20 contract address (we need to ensure the token address is 100% correct)
	dstChainType      string
}

type InboundTxBnb struct {
	BlockHeight uint64   `json:"block_height"`
	TxID        string   `json:"tx_id"`
	Fee         sdk.Coin `json:"fee"`
}

type TssReq struct {
	Data  []byte
	Index int
}

type BlockHead struct {
	Head      *types.Header
	ChainType string
}

type Erc20ChainInfo struct {
	Ctx    context.Context
	WsAddr string
	*Contract

	ChainType     string
	Client        *ethclient.Client
	ChainLocker   *sync.RWMutex
	ChainID       *big.Int
	logger        zerolog.Logger
	wg            *sync.WaitGroup // this must be the wg of the pubchain instance thus te whole process can be contolled by the main progress
	SubChannelNow chan *types.Header
	SubHandler    ethereum.Subscription
	ChannelQueue  chan *BlockHead
	// use to wait resub process
	wgSub        *sync.WaitGroup
	reSubChannel chan struct{}
	// use to store the last handle header
	CurrentHeight *big.Int
	// status
	status atomic.Int32
}

type CosMosChainInfo struct {
	ChainType    string
	encoding     *params.EncodingConfig
	logger       zerolog.Logger
	wg           *sync.WaitGroup // this must be the wg of the pubchain instance thus te whole process can be contolled by the main progress
	ChannelQueue chan *BlockHead
	CosHandler   *cossubmit.CosHandler
}

// Instance hold the oppy_bridge entity
type Instance struct {
	BSCChain              *Erc20ChainInfo
	EthChain              *Erc20ChainInfo
	AvaChain              *Erc20ChainInfo
	PolChain              *Erc20ChainInfo
	CosChain              *CosMosChainInfo
	tokenAbi              *abi.ABI
	outboundAbi           *abi.ABI
	logger                zerolog.Logger
	lastTwoPools          []*bcommon.PoolInfo
	poolLocker            *sync.RWMutex
	tssServer             tssclient.TssInstance
	InboundReqChan        chan []*bcommon.InBoundReq
	RetryInboundReq       *sync.Map // if a tx fail to process, we need to put in this channel and wait for retry
	moveFundReq           *sync.Map
	TokenList             tokenlist.BridgeTokenListI
	wg                    *sync.WaitGroup
	ChannelQueue          chan *BlockHead
	onHoldRetryQueue      []*bcommon.InBoundReq
	onHoldRetryQueueLock  *sync.Mutex
	RetryOutboundReq      *sync.Map // if a tx fail to process, we need to put in this channel and wait for retry
	FeeModule             map[string]*bcommon.FeeModule
	joltHandler           JoltHandler
	joltRetryOutBoundReq  *sync.Map
	subscriptionCloseChan chan bool
}
