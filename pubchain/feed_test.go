package pubchain

import (
	"context"
	"fmt"
	"math/big"
	"strconv"
	"sync"
	"testing"

	"google.golang.org/grpc"

	"github.com/cosmos/cosmos-sdk/client/grpc/tmservice"
	"github.com/cosmos/cosmos-sdk/crypto/hd"
	"github.com/cosmos/cosmos-sdk/crypto/keyring"
	"github.com/cosmos/cosmos-sdk/crypto/keys/ed25519"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/types/bech32/legacybech32"
	stakingtypes "github.com/cosmos/cosmos-sdk/x/staking/types"
	"github.com/ethereum/go-ethereum/core/types"
	ethTypes "github.com/ethereum/go-ethereum/core/types"
	"github.com/joltify-finance/joltify_lending/testutil/network"
	vaulttypes "github.com/joltify-finance/joltify_lending/x/vault/types"
	"github.com/stretchr/testify/suite"

	"gitlab.com/joltify/joltifychain-bridge/common"
	"gitlab.com/joltify/joltifychain-bridge/config"
	"gitlab.com/joltify/joltifychain-bridge/misc"
	"gitlab.com/joltify/joltifychain-bridge/tokenlist"
)

const (
	AddrBUSD = "0xeB42ff4cA651c91EB248f8923358b6144c6B4b79"
)

type feedTestSuite struct {
	suite.Suite
	cfg         network.Config
	network     *network.Network
	validatorky keyring.Keyring
	queryClient tmservice.ServiceClient
}

func genNValidator(n int, validatorky keyring.Keyring) ([]stakingtypes.Validator, error) {
	var validators []stakingtypes.Validator
	var uid string
	for i := 0; i < n; i++ {
		if i == 0 {
			uid = "operator"
		} else {
			uid = "o" + strconv.Itoa(i)
		}
		info, _, err := validatorky.NewMnemonic(uid, keyring.English, sdk.FullFundraiserPath, keyring.DefaultBIP39Passphrase, hd.Secp256k1)
		if err != nil {
			return nil, err
		}

		pubkey, err := info.GetPubKey()
		if err != nil {
			return nil, err
		}
		operator, err := sdk.ValAddressFromHex(pubkey.Address().String())
		if err != nil {
			return nil, err
		}
		desc := stakingtypes.NewDescription("tester", "testId", "www.test.com", "aaa", "aaa")
		testValidator, err := stakingtypes.NewValidator(operator, pubkey, desc)
		if err != nil {
			return nil, err
		}
		validators = append(validators, testValidator)
	}
	return validators, nil
}

func (fd *feedTestSuite) SetupSuite() {
	misc.SetupBech32Prefix()
	cfg := network.DefaultConfig()
	cfg.BondDenom = "stake"
	cfg.BondedTokens = sdk.NewInt(10000000000000000)
	cfg.StakingTokens = sdk.NewInt(100000000000000000)
	fd.cfg = cfg
	fd.validatorky = keyring.NewInMemory(cfg.Codec)
	// now we put the mock pool list in the test
	state := vaulttypes.GenesisState{}
	stateStaking := stakingtypes.GenesisState{}

	fd.Require().NoError(cfg.Codec.UnmarshalJSON(cfg.GenesisState[vaulttypes.ModuleName], &state))
	fd.Require().NoError(cfg.Codec.UnmarshalJSON(cfg.GenesisState[stakingtypes.ModuleName], &stateStaking))

	validators, err := genNValidator(3, fd.validatorky)
	fd.Require().NoError(err)
	for i := 1; i < 5; i++ {
		randPoolSk := ed25519.GenPrivKey()
		poolPubKey, err := legacybech32.MarshalPubKey(legacybech32.AccPK, randPoolSk.PubKey()) // nolint
		fd.Require().NoError(err)

		var nodes []sdk.AccAddress
		for _, el := range validators {
			operator, err := sdk.ValAddressFromBech32(el.OperatorAddress)
			if err != nil {
				panic(err)
			}
			nodes = append(nodes, operator.Bytes())
		}
		pro := vaulttypes.PoolProposal{
			PoolPubKey: poolPubKey,
			PoolAddr:   randPoolSk.PubKey().Address().Bytes(),
			Nodes:      nodes,
		}
		state.CreatePoolList = append(state.CreatePoolList, &vaulttypes.CreatePool{BlockHeight: strconv.Itoa(i), Validators: validators, Proposal: []*vaulttypes.PoolProposal{&pro}})
	}
	state.LatestTwoPool = state.CreatePoolList[:2]
	testToken := vaulttypes.IssueToken{
		Index: "testindex",
	}
	state.IssueTokenList = append(state.IssueTokenList, &testToken)

	buf, err := cfg.Codec.MarshalJSON(&state)
	fd.Require().NoError(err)
	cfg.GenesisState[vaulttypes.ModuleName] = buf

	var stateVault stakingtypes.GenesisState
	fd.Require().NoError(cfg.Codec.UnmarshalJSON(cfg.GenesisState[stakingtypes.ModuleName], &stateVault))
	stateVault.Params.MaxValidators = 3
	state.Params.BlockChurnInterval = 1
	buf, err = cfg.Codec.MarshalJSON(&stateVault)
	fd.Require().NoError(err)
	cfg.GenesisState[stakingtypes.ModuleName] = buf

	fd.network = network.New(fd.T(), cfg)

	fd.Require().NotNil(fd.network)

	_, err = fd.network.WaitForHeight(1)
	fd.Require().Nil(err)
	fd.queryClient = tmservice.NewServiceClient(fd.network.Validators[0].ClientCtx)
}

func (fd *feedTestSuite) TestFeedTx() {
	misc.SetupBech32Prefix()
	acc, err := generateRandomPrivKey(3)
	fd.Assert().NoError(err)
	testTx := types.MustSignNewTx(testKey, types.LatestSigner(genesis.Config), &types.LegacyTx{
		Nonce:    0,
		Value:    big.NewInt(12),
		Gas:      53001,
		GasPrice: big.NewInt(1),
	})

	backend, _ := newTestBackend(fd.T(), []*ethTypes.Transaction{testTx})
	defer backend.Close()
	client, err := backend.Attach()
	fd.Assert().NoError(err)
	defer client.Close()

	tssServer := TssMock{acc[0].sk}
	tl, err := tokenlist.CreateMockTokenlist([]string{"testAddr"}, []string{"testDenom"}, []string{config.BSC})
	fd.Assert().NoError(err)
	wg := sync.WaitGroup{}
	mockRetryMap := sync.Map{}
	cfg := config.Config{}
	cfg.PubChainConfig.WsAddressBSC = misc.WebsocketTest
	cfg.PubChainConfig.WsAddressETH = misc.WebsocketTest

	pi := &Instance{
		joltRetryOutBoundReq: &mockRetryMap,
		TokenList:            tl,
		tssServer:            &tssServer,
	}

	bscChainClient, err := NewErc20ChainInfo(context.Background(), misc.WebsocketTest, config.BSC, &wg, nil)
	fd.Assert().NoError(err)
	pi.BSCChain = bscChainClient

	poolInfo := vaulttypes.PoolInfo{
		BlockHeight: "225",
		CreatePool: &vaulttypes.PoolProposal{
			PoolPubKey: "joltpub1addwnpepqgknlvjpa7237gnrm2kakjd37xagm7435hmk6zqf5248dnext9cfshhe990",
		},
	}
	toAddr, err := misc.PoolPubKeyToEthAddress("joltpub1addwnpepqgknlvjpa7237gnrm2kakjd37xagm7435hmk6zqf5248dnext9cfshhe990")
	fd.Assert().NoError(err)

	a1 := common.NewOutboundReq("test1", acc[0].commAddr.Bytes(), toAddr.Bytes(), sdk.NewCoin("test", sdk.NewInt(1)), AddrBUSD, 125, nil, config.BSC, true)
	a2 := common.NewOutboundReq("test2", acc[0].commAddr.Bytes(), toAddr.Bytes(), sdk.NewCoin("test", sdk.NewInt(1)), AddrBUSD, 125, nil, config.BSC, true)
	a3 := common.NewOutboundReq("test3", acc[0].commAddr.Bytes(), toAddr.Bytes(), sdk.NewCoin("test", sdk.NewInt(1)), AddrBUSD, 125, nil, config.BSC, true)
	a4 := common.NewOutboundReq("test4", acc[0].commAddr.Bytes(), toAddr.Bytes(), sdk.NewCoin("test", sdk.NewInt(1)), AddrBUSD, 125, nil, config.BSC, true)
	testOutBoundReqs := []*common.OutBoundReq{&a1, &a2, &a3, &a4}

	err = pi.FeedTx(&poolInfo, testOutBoundReqs, config.BSC)
	fd.Assert().NoError(err)
	fd.Assert().Equal(testOutBoundReqs[0].BlockHeight, int64(125))
	wg.Wait()
}

func (fd *feedTestSuite) TestFeedTxCosmos() {
	wg := sync.WaitGroup{}
	mockRetryMap := sync.Map{}
	cfg := config.Config{}

	cfg.AtomChain.GrpcAddress = fd.network.Validators[0].AppConfig.GRPC.Address
	cfg.AtomChain.HTTPAddress = fd.network.Validators[0].RPCAddress
	cfg.PubChainConfig.WsAddressBSC = misc.WebsocketTest
	cfg.PubChainConfig.WsAddressETH = misc.WebsocketTest
	cfg.PubChainConfig.WsAddressAVA = misc.WebsocketTest
	cfg.PubChainConfig.WsAddressPOL = misc.WebsocketTest
	ins, err := NewChainInstance(context.Background(), cfg, nil, nil, &wg, &mockRetryMap)
	fd.Assert().NoError(err)

	grpcClient, err := grpc.Dial(fd.network.Validators[0].AppConfig.GRPC.Address, grpc.WithInsecure())
	fd.Require().NoError(err)
	ts := vaulttypes.NewQueryClient(grpcClient)
	ctx, cancel := context.WithTimeout(context.Background(), grpcTimeout)
	defer cancel()

	req := vaulttypes.QueryLatestPoolRequest{}
	resp, err := ts.GetLastPool(ctx, &req)
	fd.Assert().NoError(err)
	fmt.Printf(">>>>>>%v\n", resp.String())

	mockOutBound := make([]*common.OutBoundReq, 10)

	for i := 0; i < 10; i++ {
		b := common.OutBoundReq{
			TxID: strconv.Itoa(i),
		}
		mockOutBound[i] = &b

	}

	err = ins.FeedTxCosmos(resp.Pools[0], "jolt", mockOutBound)
	fd.Assert().Error(err)
}

func TestFeed(t *testing.T) {
	suite.Run(t, new(feedTestSuite))
}
