package pubchain

import (
	"context"
	"encoding/hex"
	"math/big"
	"sync"
	"testing"
	"time"

	"github.com/cosmos/cosmos-sdk/crypto/keys/secp256k1"
	"github.com/cosmos/cosmos-sdk/types/bech32/legacybech32" //nolint
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	vaulttypes "github.com/joltify-finance/joltify_lending/x/vault/types"
	"github.com/stretchr/testify/suite"

	bcommon "gitlab.com/joltify/joltifychain-bridge/common"
	"gitlab.com/joltify/joltifychain-bridge/config"
	"gitlab.com/joltify/joltifychain-bridge/generated"
	"gitlab.com/joltify/joltifychain-bridge/misc"
	"gitlab.com/joltify/joltifychain-bridge/tokenlist"
)

type TestNetTestSuite struct {
	suite.Suite
	pubChain *Instance
	pk1      string
	sk1      *secp256k1.PrivKey
	pk2      string
	sk2      *secp256k1.PrivKey
	opk1     string
	osk1     *secp256k1.PrivKey
	opk2     string
	osk2     *secp256k1.PrivKey
	tss      *TssMock
}

func getKeys(private string) (string, *secp256k1.PrivKey) {
	data, err := hex.DecodeString(private)
	if err != nil {
		panic(err)
	}
	sk := secp256k1.PrivKey{Key: data}
	pk, err := legacybech32.MarshalPubKey(legacybech32.AccPK, sk.PubKey()) //nolint
	if err != nil {
		panic(err)
	}
	return pk, &sk
}

func (tn *TestNetTestSuite) SetupSuite() {
	misc.SetupBech32Prefix()

	a1 := "c225ac7cf268405670c004e0b8f6b7df5fefb80f3505aaf9619ea89c787a67e7"
	a2 := "481d305c7be328b6defd500209f9fdfb5447231f4c1f665324df951029506e12"
	oa1 := "9c3788a1775f0c209a5a728eca6166b39dacab0fb0f9c3fe7066f92bc5a97d29"
	oa2 := "41714ff060781167f82e11b558276dc94bd4fd8ec3462261110432c6ead64315"

	tn.pk1, tn.sk1 = getKeys(a1)
	tn.pk2, tn.sk2 = getKeys(a2)
	tn.opk1, tn.osk1 = getKeys(oa1)
	tn.opk2, tn.osk2 = getKeys(oa2)

	tss := TssMock{sk: tn.sk1}
	tn.tss = &tss
	tl, err := tokenlist.CreateMockTokenlist([]string{"0xeB42ff4cA651c91EB248f8923358b6144c6B4b79"}, []string{"JUSD"}, []string{config.BSC})
	if err != nil {
		panic(err)
	}
	wg := sync.WaitGroup{}
	cfg := config.Config{}
	cfg.PubChainConfig.WsAddressBSC = misc.WebsocketTest
	cfg.PubChainConfig.WsAddressETH = misc.WebsocketTest

	bscChainClient, err := NewErc20ChainInfo(context.Background(), cfg.PubChainConfig.WsAddressBSC, config.BSC, &wg, nil)
	tn.Require().NoError(err)

	ethChainClient, err := NewErc20ChainInfo(context.Background(), cfg.PubChainConfig.WsAddressETH, config.ETH, &wg, nil)
	tn.Require().NoError(err)

	pubChain := &Instance{
		wg:           &wg,
		BSCChain:     bscChainClient,
		EthChain:     ethChainClient,
		TokenList:    tl,
		lastTwoPools: make([]*bcommon.PoolInfo, 2),
		poolLocker:   &sync.RWMutex{},
		tssServer:    &tss,
		moveFundReq:  &sync.Map{},
	}

	tn.pubChain = pubChain
}

func (tn *TestNetTestSuite) TestProcessNewBlock() {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	tn.pubChain.BSCChain.ChainLocker.Lock()
	number, err := tn.pubChain.BSCChain.Client.BlockNumber(ctx)
	tn.Require().NoError(err)
	tn.pubChain.BSCChain.ChainLocker.Unlock()
	err = tn.pubChain.ProcessNewERC20Block(config.BSC, tn.pubChain.BSCChain, big.NewInt(int64(number)), tn.pubChain.FeeModule, "")
	tn.Require().NoError(err)
}

func (tn *TestNetTestSuite) TestDoMoveFund() {
	addr1, err := misc.PoolPubKeyToEthAddress(tn.pk1)
	tn.Require().NoError(err)
	addr2, err := misc.PoolPubKeyToEthAddress(tn.pk2)
	tn.Require().NoError(err)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
	defer cancel()
	balance1, err := tn.pubChain.BSCChain.getBalanceWithLock(ctx, addr1)
	tn.Require().NoError(err)
	balance2, err := tn.pubChain.BSCChain.getBalanceWithLock(ctx, addr2)
	tn.Require().NoError(err)

	tn.tss.sk = tn.sk2
	previouspool := tn.pk2
	newpool := tn.pk1

	if balance1.Cmp(balance2) == 1 {
		tn.tss.sk = tn.sk1
		previouspool = tn.pk1
		newpool = tn.pk2
	}

	ethAddr, err := misc.PoolPubKeyToEthAddress(previouspool)
	tn.Require().NoError(err)
	oppyAddr, err := misc.PoolPubKeyToJoltifyAddress(previouspool)
	tn.Require().NoError(err)

	previous := bcommon.PoolInfo{
		Pk:         previouspool,
		CosAddress: oppyAddr,
		EthAddress: ethAddr,
	}

	poolInfo2 := vaulttypes.PoolInfo{
		BlockHeight: "100",
		CreatePool: &vaulttypes.PoolProposal{
			PoolPubKey: newpool,
			Nodes:      nil,
		},
	}

	err = tn.pubChain.UpdatePool(&poolInfo2)
	tn.Require().NoError(err)
	err = tn.pubChain.UpdatePool(&poolInfo2)
	tn.Require().NoError(err)

	ret := tn.pubChain.MoveFound(100, tn.pubChain.BSCChain, &previous, tn.pubChain.BSCChain.Client)

	if !ret {
		item, height := tn.pubChain.PopMoveFundItemAfterBlock(101, config.BSC)
		tn.Require().Equal(int64(0), height)
		tn.Require().Nil(item)
		item, _ = tn.pubChain.PopMoveFundItemAfterBlock(100+config.MINCHECKBLOCKGAP+100, config.BSC)
		tn.Require().NotNil(item)
		ret2 := tn.pubChain.MoveFound(100, tn.pubChain.BSCChain, item.PoolInfo, tn.pubChain.BSCChain.Client)
		tn.Require().True(ret2)
	}
}

func (tn *TestNetTestSuite) TestSetOperator() {
	newAddr, err := misc.PoolPubKeyToEthAddress(tn.opk1)
	tn.Require().NoError(err)
	preAddr, err := misc.PoolPubKeyToEthAddress(tn.opk2)
	tn.Require().NoError(err)

	outboundInstance, err := generated.NewOutbound(common.HexToAddress(tn.pubChain.BSCChain.Contract.OutboundAddr), tn.pubChain.BSCChain.Client)
	tn.Require().NoError(err)
	operatorRole, err := outboundInstance.OPERATORROLE(&bind.CallOpts{})
	tn.Require().NoError(err)

	tn.tss.sk = tn.osk2
	previouspool := tn.opk2
	newpool := tn.opk1

	hasRole, err := outboundInstance.HasRole(&bind.CallOpts{}, operatorRole, preAddr)
	tn.Require().NoError(err)
	if !hasRole {
		tn.tss.sk = tn.osk1
		previouspool = tn.opk1
		newpool = tn.pk2
		newAddr, preAddr = preAddr, newAddr
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
	defer cancel()

	ethAddr, err := misc.PoolPubKeyToEthAddress(previouspool)
	tn.Require().NoError(err)
	oppyAddr, err := misc.PoolPubKeyToJoltifyAddress(previouspool)
	tn.Require().NoError(err)
	nonce, err := tn.pubChain.BSCChain.getPendingNonceWithLock(ctx, ethAddr)
	tn.Require().NoError(err)

	previous := bcommon.PoolInfo{
		Pk:         previouspool,
		CosAddress: oppyAddr,
		EthAddress: ethAddr,
	}

	poolInfo2 := vaulttypes.PoolInfo{
		BlockHeight: "100",
		CreatePool: &vaulttypes.PoolProposal{
			PoolPubKey: newpool,
			Nodes:      nil,
		},
	}

	err = tn.pubChain.UpdatePool(&poolInfo2)
	tn.Require().NoError(err)
	err = tn.pubChain.UpdatePool(&poolInfo2)
	tn.Require().NoError(err)

	txHash, err := tn.pubChain.setOperator(outboundInstance, tn.pubChain.BSCChain, previous.Pk, previous.EthAddress, newAddr, new(big.Int).SetUint64(nonce))
	tn.Require().NoError(err)
	err = tn.pubChain.BSCChain.CheckTxStatus(txHash.Hex())
	tn.Require().NoError(err)

	hasRole, err = outboundInstance.HasRole(&bind.CallOpts{}, operatorRole, preAddr)
	tn.Require().NoError(err)
	tn.Require().False(hasRole)
	hasRole, err = outboundInstance.HasRole(&bind.CallOpts{}, operatorRole, newAddr)
	tn.Require().NoError(err)
	tn.Require().True(hasRole)
}

func TestEvent(t *testing.T) {
	suite.Run(t, new(TestNetTestSuite))
}
