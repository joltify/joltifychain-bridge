package pubchain

import (
	"context"
	"sync"
	"testing"

	"github.com/cosmos/cosmos-sdk/crypto/keys/secp256k1"
	sdk "github.com/cosmos/cosmos-sdk/types"
	banktypes "github.com/cosmos/cosmos-sdk/x/bank/types"
	"github.com/stretchr/testify/assert"

	"gitlab.com/joltify/joltifychain-bridge/common"
	"gitlab.com/joltify/joltifychain-bridge/config"
	"gitlab.com/joltify/joltifychain-bridge/misc"
)

func TestCosmosSubscribeAndTerminateAndMsgProcess(t *testing.T) {
	wg := sync.WaitGroup{}
	retryOutboundReq := sync.Map{}
	var cfg config.Config
	cfg.AtomChain.GrpcAddress = misc.MockGrpcAddress
	cfg.AtomChain.HTTPAddress = misc.MockHTTPAddress
	cfg.PubChainConfig.WsAddressBSC = misc.WebsocketTest
	cfg.PubChainConfig.WsAddressETH = misc.WebsocketTest
	cfg.PubChainConfig.WsAddressAVA = misc.WebsocketTest
	cfg.PubChainConfig.WsAddressPOL = misc.WebsocketTest
	ctxLocal, cancelLocal := context.WithCancel(context.Background())
	defer cancelLocal()
	ins, err := NewChainInstance(ctxLocal, cfg, nil, nil, &wg, &retryOutboundReq)
	assert.NoError(t, err)

	err = ins.CosChain.StartSubscription()
	assert.NoError(t, err)

	chanGet := ins.GetCosChannelQueueNewBlockChan()
	<-chanGet

	err = ins.CosChain.CosHandler.RetryChain(true)
	assert.NoError(t, err)
	<-chanGet

	var mockAddr []sdk.AccAddress
	for i := 0; i < 5; i++ {
		sk := secp256k1.GenPrivKey()
		addr, err := sdk.AccAddressFromHexUnsafe(sk.PubKey().Address().String())
		assert.NoError(t, err)
		mockAddr = append(mockAddr, addr)
	}

	poolAddr := []sdk.AccAddress{mockAddr[0], mockAddr[1]}

	mockMsg := banktypes.MsgSend{
		FromAddress: mockAddr[2].String(),
		ToAddress:   mockAddr[3].String(),
		Amount:      []sdk.Coin{sdk.NewCoin("uatom", sdk.NewInt(0))},
	}

	mockMemo := common.BridgeMemo{
		ChainType: "whatever",
		Dest:      "whatever",
	}

	_, err = ins.CosChain.processMsg(100, poolAddr, mockMemo, &mockMsg, []byte("0x1"))
	assert.ErrorContains(t, err, "zero amount")
	mockMsg.Amount = []sdk.Coin{sdk.NewCoin("uatom", sdk.NewInt(100))}
	_, err = ins.CosChain.processMsg(100, poolAddr, mockMemo, &mockMsg, []byte("0x1"))
	assert.ErrorContains(t, err, "invalid destination")

	mockMemo.Dest = mockAddr[4].String()
	_, err = ins.CosChain.processMsg(100, poolAddr, mockMemo, &mockMsg, []byte("0x1"))
	assert.ErrorContains(t, err, "not a top up message to the pool")

	mockMsg.ToAddress = mockAddr[0].String()
	_, err = ins.CosChain.processMsg(100, poolAddr, mockMemo, &mockMsg, []byte("zz"))
	assert.NoError(t, err)

	mockMsg.ToAddress = mockAddr[1].String()
	_, err = ins.CosChain.processMsg(100, poolAddr, mockMemo, &mockMsg, []byte("0x123"))
	assert.NoError(t, err)

	err = ins.CosChain.Terminate()
	assert.NoError(t, err)
}
