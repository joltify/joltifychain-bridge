package pubchain

import (
	"context"
	"encoding/base64"
	"encoding/hex"
	"errors"
	"fmt"
	"html"
	"math/big"
	"time"

	"github.com/cenkalti/backoff/v4"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	common3 "github.com/joltify-finance/tss/common"

	joltcommon "gitlab.com/joltify/joltifychain-bridge/common"
	"gitlab.com/joltify/joltifychain-bridge/config"
	"gitlab.com/joltify/joltifychain-bridge/generated"
	"gitlab.com/joltify/joltifychain-bridge/misc"
	"gitlab.com/joltify/joltifychain-bridge/tssclient"
)

// waitToSend waits for its trun to send the message
func (pi *Instance) waitToSend(chainInfo *Erc20ChainInfo, poolAddress common.Address, targetNonce uint64) error {
	bf := backoff.NewExponentialBackOff()
	bf.MaxElapsedTime = time.Minute * 2
	bf.MaxInterval = time.Second * 20

	alreadyPassed := false
	op := func() error {
		ctx, cancel := context.WithTimeout(context.Background(), chainQueryTimeout)
		defer cancel()

		nonce, err := chainInfo.getPendingNonceWithLock(ctx, poolAddress)
		if err != nil {
			pi.logger.Error().Err(err).Msgf("fail to get the nonce of the given pool address")
			return err
		}

		if nonce == targetNonce {
			return nil
		}
		if nonce > targetNonce {
			alreadyPassed = true
			return nil
		}
		return fmt.Errorf("not our round, the current nonce is %v and we want %v", nonce, targetNonce)
	}

	err := backoff.Retry(op, bf)
	if alreadyPassed {
		pi.logger.Warn().Msgf("already passed the tx, we quit")
		return errors.New("already passed the seq")
	}
	return err
}

// SendNativeTokenBatch sends the native token to the public chain
func (pi *Instance) SendNativeTokenBatch(chainInfo *Erc20ChainInfo, index int, sender, receiver common.Address, amount *big.Int, nonce *big.Int, reqTxID string, tssReqChan chan *TssReq, tssRespChan chan map[string][]byte) (common.Hash, bool, error) {
	totalFee, gasPrice, gas, err := chainInfo.GetFeeLimitWithLock()
	if err != nil {
		pi.logger.Error().Err(err).Msgf("[%v][%v]fail to get the suggested gas price", reqTxID, chainInfo.ChainType)
		return common.Hash{}, false, err
	}

	txo, err := pi.composeTxBatch(index, sender, chainInfo.ChainID, tssReqChan, tssRespChan)
	if err != nil {
		return common.Hash{}, false, err
	}
	if nonce != nil {
		txo.Nonce = nonce
	}
	pi.logger.Info().Msgf("[%v][%v]we send %v with paid fee %v", reqTxID, chainInfo.ChainType, amount, totalFee)
	txo.Value = amount

	data, err := hex.DecodeString(reqTxID)
	if err != nil {
		panic(err)
	}
	// we add extra gas for the data storage
	gas += 512
	tx := types.NewTx(&types.LegacyTx{Nonce: nonce.Uint64(), GasPrice: gasPrice, Gas: uint64(gas), To: &receiver, Value: txo.Value, Data: data})

	signedTx, err := txo.Signer(sender, tx)
	if err != nil {
		pi.logger.Error().Err(err).Msgf("[%v][%v]fail to sign the tx", reqTxID, chainInfo.ChainType)
		return common.Hash{}, false, err
	}

	ctxSend, cancelSend := context.WithTimeout(context.Background(), chainQueryTimeout)
	defer cancelSend()

	if nonce != nil {
		err = pi.waitToSend(chainInfo, sender, nonce.Uint64())
		if err != nil {
			return signedTx.Hash(), false, err
		}
	}

	err = chainInfo.sendTransactionWithLock(ctxSend, signedTx)
	if err != nil {
		pi.logger.Error().Err(err).Msgf("[%v][%v]we fail to send the outbound native tx, have reset the eth client", reqTxID, chainInfo.ChainType)
	}
	return signedTx.Hash(), false, err
}

// SendNativeTokenForMoveFund sends the native token to the public chain
func (pi *Instance) SendNativeTokenForMoveFund(chainInfo *Erc20ChainInfo, signerPk string, sender, receiver common.Address, amount *big.Int, nonce *big.Int) (common.Hash, bool, error) {
	if nonce == nil {
		return common.Hash{}, false, errors.New("invalid nonce")
	}
	fee, price, gas, err := chainInfo.GetFeeLimitWithLock()
	if err != nil {
		pi.logger.Error().Err(err).Msg("fail to get the suggested gas price")
		return common.Hash{}, false, err
	}

	// this statement is useful in processing the tiny leftover
	if amount.Cmp(fee) != 1 {
		return common.Hash{}, true, nil
	}

	if signerPk == "" {
		lastPool := pi.GetPool()[1]
		signerPk = lastPool.Pk
	}

	latest, err := chainInfo.GetBlockByNumberWithLock(nil)
	if err != nil {
		pi.logger.Error().Err(err).Msgf("fail to get the latest height")
		return common.Hash{}, false, err
	}
	blockHeight := int64(latest.NumberU64()) / ROUNDBLOCK
	tick := html.UnescapeString("&#" + "128296" + ";")
	pi.logger.Info().Msgf(">>>>>>%v we build tss at height %v for %v>>>>>>>\n", tick, blockHeight, chainInfo.ChainType)
	txo, err := pi.composeTx(signerPk, sender, chainInfo.ChainID, blockHeight)
	if err != nil {
		return common.Hash{}, false, err
	}
	txo.Nonce = nonce
	pi.logger.Info().Msgf("we have get the signature for native token")
	sendAmount := new(big.Int).Sub(amount, fee)
	txo.Value = sendAmount

	var data []byte
	tx := types.NewTx(&types.LegacyTx{Nonce: nonce.Uint64(), GasPrice: price, Gas: uint64(gas), To: &receiver, Value: sendAmount, Data: data})

	signedTx, err := txo.Signer(sender, tx)
	if err != nil {
		pi.logger.Error().Err(err).Msg("fail to sign the tx")
		return common.Hash{}, false, err
	}

	ctxSend, cancelSend := context.WithTimeout(context.Background(), chainQueryTimeout)
	defer cancelSend()

	err = chainInfo.sendTransactionWithLock(ctxSend, signedTx)
	if err != nil {
		pi.logger.Error().Err(err).Msgf("we fail to send the outbound native tx, have reset the eth client")
	}
	return signedTx.Hash(), false, err
}

// SendTokenBatch sends the token to the public chain
func (pi *Instance) SendTokenBatch(chainInfo *Erc20ChainInfo, index int, sender, receiver common.Address, amount *big.Int, nonce *big.Int, tokenAddr, reqTxID string, tssReqChan chan *TssReq, tssRespChan chan map[string][]byte) (common.Hash, error) {
	txo, err := pi.composeTxBatch(index, sender, chainInfo.ChainID, tssReqChan, tssRespChan)
	if err != nil {
		return common.Hash{}, err
	}
	if nonce != nil {
		txo.Nonce = nonce
	}
	txo.NoSend = true

	outboundTransactor, err := generated.NewOutboundTransactor(common.HexToAddress(chainInfo.Contract.OutboundAddr), chainInfo.Client)
	if err != nil {
		pi.logger.Error().Err(err).Msgf("[%v][%v]fail to new outbound transactor[%v] while processing outbound tx", reqTxID, chainInfo.ChainType, chainInfo.Contract.OutboundAddr)
		return common.Hash{}, err
	}
	data, err := hex.DecodeString(reqTxID)
	if err != nil {
		panic(err)
	}
	readyTx, err := outboundTransactor.TransferWithData(txo, receiver, amount, common.HexToAddress(tokenAddr), data)
	if err != nil {
		pi.logger.Error().Err(err).Msgf("[%v][%v]fail to send the token to the address %v with amount %v", reqTxID, chainInfo.ChainType, receiver, amount.String())
		return common.Hash{}, err
	}

	if nonce != nil {
		err = pi.waitToSend(chainInfo, sender, nonce.Uint64())
		if err != nil {
			return readyTx.Hash(), err
		}
	}

	ctxSend, cancelSend := context.WithTimeout(context.Background(), chainQueryTimeout)
	defer cancelSend()
	err = chainInfo.sendTransactionWithLock(ctxSend, readyTx)
	if err != nil {
		// we reset the ethcliet
		pi.logger.Error().Err(err).Msgf("[%v][%v]we fail to send the outbound ERC20 tx, have reset the eth client", reqTxID, chainInfo.ChainType)
	}
	return readyTx.Hash(), err
}

// SendToken sends the token to the public chain
func (pi *Instance) SendToken(chainInfo *Erc20ChainInfo, signerPk string, sender, receiver common.Address, amount *big.Int, nonce *big.Int, tokenAddr string) (common.Hash, error) {
	if signerPk == "" {
		lastPool := pi.GetPool()[1]
		signerPk = lastPool.Pk
	}

	latest, err := chainInfo.GetBlockByNumberWithLock(nil)
	if err != nil {
		pi.logger.Error().Err(err).Msgf("fail to get the latest height")
		return common.Hash{}, err
	}

	blockHeight := int64(latest.NumberU64()) / ROUNDBLOCK

	tick := html.UnescapeString("&#" + "128296" + ";")
	pi.logger.Info().Msgf(">>>>>>%v we build tss at height %v>>>>>>>\n", tick, blockHeight)
	txo, err := pi.composeTx(signerPk, sender, chainInfo.ChainID, blockHeight)
	if err != nil {
		return common.Hash{}, err
	}
	if nonce != nil {
		txo.Nonce = nonce
	}
	pi.logger.Info().Msgf("we have get the signature from tss module")
	txo.NoSend = true
	tokenInstance, err := generated.NewToken(common.HexToAddress(tokenAddr), chainInfo.Client)
	if err != nil {
		pi.logger.Error().Err(err).Msgf("fail to generate token instance for %v while processing outbound tx", tokenAddr)
		return common.Hash{}, err
	}
	readyTx, err := tokenInstance.Transfer(txo, receiver, amount)
	if err != nil {
		pi.logger.Error().Err(err).Msgf("fail to send the token to the address %v with amount %v", receiver, amount.String())
		return common.Hash{}, err
	}

	ctxSend, cancelSend := context.WithTimeout(context.Background(), chainQueryTimeout)
	defer cancelSend()

	if nonce != nil {
		err = pi.waitToSend(chainInfo, sender, nonce.Uint64())
		if err != nil {
			return readyTx.Hash(), err
		}
	}
	err = chainInfo.sendTransactionWithLock(ctxSend, readyTx)
	if err != nil {
		// we reset the ethcliet
		pi.logger.Error().Err(err).Msgf("we fail to send the outbound ERC20 tx, have reset the eth client")
	}
	return readyTx.Hash(), err
}

// SetOperator sets the operator
func (pi *Instance) SetOperator(outboundInstance *generated.Outbound, chainInfo *Erc20ChainInfo, signerPk string, sender, receiver common.Address, nonce *big.Int) (common.Hash, error) {
	if nonce == nil {
		return common.Hash{}, errors.New("invalid nonce")
	}

	if signerPk == "" {
		lastPool := pi.GetPool()[1]
		signerPk = lastPool.Pk
	}
	latest, err := chainInfo.GetBlockByNumberWithLock(nil)
	if err != nil {
		pi.logger.Error().Err(err).Msgf("[%v]fail to get the latest height", chainInfo.ChainType)
		return common.Hash{}, err
	}

	blockHeight := int64(latest.NumberU64()) / ROUNDBLOCK
	tick := html.UnescapeString("&#" + "128296" + ";")
	pi.logger.Info().Msgf(">>>>>>%v we set operator at height %v for %v>>>>>>>\n", tick, blockHeight, chainInfo.ChainType)
	txo, err := pi.composeTx(signerPk, sender, chainInfo.ChainID, blockHeight)
	if err != nil {
		return common.Hash{}, err
	}
	txo.Nonce, txo.NoSend = nonce, true
	pi.logger.Info().Msgf("[%v]we have get the signature for set operator", chainInfo.ChainType)

	tx, err := outboundInstance.SetOperator(txo, receiver)
	if err != nil {
		pi.logger.Error().Err(err).Msgf("[%v]fail to set the operator for %v", chainInfo.ChainType, receiver.String())
		return common.Hash{}, err
	}

	signedTx, err := txo.Signer(sender, tx)
	if err != nil {
		pi.logger.Error().Err(err).Msgf("[%v]fail to sign the set operator tx", chainInfo.ChainType)
		return common.Hash{}, err
	}

	ctxSend, cancelSend := context.WithTimeout(context.Background(), chainQueryTimeout)
	defer cancelSend()

	err = chainInfo.sendTransactionWithLock(ctxSend, signedTx)
	if err != nil {
		pi.logger.Error().Err(err).Msgf("[%v]we fail to send the set operator tx", chainInfo.ChainType)
	}
	return signedTx.Hash(), err
}

// ProcessOutBound send the money to public chain
func (pi *Instance) ProcessOutBound(chainInfo *Erc20ChainInfo, index int, reqItem *joltcommon.OutBoundReq, tssReqChan chan *TssReq, tssRespChan chan map[string][]byte) (string, error) {
	toAddr := common.BytesToAddress(reqItem.OutReceiverAddress)
	fromAddr := common.BytesToAddress(reqItem.FromPoolAddr)
	tokenAddr := reqItem.TokenAddr
	amount := reqItem.Coin.Amount.BigInt()
	nonce := reqItem.Nonce
	reqTxID := reqItem.TxID // the tx id that request to send the outbound tx

	pi.logger.Info().Msgf("[%v][%v]>>>>from addr %v to addr %v with amount %v of %v\n", reqTxID, chainInfo.ChainType, fromAddr, toAddr, sdk.NewDecFromBigIntWithPrec(amount, 18), tokenAddr)

	var txHash common.Hash
	var err error
	if tokenAddr == config.NativeSign {
		tokenAddr = `0x0000000000000000000000000000000000000000`
	}
	txHash, err = pi.SendTokenBatch(chainInfo, index, fromAddr, toAddr, amount, new(big.Int).SetUint64(nonce), tokenAddr, reqTxID, tssReqChan, tssRespChan)
	tssReqChan <- &TssReq{Index: index, Data: []byte("done")}
	if err != nil {
		if err.Error() == "already known" {
			pi.logger.Warn().Msgf("[%v][%v]the tx has been submitted by others", reqTxID, chainInfo.ChainType)
			return txHash.Hex(), nil
		}
		pi.logger.Error().Err(err).Msgf("[%v][%v]fail to send the token with err %v", reqTxID, chainInfo.ChainType, err)
		return txHash.Hex(), err
	}

	return txHash.Hex(), nil
}

func (pi *Instance) tssSign(msg []byte, pk string, blockHeight int64) ([]byte, error) {
	encodedMsg := base64.StdEncoding.EncodeToString(msg)
	resp, err := pi.tssServer.KeySign(pk, []string{encodedMsg}, blockHeight, nil, tssclient.TssVersion)
	if err != nil {
		pi.logger.Error().Err(err).Msg("fail to run the keysign")
		return nil, err
	}

	if resp.Status != common3.Success {
		pi.logger.Error().Err(err).Msg("fail to generate the signature")
		// todo we need to handle the blame
		return nil, err
	}
	if len(resp.Signatures) != 1 {
		pi.logger.Error().Msgf("we should only have 1 signature")
		return nil, errors.New("more than 1 signature received")
	}
	signature, err := misc.SerializeSig(&resp.Signatures[0], true)
	if err != nil {
		pi.logger.Error().Msgf("fail to encode the signature")
		return nil, err
	}
	return signature, nil
}

func (pi *Instance) TssSignBatch(msgs [][]byte, pk string, blockHeight int64) (map[string][]byte, error) {
	var encodedMSgs []string
	for _, el := range msgs {
		encodedMsg := base64.StdEncoding.EncodeToString(el)
		encodedMSgs = append(encodedMSgs, encodedMsg)
	}
	resp, err := pi.tssServer.KeySign(pk, encodedMSgs, blockHeight, nil, tssclient.TssVersion)
	if err != nil {
		pi.logger.Error().Err(err).Msg("fail to run the keysign")
		return nil, err
	}

	if resp.Status != common3.Success {
		pi.logger.Error().Err(err).Msg("fail to generate the signature")
		// todo we need to handle the blame
		return nil, err
	}
	signatureMap := make(map[string][]byte)
	for _, eachSign := range resp.Signatures {
		s := eachSign
		signature, err := misc.SerializeSig(&s, true)
		if err != nil {
			pi.logger.Error().Msgf("fail to encode the signature")
			continue
		}
		signatureMap[eachSign.Msg] = signature
	}
	return signatureMap, nil
}

func (pi *Instance) composeTx(signerPk string, sender common.Address, chainID *big.Int, blockHeight int64) (*bind.TransactOpts, error) {
	if chainID == nil {
		return nil, bind.ErrNoChainID
	}
	signer := types.LatestSignerForChainID(chainID)
	return &bind.TransactOpts{
		From: sender,
		Signer: func(address common.Address, tx *types.Transaction) (*types.Transaction, error) {
			if address != sender {
				return nil, errors.New("the address is different from the sender")
			}
			msg := signer.Hash(tx).Bytes()
			signature, err := pi.tssSign(msg, signerPk, blockHeight)
			if err != nil || len(signature) != 65 {
				return nil, errors.New("fail to sign the tx")
			}
			return tx.WithSignature(signer, signature)
		},
		Context: context.Background(),
	}, nil
}

func (pi *Instance) composeTxBatch(index int, sender common.Address, chainID *big.Int, tssReqChan chan *TssReq, tssRespChan chan map[string][]byte) (*bind.TransactOpts, error) {
	if chainID == nil {
		return nil, bind.ErrNoChainID
	}
	signer := types.LatestSignerForChainID(chainID)
	return &bind.TransactOpts{
		From: sender,
		Signer: func(address common.Address, tx *types.Transaction) (*types.Transaction, error) {
			if address != sender {
				return nil, errors.New("the address is different from the sender")
			}
			msg := signer.Hash(tx).Bytes()
			tssReqChan <- &TssReq{Data: msg, Index: index}
			signature := <-tssRespChan
			if signature == nil {
				return nil, errors.New("fail to sign the tx")
			}

			expected := base64.StdEncoding.EncodeToString(msg)
			sig, ok := signature[expected]
			if !ok {
				return nil, errors.New("fail to sign the tx")
			}
			return tx.WithSignature(signer, sig)
		},
		Context: context.Background(),
	}, nil
}
