package bridge

import (
	"bytes"
	"context"
	"fmt"
	"html"
	"log"
	"math/big"
	"os"
	"os/signal"
	"path"
	"sort"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/cenkalti/backoff/v4"
	sdk "github.com/cosmos/cosmos-sdk/types"
	banktypes "github.com/cosmos/cosmos-sdk/x/bank/types"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/pkg/errors"
	zlog "github.com/rs/zerolog/log"
	tendertypes "github.com/tendermint/tendermint/types"
	"go.uber.org/atomic"
	"golang.org/x/term"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	joltcommon "gitlab.com/joltify/joltifychain-bridge/common"
	"gitlab.com/joltify/joltifychain-bridge/config"
	"gitlab.com/joltify/joltifychain-bridge/cosbridge"
	"gitlab.com/joltify/joltifychain-bridge/monitor"
	"gitlab.com/joltify/joltifychain-bridge/pubchain"
	"gitlab.com/joltify/joltifychain-bridge/storage"
	"gitlab.com/joltify/joltifychain-bridge/tokenlist"
	"gitlab.com/joltify/joltifychain-bridge/tssclient"
)

type PreviousTssBlockOutBound struct {
	BscBlockHeight,
	EthBlockHeight,
	AvaBlockHeight,
	PolBlockHeight,
	AtomBlockHeight uint64
}

// TODO: remove this struct after pause height is moved to outbountInfo
type OutboundPauseHeight struct {
	pauseBSC,
	pauseETH,
	pauseAVA,
	pausePOL,
	pauseCOSMOS uint64
}

func (p *OutboundPauseHeight) SetHeight(h uint64, chainType string) {
	switch chainType {
	case config.BSC:
		p.pauseBSC = h
	case config.ETH:
		p.pauseETH = h
	case config.AVA:
		p.pauseAVA = h
	case config.POL:
		p.pausePOL = h
	case config.ATOM:
		p.pauseCOSMOS = h
	default:
		panic("unknown chain type")
	}
}

func (p *OutboundPauseHeight) GetHeight(chainType string) uint64 {
	switch chainType {
	case config.BSC:
		return p.pauseBSC
	case config.ETH:
		return p.pauseETH
	case config.AVA:
		return p.pauseAVA
	case config.POL:
		return p.pausePOL
	case config.ATOM:
		return p.pauseCOSMOS
	default:
		panic("unknown chain type")
	}
}

func (p *PreviousTssBlockOutBound) SetHeight(h uint64, chainType string) {
	switch chainType {
	case config.BSC:
		p.BscBlockHeight = h
	case config.ETH:
		p.EthBlockHeight = h
	case config.AVA:
		p.AvaBlockHeight = h
	case config.POL:
		p.PolBlockHeight = h
	case config.ATOM:
		p.AtomBlockHeight = h
	default:
		panic("unknown chain type")
	}
}

func (p *PreviousTssBlockOutBound) GetHeight(chainType string) uint64 {
	switch chainType {
	case config.BSC:
		return p.BscBlockHeight
	case config.ETH:
		return p.EthBlockHeight
	case config.AVA:
		return p.AvaBlockHeight
	case config.POL:
		return p.PolBlockHeight
	case config.ATOM:
		return p.AtomBlockHeight
	default:
		panic("unknown chain type")
	}
}

func readPassword() ([]byte, error) {
	var fd int
	fmt.Printf("please input the password:")
	if term.IsTerminal(syscall.Stdin) {
		fd = syscall.Stdin
	} else {
		tty, err := os.Open("/dev/tty")
		if err != nil {
			return nil, errors.Wrap(err, "error allocating terminal")
		}
		defer tty.Close()
		fd = int(tty.Fd())
	}

	pass, err := term.ReadPassword(fd)
	return pass, err
}

// NewBridgeService starts the new bridge service
func NewBridgeService(cfg config.Config) {
	pass, err := readPassword()
	if err != nil {
		log.Fatalf("fail to read the password with err %v\n", err)
	}
	wg := &sync.WaitGroup{}
	ctx, cancel := context.WithCancel(context.Background())
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	passcodeLength := 32
	passcode := make([]byte, passcodeLength)
	n, err := os.Stdin.Read(passcode)
	if err != nil {
		cancel()
		return
	}
	if n > passcodeLength {
		cancel()
		log.Fatalln("the passcode is too long")
		return
	}

	metrics := monitor.NewMetric()
	if cfg.EnableMonitor {
		metrics.Enable()
	}

	// now we load the token list

	tokenPath := path.Join(cfg.HomeDir, cfg.TokenListPath)
	tl, err := tokenlist.NewTokenList(tokenPath, int64(cfg.TokenListUpdateGap))
	if err != nil {
		fmt.Printf("fail to load token list")
		cancel()
		return
	}

	// fixme, in docker it needs to be changed to basehome
	tssServer, _, err := tssclient.StartTssServer(cfg.HomeDir, cfg.TssConfig)
	if err != nil {
		cancel()
		log.Fatalln("fail to start the tss")
		return
	}

	retryPools := joltcommon.NewRetryPools()

	joltBridge, err := cosbridge.NewJoltifyBridge(ctx, wg, cfg.CosChain.GrpcAddress, cfg.CosChain.HTTPAddress, tssServer, tl, retryPools)
	if err != nil {
		cancel()
		log.Fatalln("fail to create the joltify_bridge", err)
		return
	}

	keyringPath := path.Join(cfg.HomeDir, cfg.KeyringAddress)

	dat, err := os.ReadFile(keyringPath)
	if err != nil {
		cancel()
		log.Fatalln("error in read keyring file")
		return
	}

	err = joltBridge.InitValidators(cfg.CosChain.HTTPAddress)
	if err != nil {
		fmt.Printf("error in init the validators %v", err)
		cancel()
		return
	}
	tssHTTPServer := NewCosHttpServer(ctx, cfg.TssConfig.HTTPAddr, joltBridge.GetTssNodeID(), joltBridge)

	wg.Add(1)
	ret := tssHTTPServer.Start(wg)
	if ret != nil {
		cancel()
		return
	}

	// now we monitor the bsc transfer event
	pubChainInstance, err := pubchain.NewChainInstance(ctx, cfg, tssServer, tl, wg, joltBridge.RetryOutboundReq)
	if err != nil {
		fmt.Printf("fail to create the pub chain: %v\n", err)
		cancel()
		return
	}

	err = joltBridge.SetKey("operator", dat, pass)
	if err != nil {
		cancel()
		fmt.Printf("fail to set the keyring %v\n", err)
		return
	}

	err = pubChainInstance.SetKey("operator", dat, pass)
	if err != nil {
		cancel()
		fmt.Printf("fail to set the keyring %v\n", err)
		return
	}
	pass = []byte{}
	_ = pass

	defer func() {
		err := joltBridge.TerminateBridge()
		if err != nil {
			fmt.Printf(">>>>err %v\n", err)
		}
	}()

	defer func() {
		err := pubChainInstance.CosChain.Terminate()
		if err != nil {
			zlog.Logger.Error().Err(err).Msgf("fail to terminate the cos chain")
		}
		for _, chainType := range config.EvmChains {
			evmChainInfo := pubChainInstance.GetChainClientERC20(chainType)
			evmChainInfo.Terminate()
		}
	}()

	fsm := storage.NewTxStateMgr(cfg.HomeDir)
	// now we load the existing outbound requests
	items, err := fsm.LoadOutBoundState()
	if err != nil {
		zlog.Logger.Error().Err(err).Msg("fail to load the existing outbound requests")
	}
	if items != nil {
		for _, el := range items {
			joltBridge.AddItem(el)
		}
		zlog.Logger.Info().Msg("we have loaded the unprocessed outbound tx")
	}

	// now we load the existing inbound requests
	itemsIn, err := fsm.LoadInBoundState()
	if err != nil {
		zlog.Logger.Error().Err(err).Msg("fail to load the existing inbound requests")
	}
	if itemsIn != nil {
		for _, el := range itemsIn {
			pubChainInstance.AddInBoundItem(el)
		}
		zlog.Logger.Info().Msg("we have loaded the unprocessed inbound tx")
	}

	moveFundMgr := storage.NewMoveFundStateMgr(cfg.HomeDir)

	pubItems, err := moveFundMgr.LoadPendingItems()
	if err != nil {
		zlog.Logger.Error().Err(err).Msgf("fail to load the pending move fund, we skip!!")
	}

	if len(pubItems) != 0 {
		for _, item := range pubItems {
			pubChainInstance.AddMoveFundItem(item.PoolInfo, item.Height, item.ChainType)
		}
	}

	addEventLoop(ctx, wg, joltBridge, pubChainInstance, metrics, int64(cfg.CosChain.RollbackGap), int64(cfg.PubChainConfig.RollbackGap), tl, cfg.CosChain.GrpcAddress)
	zlog.Logger.Info().Msg("waiting for exit signal")
	<-c
	cancel()
	wg.Wait()

	items1 := joltBridge.DumpQueue()
	zlog.Logger.Info().Msgf("dump queue items %v", len(items1))
	for _, el := range items1 {
		joltBridge.AddItem(el)
	}

	items2 := pubChainInstance.DumpQueue()
	zlog.Logger.Info().Msgf("dump pub queue items %v", len(items2))
	for _, el := range items2 {
		pubChainInstance.AddInBoundItem(el)
	}

	itemsexported := joltBridge.ExportItems()
	zlog.Logger.Info().Msgf("dump outbount items %v", len(itemsexported))
	err = fsm.SaveOutBoundState(itemsexported)
	if err != nil {
		zlog.Logger.Error().Err(err).Msgf("fail to save the outbound requests!!!")
	}

	itemsInexported := pubChainInstance.ExportItems()
	zlog.Logger.Info().Msgf("dump inbount items %v", len(itemsInexported))
	err = fsm.SaveInBoundState(itemsInexported)
	if err != nil {
		zlog.Logger.Error().Err(err).Msgf("fail to save the outbound requests!!!")
	}

	// we now save the move fund items
	pubItemsSave := pubChainInstance.ExportMoveFundItems()
	zlog.Logger.Info().Msgf("dump move fund items %v", len(pubItemsSave))

	err = moveFundMgr.SavePendingItems(pubItemsSave)
	if err != nil {
		zlog.Logger.Error().Err(err).Msgf("fail to save move fund items")
	}

	zlog.Logger.Info().Msgf("we quit the bridge gracefully")
}

func addEventLoop(ctx context.Context, wg *sync.WaitGroup, joltifyChain *cosbridge.JoltChainInstance, pi *pubchain.Instance, metric *monitor.Metric, cosChainRollbackGap int64, pubRollbackGap int64, tl *tokenlist.TokenList, joltGrpc string) {
	err := joltifyChain.CosHandler.StartSubscription()
	if err != nil {
		fmt.Printf("[%v]fail to start the subscription: %v\n", config.JOLTIFY, err)
		return
	}

	err = pi.CosChain.StartSubscription()
	if err != nil {
		fmt.Printf("[%v]fail to start the subscription: %v\n", config.ATOM, err)
		return
	}
	// pubNewBlockChan is the channel for the new blocks for the public chain
	for _, chainType := range config.EvmChains {
		evmChainInfo := pi.GetChainClientERC20(chainType)
		err = evmChainInfo.StartSubscription()
		if err != nil {
			fmt.Printf("[%v]fail to subscribe the token transfer with err %v\n", chainType, err)
			return
		}
	}

	blockHeight, err := joltifyChain.GetLastBlockHeightWithLock()
	if err != nil {
		fmt.Printf("[%v]we fail to get the latest block height: %v\n", "joltify", err)
		return
	}

	atomBlockHeight, err := pi.CosChain.CosHandler.GetLastBlockHeightWithLock()
	if err != nil {
		fmt.Printf("[%v]we fail to get the latest block height: %v\n", config.ATOM, err)
		return
	}

	previousTssBlockOutBound := &PreviousTssBlockOutBound{
		AtomBlockHeight: uint64(atomBlockHeight),
	}

	for _, chainType := range config.EvmChains {
		evmChainInfo := pi.GetChainClientERC20(chainType)
		height, err := evmChainInfo.GetLastBlockHeightWithLock()
		if err != nil {
			fmt.Printf("[%v]we fail to get the latest block height: %v\n", chainType, err)
			return
		}
		previousTssBlockOutBound.SetHeight(height, chainType)
	}

	previousTssBlockInbound := blockHeight

	localSubmitLocker := sync.Mutex{}
	inKeygenInProgress := atomic.NewBool(false)

	firstTimeInbound := true
	failedInbound := atomic.NewInt32(0)
	inboundPauseHeight := int64(0)
	inBoundWait := atomic.NewBool(false)
	inBoundProcessDone := atomic.NewBool(true)

	outboundInfos := newOutboundInfos()

	wg.Add(len(config.EvmChains) + 1)
	for _, chainType := range config.EvmChains {
		evmChainInfo := pi.GetChainClientERC20(chainType)
		go func() {
			defer wg.Done()
			evmChainInfo.EvmSubscribe(pi)
		}()
	}

	go func(wg *sync.WaitGroup) {
		defer wg.Done()
		for {
			select {
			case <-ctx.Done():
				zlog.Logger.Info().Msgf("we quit the whole process")
				return
				// process the update of the validators

			case block := <-joltifyChain.GetChannelQueueValidator():
				// use validator event height, else wrong height cant get the validator
				err = joltifyChain.HandleUpdateValidators(block.Height, wg, inKeygenInProgress, false)
				if err != nil {
					zlog.Logger.Info().Msgf("error in handle update validator")
					continue
				}

			case block := <-joltifyChain.GetChannelQueueNewBlockChain():
				grpcClient, err := grpc.Dial(joltGrpc, grpc.WithTransportCredentials(insecure.NewCredentials()))
				if err != nil {
					zlog.Logger.Error().Err(err).Msgf("fail to dial at queue new block")
					continue
				}

				latestHeight, err := joltcommon.GetLastBlockHeight(grpcClient)
				if err != nil {
					zlog.Logger.Error().Err(err).Msgf("fail to get the latest block height")
					grpcClient.Close()
					continue
				}

				currentProcessBlockHeight := block.Height

				ok, _ := joltifyChain.CheckAndUpdatePool(grpcClient, latestHeight)
				if !ok {
					// it is okay to fail to submit a pool address as other nodes can submit, as long as 2/3 nodes submit, it is fine.
					zlog.Logger.Warn().Msgf("we fail to submit the new pool address")
				}

				// now we check whether we need to update the pool
				// we query the pool from the chain directly.
				poolInfo, err := joltifyChain.QueryLastPoolAddress(grpcClient)
				if err != nil {
					zlog.Logger.Error().Err(err).Msgf("error in get pool with error %v", err)
					grpcClient.Close()
					continue
				}
				if len(poolInfo) != 2 {
					zlog.Logger.Warn().Msgf("the pool only have %v address, bridge will not work", len(poolInfo))
					grpcClient.Close()
					continue
				}

				currentPool := pi.GetPool()

				pools := joltifyChain.GetPool()

				// this means the pools has not been filled with two address
				if currentPool[0] == nil {
					for _, el := range poolInfo {
						err := pi.UpdatePool(el)
						if err != nil {
							zlog.Log().Err(err).Msgf("fail to update the pool")
						}
						joltifyChain.UpdatePool(el)
					}
					err = grpcClient.Close()
					if err != nil {
						zlog.Log().Err(err).Msgf("fail to close the grpc client")
					}
					continue
				}

				if NeedUpdate(poolInfo, currentPool) {
					err := pi.UpdatePool(poolInfo[0])
					if err != nil {
						zlog.Log().Err(err).Msgf("fail to update the pool")
					}
					previousPool := joltifyChain.UpdatePool(poolInfo[0])
					if previousPool.Pk != poolInfo[0].CreatePool.PoolPubKey {
						// we force the first try of the tx to be run without blocking by the block wait
						for _, chainType := range config.Chains {
							pi.AddMoveFundItem(previousPool, latestHeight-config.MINCHECKBLOCKGAP+5, chainType)
						}
						theSecondPool := currentPool[1]
						for _, chainType := range config.Chains {
							pi.AddMoveFundItem(theSecondPool, latestHeight-config.MINCHECKBLOCKGAP+10, chainType)
						}
					}
				}

				// we update the tx new, if there exists a processable block
				processableBlockHeight := int64(0)
				if currentProcessBlockHeight > cosChainRollbackGap {
					processableBlockHeight = currentProcessBlockHeight - cosChainRollbackGap
					processableBlock, err := joltifyChain.GetBlockByHeight(grpcClient, processableBlockHeight)
					if err != nil {
						zlog.Logger.Error().Err(err).Msgf("error in get block to process %v", err)
						err = grpcClient.Close()
						if err != nil {
							zlog.Logger.Error().Err(err).Msgf("fail to close the grpc client at get block height")
						}
						continue
					}

					amISigner, err := joltifyChain.CheckWhetherSigner(pools[1].PoolInfo)
					if err != nil {
						zlog.Logger.Error().Err(err).Msg("fail to check whether we are the node submit the mint request")
						err = grpcClient.Close()
						if err != nil {
							zlog.Logger.Error().Err(err).Msgf("fail to close the grpc client at check wehther signer")
						}
						continue
					}
					if !amISigner {
						zlog.Logger.Info().Msgf("we are not the signer in joltify chain check")
						err = grpcClient.Close()
						if err != nil {
							zlog.Logger.Error().Err(err).Msgf("fail to close the grpc client at amISigner")
						}
						continue
					}

					if amISigner {
						// here we process the outbound tx
						for _, el := range processableBlock.Data.Txs {
							joltifyChain.CheckOutBoundTx(grpcClient, processableBlockHeight, el)
						}
					}
				}

				joltifyChain.CurrentHeight = currentProcessBlockHeight

				// we update the token list, if the current block height refresh the update mark
				err = tl.UpdateTokenList(joltifyChain.CurrentHeight)
				if err != nil {
					zlog.Logger.Warn().Msgf("error in updating token list %v", err)
				}

				if failedInbound.Load() > 5 {
					failedInbound.Store(0)
					mid := (latestHeight / int64(config.ROUNDBLOCK)) + 1
					inboundPauseHeight = mid * int64(config.ROUNDBLOCK)
					inBoundWait.Store(true)
				}

				if latestHeight < inboundPauseHeight {
					zlog.Logger.Warn().Msgf("to many errors for inbound, we wait for %v blocks to continue", inboundPauseHeight-latestHeight)
					if latestHeight == inboundPauseHeight-1 {
						zlog.Logger.Info().Msgf("we now load the onhold tx")
						putOnHoldBlockInBoundBack(joltGrpc, pi, joltifyChain)
					}
					grpcClient.Close()

					continue
				}
				inBoundWait.Store(false)

				if pi.IsEmpty() {
					zlog.Logger.Debug().Msgf("the inbound queue is empty, we put all onhold back")
					putOnHoldBlockInBoundBack(joltGrpc, pi, joltifyChain)
				}

				// todo we need also to add the check to avoid send tx near the churn blocks
				if processableBlockHeight-previousTssBlockInbound >= pubchain.GroupBlockGap && !pi.IsEmpty() {
					// if we do not have enough tx to process, we wait for another round
					if pi.Size() < pubchain.GroupSign && firstTimeInbound {
						firstTimeInbound = false
						metric.UpdateInboundTxNum(float64(pi.Size()))
						err = grpcClient.Close()
						if err != nil {
							zlog.Logger.Error().Err(err).Msgf("fail to close the grpc client at first time inbound")
						}
						continue
					}

					if !inBoundProcessDone.Load() {
						zlog.Logger.Warn().Msgf("the previous inbound has not been fully processed, we do not feed more tx")
						err = grpcClient.Close()
						if err != nil {
							zlog.Logger.Error().Err(err).Msgf("fail to close the grpc client at inBoundProcessDone")
						}
						continue
					}
					if inKeygenInProgress.Load() {
						zlog.Logger.Warn().Msgf("we are in keygen process, we do not feed more tx")
						metric.UpdateOutboundTxNum(float64(joltifyChain.Size()))
						continue
					}

					zlog.Logger.Warn().Msgf("we feed the inbound tx now %v", pools[1].PoolInfo.CreatePool.PoolAddr.String())

					err = joltifyChain.FeedTx(grpcClient, pools[1].PoolInfo, pi)
					if err != nil {
						zlog.Logger.Error().Err(err).Msgf("fail to feed the tx")
					}

					inBoundProcessDone.Store(false)
					previousTssBlockInbound = currentProcessBlockHeight
					firstTimeInbound = true
					metric.UpdateInboundTxNum(float64(pi.Size()))
				}

				err = grpcClient.Close()
				if err != nil {
					zlog.Logger.Error().Err(err).Msgf("fail to close the grpc client at end loop of joltify block process")
				}

			case block := <-pi.GetCosChannelQueueNewBlockChan():
				// there is an issue on the joltify chain
				// we will skip this block and catchup it while joltify is recover
				if !joltifyChain.IsRunning() {
					continue
				}

				outboundInfo := outboundInfos[config.ATOM]
				err = catchupATOMBlock(block, pi, joltifyChain, metric, cosChainRollbackGap, outboundInfo, inKeygenInProgress, previousTssBlockOutBound)
				if err != nil {
					// retry at next round to avoid lost block
					zlog.Logger.Warn().Msgf("[%v]fail to catchup the block, current[%v], will catchup[%v] at next round", outboundInfo.chainType, outboundInfo.currentHeight, block.Height)
					continue
				}

				if err = pubChainProcessCosmos(block, pi, joltifyChain, metric, cosChainRollbackGap, outboundInfo, inKeygenInProgress, previousTssBlockOutBound); err != nil {
					zlog.Logger.Warn().Msgf("[%v]fail to process block[%v], current[%v], will catchup at next round", outboundInfo.chainType, block.Height, outboundInfo.currentHeight)
					// mark the current block as handled block
					// will catchup start at block+1
					// this will occur when the program start and the pool is not ready
					if outboundInfo.currentHeight.Int64() == 0 {
						outboundInfo.currentHeight.SetInt64(block.Height)
					}
					continue
				}
				outboundInfo.currentHeight.SetInt64(block.Height)

				// process the public chain new block event
			case blockHead := <-pi.ChannelQueue:
				// there is an issue on the joltify chain
				// we will skip this block and catchup it while joltify is recover
				if !joltifyChain.IsRunning() {
					continue
				}

				outboundInfo := outboundInfos[blockHead.ChainType]
				err = catchupEVMBlock(pi, joltifyChain, joltGrpc, metric, blockHead, pubRollbackGap, outboundInfo, inKeygenInProgress, previousTssBlockOutBound)
				if err != nil {
					// retry at next round to avoid lost block
					zlog.Logger.Warn().Msgf("[%v]fail to catchup the block, current[%v], will catchup[%v] at next round", outboundInfo.chainType, outboundInfo.currentHeight, blockHead.Head.Number)
					continue
				}

				if err = pubChainProcess(pi, joltifyChain, joltGrpc, metric, blockHead, pubRollbackGap, outboundInfo, inKeygenInProgress, previousTssBlockOutBound); err != nil {
					zlog.Logger.Warn().Msgf("[%v]fail to process block[%v], current[%v], will catchup at next round", outboundInfo.chainType, blockHead.Head.Number, outboundInfo.currentHeight)
					// mark the current block as handled block
					// will catchup start at block+1
					// this will occur when the program start and the pool is not ready
					if outboundInfo.currentHeight.Int64() == 0 {
						outboundInfo.currentHeight = blockHead.Head.Number
					}
					continue
				}
				outboundInfo.currentHeight = blockHead.Head.Number

			// process the in-bound top up event which will mint coin for users
			case itemsRecv := <-pi.InboundReqChan:
				// NOTE: is need to handle if joltify chain has an issue?
				// e.g.
				// if joltifyChain.IsRunning() {
				// 	pi.InboundReqChan <- itemsRecv
				// 	continue
				// }
				wg.Add(1)
				go func() {
					defer wg.Done()
					defer inBoundProcessDone.Store(true)
					processInbound(joltGrpc, joltifyChain, pi, itemsRecv, inBoundWait, failedInbound)
				}()

			case itemsRecv := <-joltifyChain.OutboundReqChan:
				// NOTE: is need to handle if joltify chain has an issue?
				chainType := itemsRecv[0].ChainType
				outboundInfo := outboundInfos[chainType]
				wg.Add(1)
				go func() {
					defer outboundInfo.processDone.Store(true)
					defer wg.Done()
					switch chainType {
					case config.ATOM:
						processEachOutBoundCosmos(nil, joltGrpc, joltifyChain, pi, itemsRecv, outboundInfo, &localSubmitLocker)
					default:
						// we must have at least one item in itemsRecv
						chainInfo := pi.GetChainClientERC20(chainType)
						processEachOutBoundErc20(chainInfo, joltGrpc, joltifyChain, pi, itemsRecv, outboundInfo, &localSubmitLocker)
					}
				}()

			case <-time.After(time.Second * 20):
				zlog.Logger.Info().Msgf("we should not reach here")
				for _, chainType := range config.EvmChains {
					evmChainInfo := pi.GetChainClientERC20(chainType)
					if err := evmChainInfo.RetryPubChain(true); err != nil {
						zlog.Logger.Error().Err(err).Msgf("fail to restart the %v chain", chainType)
					}
				}

				err = joltifyChain.CosHandler.RetryChain(true)
				if err != nil {
					zlog.Logger.Error().Err(err).Msgf("fail to restart the joltify chain")
				}

				err = pi.CosChain.CosHandler.RetryChain(true)
				if err != nil {
					zlog.Logger.Error().Err(err).Msgf("fail to restart the cosmos chain")
				}
			}
		}
	}(wg)
}

func catchupATOMBlock(block *tendertypes.Header, pi *pubchain.Instance, joltChain *cosbridge.JoltChainInstance, metric *monitor.Metric, cosChainRollbackGap int64, outboundInfo *outboundInfo, inKeygenInProgress *atomic.Bool, previousTssBlockOutBound *PreviousTssBlockOutBound) error {
	headerNow := block
	if outboundInfo.currentHeight.Int64() == 0 || headerNow.Height-outboundInfo.currentHeight.Int64() < 2 {
		return nil
	}

	fromHeight := outboundInfo.currentHeight.Int64() + 1
	pools := joltChain.GetPool()
	if len(pools) < 2 || pools[1] == nil {
		zlog.Logger.Warn().Msgf("[%v]the pool is not ready, we will skip catchup from[%v] to(%v)", outboundInfo.chainType, fromHeight, headerNow.Height)
		outboundInfo.currentHeight.SetInt64(headerNow.Height)
		return nil
	}

	zlog.Logger.Info().Msgf("[%v]we are catching up the block from[%v] to(%v)", outboundInfo.chainType, fromHeight, headerNow.Height)

	bf := backoff.WithMaxRetries(backoff.NewConstantBackOff(time.Second*3), 3)
	var currentBlock *tendertypes.Block
	op := func() error {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
		var err error
		height := outboundInfo.currentHeight.Int64() + 1
		currentBlock, err = pi.CosChain.CosHandler.Block(ctx, &height)
		cancel()
		return err
	}

	for headerNow.Height-outboundInfo.currentHeight.Int64() > 1 {
		err := backoff.Retry(op, bf)
		if err != nil {
			zlog.Logger.Warn().Err(err).Msgf("[%v]fail to get the block[%v]", outboundInfo.chainType, outboundInfo.currentHeight.Int64()+1)
			return err
		}

		err = pubChainProcessCosmos(&currentBlock.Header, pi, joltChain, metric, cosChainRollbackGap, outboundInfo, inKeygenInProgress, previousTssBlockOutBound)
		if err != nil {
			return err
		}
		outboundInfo.currentHeight.SetInt64(currentBlock.Height)
	}
	zlog.Logger.Info().Msgf("we caught the block from[%v] to(%v)", fromHeight, headerNow.Height)
	return nil
}

func catchupEVMBlock(pi *pubchain.Instance, joltChain *cosbridge.JoltChainInstance, joltGrpc string, metric *monitor.Metric, blockHead *pubchain.BlockHead, pubRollbackGap int64, outboundInfo *outboundInfo, inKeygenInProgress *atomic.Bool, previousTssBlockOutBound *PreviousTssBlockOutBound) error {
	headerNow := blockHead.Head
	if outboundInfo.currentHeight.Int64() == 0 || big.NewInt(0).Sub(headerNow.Number, outboundInfo.currentHeight).Int64() < 2 {
		return nil
	}

	chainInfo := pi.GetChainClientERC20(blockHead.ChainType)
	if chainInfo == nil {
		zlog.Logger.Error().Msgf("[%v]chainInfo not found in catchup", blockHead.ChainType)
		return nil
	}

	fromHeight := big.NewInt(0).Add(outboundInfo.currentHeight, big.NewInt(1))
	pools := joltChain.GetPool()
	if len(pools) < 2 || pools[1] == nil {
		zlog.Logger.Warn().Msgf("[%v]the pool is not ready, we will skip catchup from[%v] to(%v)", outboundInfo.chainType, fromHeight, headerNow.Number)
		outboundInfo.currentHeight = headerNow.Number
		return nil
	}

	zlog.Logger.Info().Msgf("[%v]we are catching up the block from[%v] to(%v)", outboundInfo.chainType, fromHeight, headerNow.Number)

	bf := backoff.WithMaxRetries(backoff.NewConstantBackOff(time.Second*3), 3)
	var head *types.Header
	op := func() error {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
		var err error
		head, err = chainInfo.Client.HeaderByNumber(ctx, big.NewInt(0).Add(outboundInfo.currentHeight, big.NewInt(1)))
		cancel()
		return err
	}

	for big.NewInt(0).Sub(headerNow.Number, outboundInfo.currentHeight).Int64() > 1 {
		err := backoff.Retry(op, bf)
		if err != nil {
			zlog.Logger.Warn().Err(err).Msgf("[%v]fail to get the block[%v]", outboundInfo.chainType, big.NewInt(0).Add(outboundInfo.currentHeight, big.NewInt(1)))
			return err
		}

		block := &pubchain.BlockHead{
			Head:      head,
			ChainType: outboundInfo.chainType,
		}
		err = pubChainProcess(pi, joltChain, joltGrpc, metric, block, pubRollbackGap, outboundInfo, inKeygenInProgress, previousTssBlockOutBound)
		if err != nil {
			return err
		}
		outboundInfo.currentHeight = head.Number
	}
	zlog.Logger.Info().Msgf("we caught the block from[%v] to(%v)", fromHeight, headerNow.Number)
	return nil
}

func processInbound(oppyGrpc string, joltChain *cosbridge.JoltChainInstance, pi *pubchain.Instance, items []*joltcommon.InBoundReq, inBoundWait *atomic.Bool, failedInbound *atomic.Int32) {
	grpcClient, err := grpc.Dial(oppyGrpc, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		zlog.Logger.Error().Err(err).Msgf("fail to dial the grpc end-point")
		return
	}
	defer grpcClient.Close()

	itemsMap := make(map[string]*joltcommon.InBoundReq)
	for _, el := range items {
		itemsMap[el.Hash().Hex()] = el
	}

	needToBeProcessedItems := make([]*joltcommon.InBoundReq, 0, len(items))
	for _, item := range items {
		// we need to check against the previous account sequence
		index := item.Hash().Hex()
		if joltChain.CheckWhetherAlreadyExist(grpcClient, index) {
			zlog.Logger.Warn().Msg("already submitted by others")
			continue
		}
		needToBeProcessedItems = append(needToBeProcessedItems, item)
	}
	// if all the tx has been processed, we quit
	if len(needToBeProcessedItems) == 0 {
		failedInbound.Store(0)
		return
	}

	var hashIndexMap map[string]string
	hashIndexMap, needToBeProcessedItems, err = joltChain.DoProcessInBound(grpcClient, needToBeProcessedItems)
	if err != nil {
		// we add all the txs back to wait list
		pi.AddItemsToOnHoldQueue(needToBeProcessedItems)
		return
	}

	if len(hashIndexMap) == 0 {
		return
	}

	wg := sync.WaitGroup{}
	for index, txHash := range hashIndexMap {
		wg.Add(1)
		go func(eachIndex, eachTxHash string) {
			defer wg.Done()

			grpcClientLocal, errLocal := grpc.Dial(oppyGrpc, grpc.WithTransportCredentials(insecure.NewCredentials()))
			if errLocal != nil {
				zlog.Logger.Error().Err(errLocal).Msgf("fail to dial the grpc end-point")
				return
			}
			defer grpcClientLocal.Close()

			err := joltChain.CheckIssueTokenTxStatus(grpcClientLocal, eachIndex, 10)
			if err != nil {
				zlog.Logger.Error().Err(err).Msgf("tx[%v] has not been successfully submitted retry", itemsMap[eachIndex].HexTxID())
				if !inBoundWait.Load() {
					failedInbound.Inc()
				}
				pi.AddOnHoldQueue(itemsMap[eachIndex])
				return
			}
			tick := html.UnescapeString("&#" + "128229" + ";")
			if eachTxHash == "" {
				failedInbound.Store(0)
				zlog.Logger.Info().Msgf("%v tx[%v] have successfully top up by others", tick, itemsMap[eachIndex].HexTxID())
			} else {
				failedInbound.Store(0)
				zlog.Logger.Info().Msgf("%v tx[%v] have successfully top up with %v", tick, itemsMap[eachIndex].HexTxID(), eachTxHash)
			}
		}(index, txHash)
	}
	wg.Wait()
}

func needToBeProcessed(chainInfo *pubchain.Erc20ChainInfo, oppyChain *cosbridge.JoltChainInstance, pi *pubchain.Instance, items []*joltcommon.OutBoundReq, isCosmos bool) []*joltcommon.OutBoundReq {
	checkWg := sync.WaitGroup{}
	needToBeProcessedItems := make([]*joltcommon.OutBoundReq, 0)
	needToBeProcessedLock := sync.Mutex{}
	validators, _ := oppyChain.GetLastValidator()
	chainType := config.ATOM
	if chainInfo != nil {
		chainType = chainInfo.ChainType
	}
	for _, el := range items {
		if el == nil {
			continue
		}
		checkWg.Add(1)
		go func(each *joltcommon.OutBoundReq) {
			defer checkWg.Done()
			submittedTx, err := oppyChain.GetPubChainSubmittedTx(*each, len(validators))
			if err != nil {
				zlog.Logger.Info().Err(err).Msgf("[%v][%v]len(validator)=[%v]we continue process this tx as it has not been submitted", each.TxID, chainType, len(validators))
				needToBeProcessedLock.Lock()
				needToBeProcessedItems = append(needToBeProcessedItems, each)
				needToBeProcessedLock.Unlock()
				return
			}

			if submittedTx != "" {
				zlog.Logger.Info().Msgf("[%v][%v]len(validator)=[%v]we check whether someone has already submitted this tx %v", each.TxID, chainType, len(validators), submittedTx)

				if isCosmos {
					err := pi.CosChain.CosHandler.QueryTxStatus(pi.CosChain.CosHandler.GrpcClient, submittedTx, 10)
					if err == nil {
						zlog.Logger.Info().Msgf("[%v][%v]this tx has been submitted by others, we skip it", each.TxID, chainType)
						return
					}
				}

				err := chainInfo.CheckTxStatus(submittedTx)
				if err == nil {
					zlog.Logger.Info().Msgf("[%v][%v]this tx has been submitted by others, we skip it", each.TxID, chainType)
					return
				}
				needToBeProcessedLock.Lock()
				needToBeProcessedItems = append(needToBeProcessedItems, each)
				needToBeProcessedLock.Unlock()
			}
		}(el)
	}
	checkWg.Wait()
	return needToBeProcessedItems
}

func processSuccessfulTx(failedOutBound *atomic.Int32, oppyGrpc string, localSubmitLocker *sync.Mutex, oppyChain *cosbridge.JoltChainInstance, pi *pubchain.Instance, item *joltcommon.OutBoundReq, txHash string) {
	failedOutBound.Store(0)
	// now we submit our public chain tx to oppychain
	localSubmitLocker.Lock()
	defer localSubmitLocker.Unlock()
	bf := backoff.NewExponentialBackOff()
	bf.MaxElapsedTime = time.Minute
	bf.MaxInterval = time.Second * 10
	op := func() error {
		grpcClient, err := grpc.Dial(oppyGrpc, grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			return err
		}
		defer grpcClient.Close()

		// we need to submit the pool created height as the validator may change in chain cosmos staking module
		// since we have started process the block, it is confirmed we have two pools
		pools := pi.GetPool()
		poolCreateHeight, err := strconv.ParseInt(pools[1].PoolInfo.BlockHeight, 10, 64)
		if err != nil {
			panic("block height convert should never fail")
		}
		errInner := oppyChain.SubmitOutboundTx(grpcClient, nil, item.Hash().Hex(), poolCreateHeight, txHash, item.FeeToValidator, item.ChainType, item.TxID, item.OutReceiverAddress, item.NeedMint)
		return errInner
	}
	err := backoff.Retry(op, bf)
	if err != nil {
		zlog.Logger.Error().Err(err).Msgf("[%v][%v]we have tried but failed to submit the record with backoff", item.ChainType, item.TxID)
		return
	}
}

// doProcessOutBound process the outbound tx
func doProcessOutBound(grpcAddr string, items []*joltcommon.OutBoundReq, pi *pubchain.Instance) (map[string]*joltcommon.OutBoundReq, error) {
	grpcClient, err := grpc.Dial(grpcAddr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		zlog.Logger.Error().Err(err).Msgf("fail to dial the grpc end-point")
		return nil, err
	}
	defer grpcClient.Close()

	signMsgs := make([]*tssclient.TssSignigMsg, len(items))
	issueReqs := make([]sdk.Msg, len(items))

	blockHeight, err := joltcommon.GetLastBlockHeight(grpcClient)
	if err != nil {
		zlog.Logger.Error().Err(err).Msgf("fail to get the block height in process the outbount tx")
		return nil, err
	}
	roundBlockHeight := blockHeight / int64(config.ROUNDBLOCK)

	sort.Slice(items, func(i, j int) bool {
		return items[i].Nonce < items[j].Nonce
	})

	itemsIndex := make([]string, len(items))
	for i, item := range items {
		atomFrom := sdk.MustBech32ifyAddressBytes("cosmos", item.FromPoolAddr)
		receiver := item.OutReceiverAddress
		atomTo := sdk.MustBech32ifyAddressBytes("cosmos", receiver)

		msg := banktypes.MsgSend{
			FromAddress: atomFrom,
			ToAddress:   atomTo,
			Amount:      sdk.Coins{item.Coin},
		}

		signMsg := tssclient.TssSignigMsg{
			Pk:          item.FromPubkey,
			Signers:     nil,
			BlockHeight: roundBlockHeight,
			Version:     tssclient.TssVersion,
		}
		signMsgs[i] = &signMsg
		issueReqs[i] = &msg
		itemsIndex[i] = item.Hash().Hex()
	}

	// we assume all the request in the group use the same pool account
	accNum := items[0].AccNum
	accSeq := items[0].Nonce
	atomFromGroup := sdk.MustBech32ifyAddressBytes("cosmos", items[0].FromPoolAddr)

	// for batchsigning, the signMsgs for all the members in the group is the same
	txHashes, err := pi.CosChain.CosHandler.BatchComposeAndSend(grpcClient, issueReqs, accSeq, accNum, signMsgs[0], atomFromGroup, itemsIndex)
	if err != nil {
		zlog.Logger.Error().Msgf("we fail to process one or more txs")
	}

	hashIndexMap := make(map[string]*joltcommon.OutBoundReq)
	for _, el := range items {
		txHash := txHashes[el.Nonce]
		k := fmt.Sprintf("%v:%v", el.Nonce, txHash)
		hashIndexMap[k] = el
	}

	return hashIndexMap, nil
}

func processEachOutBoundCosmos(chainInfo *pubchain.Erc20ChainInfo, oppyGrpc string, oppyChain *cosbridge.JoltChainInstance, pi *pubchain.Instance, items []*joltcommon.OutBoundReq, outboundInfo *outboundInfo, localSubmitLocker *sync.Mutex) {
	needToBeProcessedItems := needToBeProcessed(chainInfo, oppyChain, pi, items, true)
	faidCount := outboundInfo.failed.Load()
	zlog.Logger.Info().Msgf("processEachOutBound %v len(needToBeProcessedItems)=%v, len(failedOutBound)=%v", config.ATOM, len(needToBeProcessedItems), faidCount)
	if len(needToBeProcessedItems) == 0 {
		outboundInfo.failed.Store(0)
		return
	}
	hashIndexMap, err := doProcessOutBound(pi.CosChain.CosHandler.GrpcAddr, needToBeProcessedItems, pi)
	if err != nil {
		// we add all the txs back to wait list
		oppyChain.AddItemsToOnHoldQueue(items)
		return
	}

	emptyHash := common.Hash{}.Hex()
	for k, item := range hashIndexMap {
		txHash := strings.Split(k, ":")[1]
		if txHash != emptyHash {
			checkTxErr := pi.CosChain.CosHandler.QueryTxStatus(pi.CosChain.CosHandler.GrpcClient, txHash, 10)
			if checkTxErr == nil {
				encodeFrom := sdk.MustBech32ifyAddressBytes("cosmos", item.FromPoolAddr)
				encodeTo := sdk.MustBech32ifyAddressBytes("cosmos", item.OutReceiverAddress)
				tick := html.UnescapeString("&#" + "11014" + ";")
				zlog.Logger.Info().Msgf("%v we have send outbound tx(%v) from %v to %v (%v)", tick, txHash, encodeFrom, encodeTo, item.Coin.Amount.String())
				processSuccessfulTx(outboundInfo.failed, oppyGrpc, localSubmitLocker, oppyChain, pi, item, txHash)
				continue
			}
		}
		// now we put this item back in retry
		if !outboundInfo.wait.Load() {
			outboundInfo.failed.Inc()
		}
		zlog.Logger.Info().Msgf("we add the txhash %v for the retry of tx %v", txHash, item.TxID)
		item.SubmittedTxHash = txHash
		oppyChain.AddOnHoldQueue(item)
	}
}

func processEachOutBoundErc20(chainInfo *pubchain.Erc20ChainInfo, oppyGrpc string, oppyChain *cosbridge.JoltChainInstance, pi *pubchain.Instance, items []*joltcommon.OutBoundReq, outboundInfo *outboundInfo, localSubmitLocker *sync.Mutex) {
	needToBeProcessedItems := needToBeProcessed(chainInfo, oppyChain, pi, items, false)
	faidCount := outboundInfo.failed.Load()
	zlog.Logger.Info().Msgf("processEachOutBound %v len(needToBeProcessedItems)=%v, len(failedOutBound)=%v", chainInfo.ChainType, len(needToBeProcessedItems), faidCount)
	if len(needToBeProcessedItems) == 0 {
		outboundInfo.failed.Store(0)
		return
	}

	// now we process each tx
	sort.Slice(needToBeProcessedItems, func(i, j int) bool {
		return needToBeProcessedItems[i].Nonce < needToBeProcessedItems[j].Nonce
	})

	allTxs := make([][]byte, len(needToBeProcessedItems))
	for i, el := range needToBeProcessedItems {
		allTxs[i] = el.Hash().Bytes()
	}
	txSeqHash := crypto.Keccak256Hash(allTxs...)

	emptyHash := common.Hash{}.Hex()
	tssWaitGroup := &sync.WaitGroup{}
	bc := pubchain.NewBroadcaster()
	tssReqChan := make(chan *pubchain.TssReq, len(needToBeProcessedItems))
	defer close(tssReqChan)
	for i, pItem := range needToBeProcessedItems {
		tssWaitGroup.Add(1)
		go func(index int, item *joltcommon.OutBoundReq) {
			var txHash string
			var err, checkTxErr error
			defer tssWaitGroup.Done()
			tssRespChan, err := bc.Subscribe(int64(index))
			if err != nil {
				panic("should not been subscribed!!")
			}
			defer bc.Unsubscribe(int64(index))

			txHash, err = pi.ProcessOutBound(chainInfo, index, item, tssReqChan, tssRespChan)

			if err != nil {
				zlog.Logger.Error().Err(err).Msgf("[%v][%v]fail to broadcast the tx", item.TxID, chainInfo.ChainType)
			}
			if txHash != emptyHash {
				amount := item.Coin.Amount.BigInt()
				checkTxErr = chainInfo.CheckTxStatus(txHash)
				if checkTxErr == nil {
					encodeFrom := common.BytesToAddress(item.FromPoolAddr)
					encodeTo := common.BytesToAddress(item.OutReceiverAddress)
					tick := html.UnescapeString("&#" + "11014" + ";")
					zlog.Logger.Info().Msgf("%v [%v][%v]we have send outbound tx(%v) from %v to %v (%v)", tick, item.TxID, chainInfo.ChainType, txHash, encodeFrom.String(), encodeTo.String(), amount.String())
					processSuccessfulTx(outboundInfo.failed, oppyGrpc, localSubmitLocker, oppyChain, pi, item, txHash)
					return
				}
			}
			zlog.Logger.Warn().Msgf("[%v][%v]the tx is fail in submission, we need to resend", item.TxID, chainInfo.ChainType)
			if !outboundInfo.wait.Load() {
				outboundInfo.failed.Inc()
			}
			item.SubmittedTxHash = txHash
			oppyChain.AddOnHoldQueue(item)
		}(i, pItem)
	}
	// here we process the tss msg in batch
	tssWaitGroup.Add(1)
	go func() {
		defer tssWaitGroup.Done()
		var allsignMSgs [][]byte
		received := make(map[int][]byte)
		collected := false
		for {
			msg := <-tssReqChan
			received[msg.Index] = msg.Data
			if len(received) >= len(needToBeProcessedItems) {
				collected = true
			}
			if collected {
				break
			}
		}
		for _, val := range received {
			if bytes.Equal([]byte("done"), val) {
				continue
			}
			allsignMSgs = append(allsignMSgs, val)
		}

		lastPool := pi.GetPool()[1]
		latest, err := chainInfo.GetBlockByNumberWithLock(nil)
		if err != nil {
			zlog.Logger.Error().Err(err).Msgf("fail to get the latest height")
			bc.Broadcast(nil)
			return
		}
		val := new(big.Int).Add(latest.Number(), txSeqHash.Big())
		consensusFig := new(big.Int).Mod(val, new(big.Int).SetUint64(9223372036854775807))
		adjBlockHeight := consensusFig.Int64() / pubchain.ROUNDBLOCK
		signature, err := pi.TssSignBatch(allsignMSgs, lastPool.Pk, adjBlockHeight)
		if err != nil {
			zlog.Logger.Info().Msgf("fail to run batch keysign")
		}
		bc.Broadcast(signature)
	}()
	tssWaitGroup.Wait()
}

func putOnHoldBlockInBoundBack(oppyGrpc string, pi *pubchain.Instance, oppyChain *cosbridge.JoltChainInstance) {
	grpcClient, err := grpc.Dial(oppyGrpc, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		zlog.Logger.Error().Err(err).Msgf("fail to dial the grpc end-point")
		return
	}
	defer grpcClient.Close()

	zlog.Logger.Debug().Msgf("we reload all the failed tx")
	itemInbound := pi.DumpQueue()
	wgDump := &sync.WaitGroup{}
	wgDump.Add(len(itemInbound))
	for _, el := range itemInbound {
		go func(each *joltcommon.InBoundReq) {
			defer wgDump.Done()
			err := oppyChain.CheckIssueTokenTxStatus(grpcClient, each.Hash().Hex(), 10)
			if err == nil {
				tick := html.UnescapeString("&#" + "127866" + ";")
				zlog.Logger.Info().Msgf(" %v the tx has been submitted, we catch up with others on oppyChain", tick)
			} else {
				pi.AddInBoundItem(each)
			}
		}(el)
	}
	wgDump.Wait()
}

func putOnHoldBlockOutBoundBack(chainInfo *pubchain.Erc20ChainInfo, joltChain *cosbridge.JoltChainInstance) {
	zlog.Logger.Debug().Msgf("we reload all the failed %v tx back", chainInfo.ChainType)
	itemsOutBound := joltChain.RetrieveItemsWithType(chainInfo.ChainType)
	wgDump := &sync.WaitGroup{}
	wgDump.Add(len(itemsOutBound))
	for _, el := range itemsOutBound {
		go func(each *joltcommon.OutBoundReq) {
			defer wgDump.Done()
			empty := common.Hash{}.Hex()
			if each.SubmittedTxHash == empty {
				joltChain.AddItem(each)
				return
			}
			err := chainInfo.CheckTxStatus(each.SubmittedTxHash)
			if err != nil {
				joltChain.AddItem(each)
				return
			}
			tick := html.UnescapeString("&#" + "127866" + ";")
			zlog.Logger.Info().Msgf(" %v the tx has been submitted, we catch up with others on pubchain", tick)
		}(el)
	}
	wgDump.Wait()
}

func putOnHoldBlockOutBoundBackCosmos(chainInfo *pubchain.CosMosChainInfo, joltChain *cosbridge.JoltChainInstance) {
	grpcClient, err := grpc.Dial(chainInfo.CosHandler.GrpcAddr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		zlog.Logger.Error().Err(err).Msgf("fail to dial the grpc end-point")
		return
	}
	defer grpcClient.Close()

	zlog.Logger.Debug().Msgf("we reload all the failed tx")
	itemsOutBound := joltChain.RetrieveItemsWithType(config.ATOM)
	wgDump := &sync.WaitGroup{}
	wgDump.Add(len(itemsOutBound))
	for _, el := range itemsOutBound {
		go func(each *joltcommon.OutBoundReq) {
			defer wgDump.Done()
			empty := common.Hash{}.Hex()
			if each.SubmittedTxHash == empty {
				joltChain.AddItem(each)
				return
			}
			err := chainInfo.CosHandler.QueryTxStatus(grpcClient, each.SubmittedTxHash, 10)
			if err != nil {
				joltChain.AddItem(each)
				return
			}
			tick := html.UnescapeString("&#" + "127866" + ";")
			zlog.Logger.Info().Msgf(" %v the tx has been submitted, we catch up with others on pubchain", tick)
		}(el)
	}
	wgDump.Wait()
}

func updateMetrics(metric *monitor.Metric) {
	metric.UpdateStatus()
}
