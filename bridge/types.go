package bridge

import (
	"math/big"

	"go.uber.org/atomic"

	"gitlab.com/joltify/joltifychain-bridge/config"
)

type outboundInfo struct {
	firstTime   bool
	pauseHeight uint64
	chainType   string
	failed      *atomic.Int32
	wait        *atomic.Bool
	processDone *atomic.Bool

	currentHeight *big.Int
}

func newOutboundInfo(chainType string) *outboundInfo {
	return &outboundInfo{
		firstTime:   true,
		pauseHeight: 0,
		chainType:   chainType,
		failed:      atomic.NewInt32(0),
		wait:        atomic.NewBool(false),
		processDone: atomic.NewBool(true),

		currentHeight: big.NewInt(0),
	}
}

func newOutboundInfos() map[string]*outboundInfo {
	outboundInfos := make(map[string]*outboundInfo, len(config.Chains))
	for _, chainType := range config.Chains {
		outboundInfos[chainType] = newOutboundInfo(chainType)
	}
	return outboundInfos
}
