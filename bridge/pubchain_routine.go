package bridge

import (
	"context"
	"errors"
	"math/big"
	"time"

	"github.com/ethereum/go-ethereum/ethclient"
	zlog "github.com/rs/zerolog/log"
	ttypes "github.com/tendermint/tendermint/types"
	"go.uber.org/atomic"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"gitlab.com/joltify/joltifychain-bridge/common"
	"gitlab.com/joltify/joltifychain-bridge/config"
	"gitlab.com/joltify/joltifychain-bridge/cosbridge"
	"gitlab.com/joltify/joltifychain-bridge/monitor"
	"gitlab.com/joltify/joltifychain-bridge/pubchain"
)

// ErrChain error of joltify or handled chain issue
var ErrChain = errors.New("chain error")

func pubChainProcess(pi *pubchain.Instance, joltChain *cosbridge.JoltChainInstance, oppyGrpc string, metric *monitor.Metric, blockHead *pubchain.BlockHead, pubRollbackGap int64, outboundInfo *outboundInfo, inKeygenInProgress *atomic.Bool, previousTssBlockOutBound *PreviousTssBlockOutBound) error {
	head := blockHead.Head
	chainInfo := pi.GetChainClientERC20(blockHead.ChainType)
	if chainInfo == nil {
		zlog.Logger.Error().Msgf("[%v]chainInfo not found", blockHead.ChainType)
		return nil
	}

	pools := joltChain.GetPool()
	if len(pools) < 2 || pools[1] == nil {
		// this is need once we resume the bridge to avoid the panic that the pool address has not been filled
		zlog.Logger.Warn().Msgf("[%v]we do not have 2 pools to start the tx", blockHead.ChainType)
		return ErrChain
	}

	amISigner, err := joltChain.CheckWhetherSigner(pools[1].PoolInfo)
	if err != nil {
		zlog.Logger.Error().Err(err).Msgf("[%v]fail to check whether we are the node submit the mint request", blockHead.ChainType)
		return ErrChain
	}

	if !amISigner {
		zlog.Logger.Info().Msgf("[%v]we are not the signer, we quite the block process", blockHead.ChainType)
		return nil
	}

	latestHeight, err := chainInfo.GetLastBlockHeightWithLock()
	if err != nil {
		zlog.Logger.Error().Err(err).Msgf("[%v]fail to get the latest block height", blockHead.ChainType)
		return ErrChain
	}

	joltBlockHeight, err := joltChain.GetLastBlockHeightWithLock()
	if err != nil {
		zlog.Logger.Error().Err(err).Msgf("[%v]we fail to get the joltify chain latest height", blockHead.ChainType)
		return ErrChain
	}

	updateMetrics(metric)

	// process block with rollback gap
	processableBlockHeight := big.NewInt(0).Sub(head.Number, big.NewInt(pubRollbackGap))

	err = pi.ProcessNewERC20Block(blockHead.ChainType, chainInfo, processableBlockHeight, joltChain.FeeModule, oppyGrpc)
	if err != nil {
		zlog.Logger.Error().Err(err).Msgf("[%v]fail to process the inbound block[%v]", blockHead.ChainType, blockHead.Head.Number.Uint64())
		return ErrChain
	}
	isMoveFund := false
	previousMoveFundItem, height := pi.PopMoveFundItemAfterBlock(joltBlockHeight, chainInfo.ChainType)

	if previousMoveFundItem != nil {
		// we move fund in the public chain
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
		ethClient, err := ethclient.DialContext(ctx, chainInfo.WsAddr)
		cancel()
		if err != nil {
			pi.AddMoveFundItem(previousMoveFundItem.PoolInfo, height, chainInfo.ChainType)
			zlog.Logger.Error().Err(err).Msgf("[%v]fail to dial the websocket", blockHead.ChainType)
		}
		if ethClient != nil {
			isMoveFund = pi.MoveFound(height, chainInfo, previousMoveFundItem.PoolInfo, ethClient)
			ethClient.Close()
		}
	}
	if isMoveFund {
		// once we move fund, we do not send tx to be processed
		return nil
	}

	if outboundInfo.failed.Load() > 5 {
		mid := (latestHeight / uint64(config.ROUNDBLOCK)) + 1
		outboundInfo.pauseHeight = mid * uint64(config.ROUNDBLOCK)
		outboundInfo.failed.Store(0)
		outboundInfo.wait.Store(true)
	}

	if latestHeight < outboundInfo.pauseHeight {
		zlog.Logger.Warn().Msgf("[%v]to many outbound errors we wait for %v blocks to continue", blockHead.ChainType, outboundInfo.pauseHeight-latestHeight)
		if latestHeight == outboundInfo.pauseHeight-1 {
			zlog.Info().Msgf("[%v]we now load the onhold outbound tx", blockHead.ChainType)
			putOnHoldBlockOutBoundBack(chainInfo, joltChain)
		}
		return nil
	}

	outboundInfo.wait.Store(false)

	if !outboundInfo.processDone.Load() {
		zlog.Warn().Msgf("[%v]the previous outbound has not been fully processed, we do not feed more tx", blockHead.ChainType)
		metric.UpdateOutboundTxNum(float64(joltChain.Size()))
		return nil
	}

	if inKeygenInProgress.Load() {
		zlog.Warn().Msgf("[%v]we are in keygen process, we do not feed more tx", blockHead.ChainType)
		metric.UpdateOutboundTxNum(float64(joltChain.Size()))
		return nil
	}

	if joltChain.IsEmpty() {
		zlog.Logger.Debug().Msgf("[%v]the outbound queue is empty, we put all onhold back", blockHead.ChainType)
		putOnHoldBlockOutBoundBack(chainInfo, joltChain)
	}

	// todo we need also to add the check to avoid send tx near the churn blocks
	if processableBlockHeight.Uint64()-previousTssBlockOutBound.GetHeight(blockHead.ChainType) >= cosbridge.GroupBlockGap && !joltChain.IsEmpty() {
		// if we do not have enough tx to process, we wait for another round
		if joltChain.Size() < pubchain.GroupSign && outboundInfo.firstTime {
			outboundInfo.firstTime = false
			metric.UpdateOutboundTxNum(float64(joltChain.Size()))
			return nil
		}

		zlog.Logger.Warn().Msgf("we feed the %v outbound tx now %v (processableBlockHeight:%v, previousTssBlockOutBound:%v)", blockHead.ChainType, pools[1].PoolInfo.CreatePool.PoolAddr.String(), processableBlockHeight, previousTssBlockOutBound.GetHeight(blockHead.ChainType))

		outboundItems := joltChain.PopItem(pubchain.GroupSign, blockHead.ChainType)

		if outboundItems == nil {
			zlog.Logger.Info().Msgf("[%v]empty queue", blockHead.ChainType)
			return nil
		}

		err = pi.FeedTx(pools[1].PoolInfo, outboundItems, blockHead.ChainType)
		if err != nil {
			zlog.Logger.Error().Err(err).Msgf("[%v]fail to feed the outbound tx", blockHead.ChainType)
			joltChain.AddItemsToOnHoldQueue(outboundItems)
			return nil
		}
		previousTssBlockOutBound.SetHeight(processableBlockHeight.Uint64(), blockHead.ChainType)
		outboundInfo.firstTime = true
		metric.UpdateOutboundTxNum(float64(joltChain.Size()))
		joltChain.OutboundReqChan <- outboundItems
		outboundInfo.processDone.Store(false)
	}
	return nil
}

func pubChainProcessCosmos(block *ttypes.Header, pi *pubchain.Instance, joltChain *cosbridge.JoltChainInstance, metric *monitor.Metric, pubRollbackGap int64, outboundInfo *outboundInfo, inKeygenInProgress *atomic.Bool, previousTssBlockOutBound *PreviousTssBlockOutBound) error {
	currentProcessBlockHeight := block.Height
	processableBlockHeight := currentProcessBlockHeight - pubRollbackGap

	pools := joltChain.GetPool()
	if len(pools) < 2 || pools[1] == nil {
		// this is need once we resume the bridge to avoid the panic that the pool address has not been filled
		zlog.Logger.Warn().Msgf("[%v]we do not have 2 pools to start the tx", pi.CosChain.ChainType)
		return ErrChain
	}

	amISigner, err := joltChain.CheckWhetherSigner(pools[1].PoolInfo)
	if err != nil {
		zlog.Logger.Error().Err(err).Msgf("[%v]fail to check whether we are the node submit the mint request", pi.CosChain.ChainType)
		return ErrChain
	}

	if !amISigner {
		zlog.Logger.Info().Msgf("[%v]we are not the signer, we quite the block process", pi.CosChain.ChainType)
		return nil
	}
	cosmosBlockRaw, err := common.GetBlockByHeight(pi.CosChain.CosHandler.GrpcClient, processableBlockHeight)
	if err != nil {
		zlog.Error().Err(err).Msgf("[%v]fail to get the block by height %v", pi.CosChain.ChainType, processableBlockHeight)
		return ErrChain
	}
	latestCosmosHeight, err := common.GetLastBlockHeight(pi.CosChain.CosHandler.GrpcClient)
	if err != nil {
		zlog.Error().Err(err).Msgf("[%v]we fail to get the joltify chain latest height", pi.CosChain.ChainType)
		return ErrChain
	}
	pi.ProcessNewCosmosBlock(cosmosBlockRaw, pools, processableBlockHeight)

	joltBlockHeight, err := joltChain.GetLastBlockHeightWithLock()
	if err != nil {
		zlog.Error().Err(err).Msgf("[%v]we have reset the joltify chain grpc as it is failed to be connected", pi.CosChain.ChainType)
		return nil
	}

	isMoveFund := false
	previousMoveFundItem, height := pi.PopMoveFundItemAfterBlock(joltBlockHeight, pi.CosChain.ChainType)
	if previousMoveFundItem != nil {
		// we move fund in the public chain
		grpcClient, err := grpc.Dial(pi.CosChain.CosHandler.GrpcAddr, grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			zlog.Logger.Error().Err(err).Msgf("fail to dial at queue new block for %v", pi.CosChain.ChainType)
			return nil
		}
		if grpcClient != nil {
			isMoveFund = pi.MoveFundCosmos(height, "cosmos", grpcClient, previousMoveFundItem.PoolInfo)
			grpcClient.Close()
		}
	}
	if isMoveFund {
		// once we move fund, we do not send tx to be processed
		return nil
	}

	if outboundInfo.failed.Load() > 5 {
		mid := (latestCosmosHeight / int64(config.ROUNDBLOCK)) + 1
		outboundInfo.pauseHeight = uint64(mid * int64(config.ROUNDBLOCK))
		outboundInfo.failed.Store(0)
		outboundInfo.wait.Store(true)
	}

	if uint64(latestCosmosHeight) < outboundInfo.pauseHeight {
		zlog.Logger.Warn().Msgf("to many errors for %v outbound we wait for %v blocks to continue", pi.CosChain.ChainType, outboundInfo.pauseHeight-uint64(latestCosmosHeight))
		if uint64(latestCosmosHeight) == outboundInfo.pauseHeight-1 {
			zlog.Info().Msgf("we now load the onhold %v tx", pi.CosChain.ChainType)
			putOnHoldBlockOutBoundBackCosmos(pi.CosChain, joltChain)
		}
		return nil
	}

	outboundInfo.wait.Store(false)

	if !outboundInfo.processDone.Load() {
		zlog.Warn().Msgf("the previous outbound has not been fully processed, we do not feed more %v tx", pi.CosChain.ChainType)
		metric.UpdateOutboundTxNum(float64(joltChain.Size()))
		return nil
	}

	if inKeygenInProgress.Load() {
		zlog.Warn().Msgf("we are in keygen process, we do not feed more %v tx", pi.CosChain.ChainType)
		metric.UpdateOutboundTxNum(float64(joltChain.Size()))
		return nil
	}

	if joltChain.IsEmpty() {
		zlog.Logger.Debug().Msgf("the inbound queue is empty, we put all onhold back for %v", pi.CosChain.ChainType)
		putOnHoldBlockOutBoundBackCosmos(pi.CosChain, joltChain)
	}

	// todo we need also to add the check to avoid send tx near the churn blocks
	if processableBlockHeight-int64(previousTssBlockOutBound.GetHeight(pi.CosChain.ChainType)) >= cosbridge.GroupBlockGap && !joltChain.IsEmpty() {
		// if we do not have enough tx to process, we wait for another round
		if joltChain.Size() < pubchain.GroupSign && outboundInfo.firstTime {
			outboundInfo.firstTime = false
			// FIXME: update the metric with different chain
			metric.UpdateOutboundTxNum(float64(joltChain.Size()))
			return nil
		}

		zlog.Logger.Warn().Msgf("we feed the %v outbound tx now %v (processableBlockHeight:%v, previousTssBlockOutBound:%v)", pi.CosChain.ChainType, pools[1].PoolInfo.CreatePool.PoolAddr.String(), processableBlockHeight, previousTssBlockOutBound.GetHeight(pi.CosChain.ChainType))

		outboundItems := joltChain.PopItem(pubchain.GroupSign, pi.CosChain.ChainType)

		if outboundItems == nil {
			zlog.Logger.Info().Msgf("empty queue for chain %v", pi.CosChain.ChainType)
			return nil
		}

		err = pi.FeedTxCosmos(pools[1].PoolInfo, "cosmos", outboundItems)
		if err != nil {
			zlog.Logger.Error().Err(err).Msgf("fail to feed the %v tx", pi.CosChain.ChainType)
			joltChain.AddItemsToOnHoldQueue(outboundItems)
			return nil
		}

		previousTssBlockOutBound.SetHeight(uint64(processableBlockHeight), pi.CosChain.ChainType)
		outboundInfo.firstTime = true
		metric.UpdateOutboundTxNum(float64(joltChain.Size()))
		joltChain.OutboundReqChan <- outboundItems
		outboundInfo.processDone.Store(false)
	}
	return nil
}
